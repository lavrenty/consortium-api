package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/stripe/stripe-go"
)

func handleBillingCard(c *gin.Context) {
	user := c.MustGet("current").(*User)

	var cardMsg struct {
		Token string `json:"token"`
	}

	err := c.BindJSON(&cardMsg)
	if err != nil {
		apiError(c, 400, "invalid argument", err)
		return
	}

	if err := user.AddCard(&stripe.CardParams{Token: cardMsg.Token}); err != nil {
		apiError(c, 500, "internal error", err)
		return
	}

	c.JSON(http.StatusOK, gin.H{"success": true, "user": user.ToFullJSON()})
}

func handleBillingCardDestroy(c *gin.Context) {
	user := c.MustGet("current").(*User)

	if err := user.DelCard(); err != nil {
		apiError(c, 500, "internal error", err)
		return
	}
	c.JSON(http.StatusOK, gin.H{"success": true, "user": user.ToFullJSON()})
}

// GET v1/billing/setup
func handleBillingSetup(c *gin.Context) {
	user := c.MustGet("current").(*User)

	token, err := createToken(user, "billing-setup")
	if err != nil {
		c.HTML(http.StatusInternalServerError, "500.html", gin.H{"message": "Error creating token"})
		return
	}

	c.HTML(http.StatusOK, "billing_setup.html", gin.H{
		"url":   config.Origin,
		"token": token,
	})
}

// POST v1/billing/transfer - issue a customer payout transaction
func handleBillingTransfer(c *gin.Context) {
	_ = c.MustGet("current").(*User)

	apiError(c, 500, "not implemented", nil)
}
