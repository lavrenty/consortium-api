package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"sync"
	"sync/atomic"
	"time"

	"github.com/Pallinder/go-randomdata"
	"github.com/Sirupsen/logrus"
	"github.com/fatih/color"
	"github.com/gorilla/websocket"
	"github.com/stripe/stripe-go"
	"github.com/stripe/stripe-go/account"
	"github.com/stripe/stripe-go/bankaccount"
)

func clientSimConnect(userID uint, path string) *wsConn {
	user, err := GetUser(userID)
	if err != nil || user == nil {
		panic(err)
	}

	t, err := createToken(user, "simulator-ws")
	if err != nil {
		panic(err)
	}

	urlstr := os.ExpandEnv("$SIMULATOR_URL")
	if len(urlstr) == 0 {
		urlstr = fmt.Sprintf("http://%s", config.Listen)
	}

	u, err := url.Parse(urlstr)
	if err != nil {
		panic(err)
	}
	switch u.Scheme {
	case "http":
		u.Scheme = "ws"
	case "https":
		u.Scheme = "wss"
	default:
		panic(fmt.Sprintf("unknown scheme %s", u.Scheme))
	}
	u.Path = path
	h := http.Header{
		"Authorization": {fmt.Sprintf("BEARER %s", t)},
	}

	ws, _, err := websocket.DefaultDialer.Dial(u.String(), h)
	if err != nil {
		log.Fatalf("user %d ws connect to %s failed: %v\n", userID, u.String(), err)
	}

	return &wsConn{Conn: ws}
}

func clientSimLocalTopic() *Topic {
	topics := []string{
		"Web Development",
		"Investing",
		"Technology",
		"C (programming language)",
		"Entrepreneurship",
		"Fashion and Style",
		"Health",
		"Costa Rica",
		"Quora",
		"Movies",
		"Amazon Web Services",
		"Mathematics",
		"Startup Advice and Strategy",
		"Web Applications",
		"Go (programming language)",
		"Bitcoin",
		"Cryptocurrencies",
		"Blockchain",
		"Economics",
		"iOS development",
		"Venture Capital",
		"Project Management",
		"Computer Programmers",
		"Music",
		"Mathematics",
		"Psychology",
		"Medicine and Healthcare",
		"Physics",
		"Business",
		"Books",
		"Startups",
		"India",
		"Cooking",
		"Computer Science",
		"Computer Programming",
		"Healthy Eating",
		"Education",
		"Fine Art",
		"Food",
		"Fashion and Style",
		"Finance",
		"Design",
		"Sketch (Vector design application)",
		"Google",
		"The Future",
		"MySQL",
		"Redis (NoSQL database)",
	}

	topic, err := FindOrCreateTopicByName(topics[rand.Intn(len(topics))])
	if err != nil {
		panic(err)
	}
	return topic
}

func clientSimHTTP(user *User, path string, j JSON) []byte {
	jsonStr, _ := json.Marshal(j)

	t, err := createToken(user, "simulator")
	if err != nil {
		panic(err)
	}

	host := os.ExpandEnv("$SIMULATOR_URL")
	if len(host) == 0 {
		host = fmt.Sprintf("http://%s", config.Listen)
	}
	url := fmt.Sprintf("%s/v1/%s", host, path)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("BEARER %s", t))

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatalf("user %d request %s failed with error %v\n", user.ID, url, err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		log.Fatalf("user %d request %s failed with %d (%s)\n", user.ID, url, resp.StatusCode, resp.Status)
	}

	body, _ := ioutil.ReadAll(resp.Body)

	return body
}

func clientSimHTTPCard(user *User, topics []*Topic) uint {
	topicIds := []uint{}
	for _, topic := range topics {
		topicIds = append(topicIds, topic.ID)
	}

	text := randomdata.Paragraph()
	tl := len(text)
	if tl > CardMaxTextLength-2 {
		tl = CardMaxTextLength - 2
	}

	rate := CardMinRate + rand.Intn(500)
	if rand.Intn(10) <= 5 {
		rate = 0
	}

	if rate > 0 && !user.IsBillable() {
		if err := user.AddCard(&stripe.CardParams{
			Number: "4242424242424242",
			CVC:    "234",
			Month:  "10",
			Year:   "2020",
			Name:   user.PublicName(),
		}); err != nil {
			log.Fatalf("user %d failed to add credit card\n", user.ID)
		}
	}

	body := clientSimHTTP(user, "cards", JSON{
		"topic_ids": topicIds,
		"text":      text[:tl],
		"rate":      rate,
		"language":  "en",
	})

	var cardMsg struct {
		Success bool
		CardID  uint `json:"card_id"`
		Error   struct {
			Code    int
			Message string
		}
	}

	if err := json.Unmarshal(body, &cardMsg); err != nil {
		panic(err)
	}

	if cardMsg.Success {
		return cardMsg.CardID
	}

	log.Fatalf("clientSimHTTPCard user %d failed: %s\n", user.ID, cardMsg.Error.Message)

	return 0
}

func clientSimulate(count int) {
	var wg sync.WaitGroup
	var radarCount int32

	rand.Seed(time.Now().UnixNano())

	// start dynamic answering machine
	user := clientSimLocalLogin()
	go clientSimResponder(user)

	for i := 0; i < count; i++ {
		clientLinger()
		wg.Add(1)
		go func() {
			defer wg.Done()

			user := clientSimLocalLogin()
			for {
				rc := atomic.LoadInt32(&radarCount)
				//  rand.Intn(2) == 0
				if float64(rc) < float64(count)/2 {
					logrus.Debugf("s user%d start card", user.ID)

					atomic.AddInt32(&radarCount, 1)
					clientSimAsk(user)
					atomic.AddInt32(&radarCount, -1)
				} else {
					logrus.Debugf("s user%d start dashboard", user.ID)
					clientSimDash(user)
				}
				clientLinger()
			}
		}()
	}

	wg.Wait()
	logrus.Debugf("simulator all done")
}

func clientLinger() {
	time.Sleep(time.Duration(rand.Intn(1000)) * time.Millisecond)
}

func clientSimResponder(user *User) {
	topic, err := FindOrCreateTopicByName("Answer")
	if err != nil {
		panic(err)
	}

	if err := user.Subscribe(topic, false); err != nil {
		panic(err)
	}

	logrus.Debugf("responder user%d on", user.ID)

	for {
		// 2 connect to dashboard, pick random stuff
		cardID := clientSimDashboard(user, topic.ID)
		if cardID == 0 {
			break
		}

		logrus.Debugf("responder card %d", cardID)
		u, err := MustGetUser(user.ID)
		if err != nil {
			panic(err)
		}
		go func(user *User, cardID uint) {
			// 3 get call details
			callID := clientSimHTTPCall(user, cardID)
			if callID == 0 {
				return
			}

			// 4 make call
			logrus.Debugf("responder call %d", callID)
			clientSimCall(user, callID, false, 1)

			// 5 rate guy
			clientLinger()
			clientSimHTTPRate(user, callID)
		}(u, cardID)
	}

	return
}

func clientSimHTTPRate(user *User, callID uint) {
	clientSimHTTP(user, fmt.Sprintf("call/%d/rate", callID), JSON{
		"rating":       rand.Intn(6),
		"conn_quality": rand.Intn(6),
	})
}

func clientSimAsk(user *User) {
	// 1 local find/create topics
	topics := []*Topic{}
	for i := 0; i < 1+rand.Intn(5); i++ {
		topics = append(topics, clientSimLocalTopic())
	}

	// 2 http create card
	cardID := clientSimHTTPCard(user, topics)
	clientLinger()

	// 3 ws go to radar
	callID := clientSimRadar(user, cardID)
	if callID == 0 {
		return
	}

	// 4 connect for chat (1-30 mins)
	clientSimCall(user, callID, true, 0)

	// 5 rate guy
	clientLinger()
	clientSimHTTPRate(user, callID)
}

func clientSimDash(user *User) {
	// 1 local find/create topics and join them
	for i := 0; i < 1+rand.Intn(10); i++ {
		topic := clientSimLocalTopic()
		if err := user.Subscribe(topic, false); err != nil {
			panic(err)
		}
	}

	clientLinger()

	// 2 connect to dashboard, pick random stuff
	cardID := clientSimDashboard(user, 0)
	if cardID == 0 {
		return
	}

	// 3 get call details
	callID := clientSimHTTPCall(user, cardID)
	if callID == 0 {
		return
	}

	// 4 make call
	clientSimCall(user, callID, false, 0)

	// 5 rate guy
	clientLinger()
	clientSimHTTPRate(user, callID)
}

// returns callID
func clientSimHTTPCall(user *User, cardID uint) uint {
	body := clientSimHTTP(user, fmt.Sprintf("card/%d/dial", cardID), JSON{})

	var callMsg struct {
		Success bool
		Call    struct {
			ID       uint `json:"id"`
			CallerID uint `json:"caller_id"`
			CalleeID uint `json:"callee_id"`
			Rate     uint
		}
		Session struct {
			SID   string `json:"sid"`
			Token string `json:"token"`
		}
		Error struct {
			Code    uint
			Message string
		}
	}

	if err := json.Unmarshal(body, &callMsg); err != nil {
		panic(err)
	}

	if callMsg.Success {
		logrus.Debugf("call card%d sid(%s) token(%s)", cardID, callMsg.Session.SID, callMsg.Session.Token)
		return callMsg.Call.ID
	}

	logrus.Debugf("call card%d failed %d %s", cardID, callMsg.Error.Code, callMsg.Error.Message)
	return 0
}

// call simulator
func clientSimCall(user *User, callID uint, isCallee bool, scenarioID int) {
	var callStateMsg struct {
		Rate     uint
		Cost     int
		Duration uint
		Live     bool
		Hangup   bool
	}

	green := color.New(color.FgGreen).SprintFunc()
	red := color.New(color.FgRed).SprintFunc()
	info := color.New(color.FgWhite, color.BgGreen).SprintFunc()

	hiblue := color.New(color.FgHiBlue).SprintFunc()
	himagenta := color.New(color.FgHiMagenta).SprintFunc()

	id := fmt.Sprintf("c call%d(%s)", callID, hiblue("caller"))
	if isCallee {
		id = fmt.Sprintf("c call%d(%s)", callID, himagenta("callee"))
	}

	ws := clientSimConnect(user.ID, fmt.Sprintf("/v1/call/%d", callID))
	defer ws.Close()

	client := wsClient(ws, false)

	type CallScenarioCmd struct {
		MaxDelay int
		Command  string
	}

	scenarios := [][]CallScenarioCmd{
		{{1, "live"}, {600, "hangup"}},
		{{30, "live"}, {300, "dead"}, {30, "live"}, {600, "hangup"}},
		{{30, "live"}, {600, "dead"}, {10, "hangup"}},
		{{30, "live"}, {600, "dead"}, {30, "live"}, {600, "hangup"}},
		{{30, "live"}, {600, "dead"}, {30, "live"}, {600, "hangup"}},
		{{10, "live"}, {200, "hangup"}},
		{{20, "live"}, {300, "dead"}, {20, "hangup"}},
		{{20, "live"}, {300, "dead"}},
		{{60, "live"}, {600, "dead"}},
		{{10, "hangup"}},
	}
	scenario := scenarios[rand.Intn(len(scenarios))]
	if scenarioID > 0 {
		scenario = scenarios[scenarioID-1]
	}
	logrus.Debugf("%s scenario %v", id, scenario)

	cmds := make(chan string)
	go func(scenario []CallScenarioCmd, cmds chan string) {
		for _, cmd := range scenario {
			delay := time.Duration(1+rand.Intn(cmd.MaxDelay)) * time.Second
			select {
			case <-time.After(delay):
				cmds <- cmd.Command
			}
		}
		time.Sleep(time.Duration(1+rand.Intn(10)) * time.Second)
		close(cmds)
	}(scenario, cmds)

loop:
	for {
		select {
		case msg, ok := <-cmds:
			if !ok {
				logrus.Debugf("%s cmds closed", id)
				break loop
			}
			logrus.Debugf("%s cmd > %s", id, msg)
			if err := ws.WriteJSON(JSON{"method": msg}); err != nil {

			}

		case msg, ok := <-client:

			if !ok {
				logrus.Debugf("%s client closed", id)
				break loop
			}
			switch msg.Method {
			case "error":
				message, ok := msg.Raw["message"].(string)
				if !ok {
					message = string(msg.Bytes)
				}
				logrus.Debugf("%s error %s", id, message)

			case "update":
				// update details
				if err := json.Unmarshal(msg.Bytes, &callStateMsg); err != nil {
					panic(err)
				}

				liveStr := red("dead")
				if callStateMsg.Live {
					liveStr = green("live")
				}
				logrus.Debugf("%s %s %s $%.02f", id, liveStr, time.Duration(time.Duration(callStateMsg.Duration)*time.Second).String(), float64(callStateMsg.Cost)/100)
				if callStateMsg.Hangup {
					logrus.Debugf("%s cmd < hangup", id)
					break loop
				}
			}
		}
	}

	logrus.Debugf("%s %s $%.02f done", id, info(time.Duration(time.Duration(callStateMsg.Duration)*time.Second).String()), float64(callStateMsg.Cost)/100)
}

func clientSimLocalLogin() *User {
	user, err := FindOrCreateUserFromFacebook(strconv.Itoa(rand.Intn(int(1e10))))
	if err != nil {
		panic(err)
	}

	// ha ha ha, descrimination!
	if rand.Intn(3) == 0 {
		// female
		user.Gender.Scan("female")
		user.FirstName.Scan(randomdata.FirstName(randomdata.Female))
		user.Picture.Scan(fmt.Sprintf("http://lorempixel.com/200/200/cats/?%d", rand.Intn(int(1e10))))
	} else {
		// male
		user.Gender.Scan("male")
		user.FirstName.Scan(randomdata.FirstName(randomdata.Male))
		user.Picture.Scan(fmt.Sprintf("http://lorempixel.com/200/200/animals/?%d", rand.Intn(int(1e10))))
	}

	user.LastName.Scan(randomdata.LastName())
	user.Name.Scan(user.FirstName.String + " " + user.LastName.String)

	if _, err = db.NamedExec("UPDATE users SET gender = :gender, name = :name, first_name = :first_name, last_name = :last_name, picture = :picture, updated_at = now() WHERE id = :id", user); err != nil {
		panic(err)
	}

	if err = user.FakeFacebookVerification(); err != nil {
		panic(err)
	}

	if user, err = GetUser(user.ID); err != nil || user == nil {
		panic(err)
	}

	// create stripe account
	params := &stripe.AccountParams{
		Managed: true,
		Country: "US",
		LegalEntity: &stripe.LegalEntity{
			Type: stripe.Individual,
			DOB: stripe.DOB{
				Day:   rand.Intn(26) + 1,
				Month: rand.Intn(10) + 1,
				Year:  1950 + rand.Intn(40),
			},
			SSN: "1332",
			Address: stripe.Address{
				Country: "US",
				State:   "CA",
				City:    "San Francisco",
				Line1:   "Dolores str",
				Zip:     "94114",
			},
			First: user.FirstName.String,
			Last:  user.LastName.String,
		},
		TOSAcceptance: &stripe.TOSAcceptanceParams{
			Date:      time.Now().Unix(),
			IP:        "127.0.0.1",
			UserAgent: "Conspiracy/1.0 Simulator",
		},
	}
	params.AddMeta("user_id", fmt.Sprintf("%d", user.ID))
	params.AddMeta("remote_ip", "127.0.0.1")

	sacc, err := account.New(params)
	if err != nil {
		panic(err)
	}

	a := &StripeAccount{
		UserID:          user.ID,
		StripeAccountID: sacc.ID,
		Country:         sacc.Country,
		DefaultCurrency: sacc.DefaultCurrency,
		Secret:          sacc.Keys.Secret,
		Publish:         sacc.Keys.Publish,
	}
	if err := a.Save(); err != nil {
		panic(err)
	}

	bankParams := &stripe.BankAccountParams{
		AccountID:         a.StripeAccountID,
		AccountHolderType: "individual",
		AccountHolderName: user.PublicName(),
		Country:           "US",
		Currency:          a.DefaultCurrency,
		Routing:           "110000000",
		Account:           "000123456789",
	}

	if _, err := bankaccount.New(bankParams); err != nil {
		panic(err)
	}

	// // add debit card for payout
	// cardParams := &stripe.CardParams.{
	// 	Account:  a.StripeAccountID,
	// 	Name:     user.PublicName(),
	// 	Number:   "5200828282828210",
	// 	Month:    "02",
	// 	Year:     "18",
	// 	CVC:      "232",
	// 	Currency: a.DefaultCurrency,
	// 	Zip:      "94114",
	// 	Default:  true,
	// }
	// cardParams.AddMeta("user_id", fmt.Sprintf("%d", user.ID))
	// cardParams.AddMeta("remote_ip", "127.0.0.1")
	//
	// // sc := a.GetStripeClient()
	// // if _, err := sc.Cards.New(cardParams); err != nil {
	// // 	panic(err)
	// // }
	//
	// if _, err := card.New(cardParams); err != nil {
	// 	panic(err)
	// }

	return user
}

// returns call id or 0
func clientSimRadar(user *User, cardID uint) uint {
	card, err := GetCard(cardID)
	if err != nil || card == nil {
		panic(err)
	}

	ws := clientSimConnect(card.UserID, fmt.Sprintf("/v1/card/%d/radar", card.ID))
	defer ws.Close()

	client := wsClient(ws, false)

	// set timeout
	timeout := time.After(time.Duration(1+rand.Intn(15)) * time.Minute)

loop:

	for {
		select {
		case <-timeout:
			logrus.Debugf("r card%d timeout", cardID)
			break loop

		// recieve client packets
		case msg, ok := <-client:
			if !ok {
				logrus.Debugf("r card%d client closed", cardID)
				break loop
			}
			switch msg.Method {
			case "error":
				message, ok := msg.Raw["message"].(string)
				if !ok {
					message = string(msg.Bytes)
				}
				logrus.Debugf("r card%d error %s", cardID, message)

			case "update":
				logrus.Debugf("r card%d update %v", cardID, msg.Raw)

			case "view":
				logrus.Debugf("r card%d view %v", cardID, msg.Raw)

			case "call":
				logrus.Debugf("r card%d call %v", cardID, msg.Raw)
				var callMsg struct {
					Call struct {
						ID uint
					}
				}
				if err := json.Unmarshal(msg.Bytes, &callMsg); err != nil {
					panic(err)
				}
				return callMsg.Call.ID

			case "failure":
				logrus.Debugf("r card%d failure %v", cardID, msg.Raw)
				break loop

			default:
				logrus.Debugf("r card%d unknown command `%s` (%s)", cardID, msg.Method, string(msg.Bytes))
			}
		}
	}
	logrus.Debugf("r card%d off", cardID)

	return 0
}

// SimCard for simulator use only
type SimCard struct {
	ID       uint `json:"id"`
	UserID   uint `json:"user_id"`
	Text     string
	Rate     uint
	Language string
	Views    int
	TopicIds []uint `json:"topic_ids"`
	Live     bool
}

// UpdateFromJSON gah
func (s *SimCard) UpdateFromJSON(data []byte) error {
	var msg struct {
		Card struct {
			SimCard
		} `json:"card"`
		// users:
		// topics:
	}

	if err := json.Unmarshal(data, &msg); err != nil {
		return err
	}

	*s = msg.Card.SimCard
	return nil
}

// returns cardID to call or 0
func clientSimDashboard(user *User, topicID uint) uint {
	cards := map[uint]SimCard{}

	ws := clientSimConnect(user.ID, "/v1/cards/dashboard")
	defer ws.Close()

	client := wsClient(ws, false)

	ticker := time.NewTicker(time.Duration(5+rand.Intn(10)) * time.Second)
	defer ticker.Stop()

	timeout := time.After(time.Duration(5+rand.Intn(30)) * time.Minute)

loop:
	for {
		select {
		case <-timeout:
			break

		case <-ticker.C:
			if topicID > 0 {
				if len(cards) > 0 {
					for cardID := range cards {
						kill := true
						for _, t := range cards[cardID].TopicIds {
							if t == topicID {
								kill = false
								break
							}
						}

						if kill {
							logrus.Debugf("d user%d cmd > close %d (topic %v missing in %v)", user.ID, cardID, topicID, cards[cardID].TopicIds)
							delete(cards, cardID)
							if err := ws.WriteJSON(JSON{
								"method":  "close",
								"card_id": cardID,
							}); err != nil {
								logrus.Debugf("d user%d write error (%s)", user.ID, err)
								break loop
							}
						}
					}

					// call first valid card
					for cardID := range cards {
						return cardID
					}
				}
			} else if topicID == 0 && len(cards) > 0 {
				// randomly close card
				if rand.Intn(7) == 0 {
					for cardID := range cards {
						delete(cards, cardID)
						logrus.Debugf("d user%d cmd > close %d (randomly)", user.ID, cardID)
						if err := ws.WriteJSON(JSON{
							"method":  "close",
							"card_id": cardID,
						}); err != nil {
							logrus.Debugf("d user%d write error (%s)", user.ID, err)
							break loop
						}

						break
					}
				}

				// randomly call
				if rand.Intn(100) == 0 {
					for cardID := range cards {
						logrus.Debugf("d user%d will call card%d", user.ID, cardID)
						return cardID
					}
				}

				// randomly disconnect
				if rand.Intn(1000) == 0 {
					break loop
				}
			}

		case msg, ok := <-client:
			if !ok {
				logrus.Debugf("d user%d client closed", user.ID)
				break loop
			}
			switch msg.Method {
			case "error":
				message, ok := msg.Raw["message"].(string)
				if !ok {
					message = string(msg.Bytes)
				}
				logrus.Debugf("d user%d error %s", user.ID, message)
			case "close":
				// follow server lead and close
				cardID := uint(msg.Raw["card_id"].(float64))
				logrus.Debugf("d user%d cmd < close %d", user.ID, cardID)

				if _, ok := cards[cardID]; !ok {
					logrus.Debugf("d user%d close invalid card", user.ID)
					break loop
				}
				delete(cards, cardID)
				logrus.Debugf("d user%d cmd > close %d", user.ID, cardID)
				if err := ws.WriteJSON(JSON{
					"method":  "close",
					"card_id": cardID,
				}); err != nil {
					logrus.Debugf("d user%d write error (%s)", user.ID, err)
					break loop
				}

			case "system":
				logrus.Debugf("d user%d cmd < system %v", user.ID, msg.Raw)

			case "show":
				// store card in our `cards` array
				logrus.Debugf("d user%d cmd < show %v", user.ID, msg.Raw)

				card := SimCard{}
				if err := card.UpdateFromJSON(msg.Bytes); err != nil {
					logrus.Debugf("d user%d json error %s", user.ID, err)
					break loop
				}
				cards[card.ID] = card

			case "update":
				logrus.Debugf("d user%d cmd < update %v", user.ID, msg.Raw)

				update := SimCard{}
				if err := json.Unmarshal(msg.Bytes, &update); err != nil {
					logrus.Debugf("d user%d json error %s", user.ID, err)
					break loop
				}
				cardID := uint(msg.Raw["card_id"].(float64))
				card := cards[cardID]
				card.Live = update.Live
				card.Views = update.Views
				cards[cardID] = card

			default:
				logrus.Debugf("d user%d unknown command `%s` (%s)", user.ID, msg.Method, string(msg.Bytes))
			}

		}
	}
	logrus.Debugf("d user%d off", user.ID)

	return 0
}
