package main

import "time"

// DefaultLanguage is pre-filled in user profile if no language is specified and locale is missing
const DefaultLanguage = "en"

// CallTimeFree is what it says it is
const CallTimeFree = 60.0

const (
	// BillingServiceBase is our base cost for services
	BillingServiceBase = 100
	// BillingServiceFee is how much we take (10% is 0.1)
	BillingServiceFee = 0.1
)

const (
	// DashboardSlots is how much card slots we have for each connection
	DashboardSlots = 4
	// DashboardPingerTimeout in seconds (wsPinger)
	DashboardPingerTimeout = 10 * time.Second
)

var (
	// DefaultRates ...
	DefaultRates = [...]uint{0, 10, 20, 30, 50, 80, 100, 150, 200, 300, 400, 500}
)

const (
	// DefaultFbPermissions ...
	DefaultFbPermissions = "user_likes,user_friends,email,user_birthday"
)

const (
	// CardMinTextLength specifies minimal allowed text length
	CardMinTextLength = 2

	// CardMaxTextLength ..
	CardMaxTextLength = 140

	// CardMinRate specifies minimal rate per minute to be specified on card (in cents)
	CardMinRate = 10
)

const (
	// NotificationsDefault ...
	NotificationsDefault = "daily"

	// NotificationsAlways is sent on every relevant question
	NotificationsAlways = "always"

	// NotificationsHourly is sent once an hour
	NotificationsHourly = "hourly"

	// NotificationsDaily is sent once every day
	NotificationsDaily = "daily"

	// NotificationsWeekly is sent at least once a week
	NotificationsWeekly = "weekly"
)
