package main

import (
	"fmt"

	"github.com/gin-gonic/gin"
)

// See https://developer.mozilla.org/en-US/docs/Web/Security/CSP/Using_CSP_violation_reports#Sample_violation_report
func handleCSPReport(c *gin.Context) {
	var args struct {
		CSPReport struct {
			DocumentURI       string `json:"document-uri" binding:"required"`
			Referrer          string `json:"referrer"`
			BlockedURI        string `json:"blocked-uri" binding:"required"`
			ViolatedDirective string `json:"violated-directive" binding:"required"`
			OriginalPolicy    string `json:"original-policy" binding:"required"`
		} `json:"csp-report" binding:"required"`
	}

	if err := c.BindJSON(&args); err != nil {
		apiError(c, 404, "invalid arguments", err)
		return
	}

	sentry.CaptureError(fmt.Errorf("csp violation at %s", args.CSPReport.DocumentURI), map[string]string{
		"document-uri":       args.CSPReport.DocumentURI,
		"referrer":           args.CSPReport.Referrer,
		"blocked-uri":        args.CSPReport.BlockedURI,
		"violated-directive": args.CSPReport.ViolatedDirective,
		"original-policy":    args.CSPReport.OriginalPolicy,
	})

	apiSuccess(c)
}
