package main

import (
	"errors"
	"net/http"
	"strconv"
	"strings"

	"github.com/Sirupsen/logrus"
	"github.com/gin-gonic/gin"
)

// APIError is for apiError call
type APIError string

// ErrAPILineBusy ...
const (
	ErrAPILineBusy       APIError = "line_busy"
	ErrAPINoReply                 = "no_reply"
	ErrAPITextInvalid             = "text_invalid"
	ErrAPIRateInvalid             = "rate_invalid"
	ErrAPITopicInvalid            = "topic_invalid"
	ErrAPINotFound                = "not_found"
	ErrAPIContactSupport          = "contact_support"
)

func apiSuccess(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"success": true})
}

func apiErrorWithType(c *gin.Context, t APIError, message string) {
	logger := c.MustGet("logger").(*logrus.Entry)
	if config.Debug {
		logger.Debugf("[API] %s %s failing with code 400, type %s, message %s\n", c.Request.Method, c.Request.URL.String(), t, message)
	}

	c.JSON(http.StatusOK, gin.H{"success": false, "error": gin.H{"code": 400, "type": t, "message": message}})
	c.Abort()
}

func apiErrorInternal(c *gin.Context, err error) {
	apiError(c, http.StatusInternalServerError, "internal error", err)
}

func apiError(c *gin.Context, code int, message string, err error) {
	if err == nil {
		err = errors.New(message)
	}

	logger := c.MustGet("logger").(*logrus.Entry)
	if config.Debug {
		logger.WithError(err).Debugf("[API] %s %s failing with code %d, message %s", c.Request.Method, c.Request.URL.String(), code, message)
	}

	// only log 5xx errors
	if code >= 500 {
		sentry.CaptureError(err, map[string]string{
			"type":    "api",
			"url":     c.Request.URL.String(),
			"method":  c.Request.Method,
			"ip":      strings.Split(c.ClientIP(), ":")[0],
			"code":    strconv.Itoa(code),
			"message": message,
		})
	}

	c.JSON(http.StatusOK, gin.H{"success": false, "error": gin.H{"code": code, "type": "error", "message": message}})
	c.Abort()
}
