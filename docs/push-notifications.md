# Push Notifications

### Card notification

Is sent when new relevant card appears. Basically it matches the GET /card/:id response.

Parameters:

```js
{
  type: "card_live",
  data: {
    card: <Card>,
    users: {<User(condensed)>, ...},
    topics: {<Topic>, ...},
  },
  expires_in: int // seconds in which to discard notification
}
```
