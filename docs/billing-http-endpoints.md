# Billing HTTP Endpoints

#### POST /billing/card [access_token]

Set current credit card for user

```js
{
	token: string // stripe token
}
```

```js
{
	success: bool,
	user: <User(full)>
}
```


#### DELETE /billing/card [access_token]

Remove current user card

Reply:

```js
{
		success: bool,
		user: <User(full)>
}
```


## Stripe Connect Endpoints

#### GET /billing/stripe [access_token]

Returns current stripe account details

Reply:

```js
{
	success: bool,
	account: <StripeAccount>
}
```

#### POST /billing/stripe [access_token]

Request:

```js
{
	country: string // 2-letter country code for which account is being created
}
```

Reply:

```js
{
	success: bool,
	account: <StripeAccount>
}
```

#### PUT /billing/stripe [access_token]

Update stripe connect information, as well as verification fields?

*WIP*

#### DELETE /billing/stripe [access_token]

*WIP* Deletes stripe connect information

Reply:

```js
{
	success: bool
}
```

### POST /billing/stripe/card [access_token]

Connect Stripe.JS card token with managed stripe account

Request:

```js
{
	token: string
}
```

Reply:
```js
{
	success: bool
}
```

### POST /billing/stripe/bank [access_token]

Connect Stripe.JS bank token with managed stripe account

Request:

```js
{
	token: string
}
```

Reply:
```js
{
	success: bool
}
```
