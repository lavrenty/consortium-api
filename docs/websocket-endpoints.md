# Websocket endpoints

## DASHBOARD

**WSS**: /cards/dashboard [access_token]

**Client packets**

`close`: must be called when card is dismissed by user (swipe, close)

```js
{
	method: "close",
	?card_id: int, // if regular card is closed
	?system_id: int, // if system card is closed
	?swipe: enum{left,right} // optional
}
```

**Server packets**

`show`

```js
{
	method: "show",
	card: <Card(meta)>,
	caller_id: int, // user that this card was paired with
	users: {<User(condensed)>, ...},
	topics: {<Topic>, ...}
}
```

`system` show system card

```js
{
	method: "system",
	id: int,
	type: string, // currently: facebook, endorsement
	data: {...} // card specific data
}
```

`close` SUGGESTION to close card on screen, client must send close afterwards

```js
{
	method: "close",
	card_id: int,
}
```


`update` update cards on-screen data

```js
{
	method: "update",
	card_id: int, // card id
	?caller_id: int, // optional, user id (user supplied in .users{} array)
	live: bool, // should be true when card ok
	  // if `live` is false you will recieve no more updates on the card
	?rate: float,
	?views: int,
	?users: {<User(condensed)>, ...}
}
```


## RADAR

**WSS** /card/:card_id/radar [access_token]

Radar protocol. Might return 404 if card expired, already answered or not found.

Websocket server push (client is read-only)

If client decides to cancel call, just close websocket

Once connected, specified Card is immediatelly spread out and search begins

**Server packets**

`update` update to radar screen

```js
{
	method: "update",
	card_id: int,
	views: int,
	rate: float,
	busy: bool,
	live: bool
}
```

`view` is sent when user views your card

```js
{
	method: "view",
	card_id: int,
	?picture: string, // optional profile picture
}
```


`success` got client, call must be established

```js
{
	method: "call",
	call: <Call>,
	caller: <User>,
	session: <Session>
}
```

`failed` could not find anyone

```js
{
	method: "failure"
}
```

Next step: wss://../calls/:id


## CALL

**WSS** /call/:call_id [access_token]

Call protocol. Might return 404 if call is expired

Last packet from server is always with `hangup: true`

**Client packets**

`live` send when client side video is streaming correctly

```js
{
	method: "live"
}
```

`dead` sent when client side part of the video chat is not working, stalled or reconnecting

```js
{
	method: "dead"
}
```

`hangup` when the hangup button clicked

```js
{
	method: "hangup"
}
```


**Server packets**

`update` update on-screen data

```js
{
	method: "update",
	live: bool, // wether other party is live or not, e.g. stop the counters
	rate: float, // rate per minute
	cost: float, // overall cost
	duration: int, // duration in seconds
	hangup: bool // if connection is done
}
```
