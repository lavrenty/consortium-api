# Models

## User

This is public profile of regular user

```js
{
	type: "user",
	id: int,
	name: string,
	first_name: string,
	middle_name: string,
	last_name: string,
	languages: [string], // [en,ru, ...]
	is_verified: bool,
	?gender: enum{male,famale},
	?picture: string // url
}
```


### type: condensed

This is public profile of pseudo-anonymous user

```js
{
	type: "user",
	id: int,
	name: string, // condensed, e.g. "Andrew Z." instead of "Andrew Zaitsev-Zotov"
	first_name: string,
	last_name: string, // only first letter, e.g. "Z." instead of "Zaitsev-Zotov"
	languages: [string], // [en,ru, ...]
	is_verified: bool,
	?gender: enum{male,famale},
	?picture: string // url
}
```


### type: full

This includes all personal data available

```js
{
	type: "user",
	id: int,
	name: string,
	first_name: string,
	middle_name: string,
	last_name: string,
	languages: [string], // [en,ru, ...]
	is_verified: bool,
	?gender: enum{male,famale},
	?picture: string, // url

	?balance: int, // current account payout balance available for user payout
	?balance_currency: string, // 3-letter currency code. if balance is present, this field will be present as well.

	?pending_balance: int, // pending balance (e.g. not verified, being processed, etc)
	?pending_balance_currency: string, // 3-letter currency code. if pending_balance is present, this field will be present as well.

	?payout: <StripeAccount>,

	// personal fields
	questions_count: int, // how many calls with questions
	answers_count: int, // how many calls answered

	locale: string,
	has_facebook: bool,
	has_mobile: bool,
	?has_linkedin: bool, // if the field is not present, client apps must not show linkedin connect button
	?has_twitter: bool,

	notifications: bool, // wether notifications for current user are enabled or disabled
	notifications_frequency: string, // 'always', 'hourly', 'daily', 'weekly'; defaults to 'always'

	is_verified: bool, // wether or not account is verified (can create / answer cards at all)
	is_payable: bool, // if can answer cards with rate > 0
	is_billable: bool, // if can create cards with rate > 0
	has_unpaid_invoice: bool, // true if user has outstanding invoice that was unpaid and it's up to user to fix this by
  // either re-adding card or re-issuing payment

	// card details
	card_brand: string, // (if is_billable true) as in stripe, one of  "Unknown", "Visa", "American Express", "MasterCard", "Discover" "JCB", "Diners Club"
	card_last4: string // (if is_billable true) last four digits of credit card
}
```

## Topic

```js
{
	type: "topic",
	id: int,
	name: string
}
```

### type: stats

```js
{
	type: "topic",
	id: int,
	name: string,
	score: int, // relative score of popularity of this topic compraed with other topics
	calls_count: int, // how much calls were made on this topic
	is_suggested: bool // wether topic is suggested
}
```

## Estimate

```js
{
	type: "estimate",
	rate: int, // one of supported rates
	currency: string, // 3-letter iso code for currency
	users_count: int, // how much users do we have matching this criteria
	score: int, // suggested score for reaching best minds on this rate
}
```


## Call

```js
{
	type: "call",
	id: int,
	caller_id: int,
	callee_id: int,
	rate: float,

	is_hangup: bool,
	?duration: int, // only if is_hangup == true
	?started: unixtime, // only if is_hangup == true
	?ended: unixtime // only if is_hangup == true
}
```

## Session

WebRTC, OpenTok session info

```js
{
	token: string,
	sid: string
}
```

## Card

```js
{
	type: "card",
	id: int,
	user_id: int,
	text: string,
	rate: float,
	currency: string, // 3-letter, "usd", "eur", ...
	language: string,
	created: unixtime,

	// CardMeta ext, optional
	views: int,
	topic_ids: [int],
	live: bool
}
```


## System Card

### type: facebook

Facebook Connect required

On click: issue facebook connect and collect user permissions as specified by the `fb_permissions` string

```js
{
	fb_permissions: string
}
```

### type: endorsement

Endorsement request (Do you think user <User> is good in topic <Topic> ?)

```js
{
	user: <User(Public)>,
	topic: <Topic>
}
```

### type: topic

Topic suggestion for user to add to their profile

```js
{
	topic: <Topic>
}
```

### type: notifications

Card that suggests user to enable notifications on their profile

This card has no JSON attached


## StripeAccount

```js
{
	id: int,
	country: string, // 2-letter country code
	default_currency: string, // 3 letter default currency for this account
	transfers_enabled: bool, // wether transfers for this account are enabled
	details_submitted: bool, // wether account details were submitted
	publish: string, // stripe publishable key for card/bank token generation
	?disabled_reason: string, // A string describing the reason for this account being unable to charge and/or transfer, if that is the case. Possible values are rejected.fraud, rejected.terms_of_service, rejected.listed, rejected.other, fields_needed, listed, or other.
	?due_by: datetime, // date by which information must be provided to avoid account locking
	// ?fields_needed: [string] // array of required fields
}
```

## HistoryItem

```js
{
	id: int,
	type: "history_item",
	call_id: int,
	callee_id: int,
	caller_id: int,
	conn_quality: int, // int 1-5 as rated by user; 0 if not rated
	rating: int, // int 1-5 as rated by user; 0 if not rated
	duration: int, // in seconds
	rate: int, // rate in cents (proposed rate, card rate)
	// effective_rate: int, // actual rate in cents, e.g. how much users agreed on
	cost: int, // cost in cents
	call_cost: int, // call cost (e.g. without any fees / taxes)
	currency: string, // 3-letter currency for this transaction
	text: string,
	language: string, // 2-letter is code
	topic_ids: [int], // array of topic ids
	started: unixtime,
	ended: unixtime,
	?base_fee: int, // in cents, how much we charge for services. only for callee.
	?conspiracy_fee: int, // in cents, how much was service fee for this call. only for caller.
	?charge: { // charge on this item, only present when call has rate > 0 and duration > 60 seconds
		paid: bool, // wether invoice was paid or payment is pending
		amount: int, // charged amount, in cents
		currency: string, // 3-letter currency code
		?card_last_four: string, // e.g. "4232"
		?card_brand: string, // visa, amex, mastercard, etc
	}
}
```

## Paginator

```js
{
	page: int,       // current page, defaults to 1
	has_more: bool, // wether there are more pages after this one
	total_count: int // total number of items (not pages!)
}
```
