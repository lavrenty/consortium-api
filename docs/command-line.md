# Command line options

## Main API server (see cospiracy.conf for listening address)

  $ ./conspiracy-api

## Background tasks

### Worker [required]

Background tasks worker, delivered through nsq (tasks.go, worker.go)

  $ ./conspiracy-api -worker

### Pusher [required]

Background push notifications workers (pusher.go)

  $ ./conspiracy-api -pusher

### Scheduler [required]

Runs tasks at specified times (scheduler.go)

  $ ./conspiracy-api -scheduler


## Console API access


### Connect to dashboard websocket

As user with ID 1 (ws_dashboard.go)

  $ ./conspiracy-api -user 1 -dashboard

### Connect to radar websocket

Open radar for card 33. You will be authorized as user who owns card 33

  $ ./conspiracy-api -radar 33

### Dial on a card that's in radar

As user with ID 2 dial on card 33. This will notify user 1 who has open radar for this card.

  $ ./conspiracy-api -user 2 -dial 33

### Connect to call websocket

As user with ID 1 on call with ID 2 (ws_call.go)

  $ ./conspiracy-api -user 1 -call 2

### Generate auth token for curl/wget

  $ ./conspiracy -token -user 1


## Simulator

**LOCAL USAGE ONLY** Please note that all this tasks use local database connection to do certain things and are not purely remote simulators. It means that you CAN NOT connect to api.notconspiracy.com from your laptop.

Lanuch 100 bots

  $ ./conspiracy-api -simulate 100


## Migrations

Run migrations in up/down direction:

```shell
$ ./conspiracy-api -migrate up
```

Put migration files inside `db/migrations` folder with a proper name (i.e `1_create_users.up.sql`, `1_drop_users.down.sql` where `1` is a version number and `up`/`down` are direction keywords).
