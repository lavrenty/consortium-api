# HTTP endpoints

## Login / Signup

#### POST /auth/facebook/connect

After local fb login, exchange token with backend. Backend will verify and update user data, returning you the fresh user object

Request:

```js
{
	"token": string,  // facebook user token
	"locale": string, // en_US
	"languages": [string, ...] // ["ru", "en", "us"] (NSLocale preferredLanguages, 2-letter codes)

}
```

Reply:

```js
{
	success: bool,
	access_token: string, // your access token that you use later
	user: <User(full)>
}
```


## User profile

#### GET /user/:user_id/profile [access_token]

Returns current user profile

```js
{
	success: bool,
	user: <User(full)>,
	topics: {<Topic(stats)>, ...}
}
```


#### DELETE /user/:user_id [access_token]

Disables user account and logs him out


#### POST /user/:user_id/language/:lang [access_token]

Add specified language to user profile. Must be 2-letter code.

```js
{
	success: bool,
	langauges: [string, ...]
}
```

#### DELETE /user/:user_id/language/:lang [access_token]

Delete lang from profile.

```js
{
	success: bool,
	langauges: [string, ...]
}
```

#### POST /user/:user_id/languages [access_token]

Sets user languages to specified list

Request:

```js
{
	languages: [string, ...] // 2-letter codes, e.g. ["en","fr","es",...]
}
```

Reply:

```js
{
	success: bool,
	languages: [string, ...]
}
```

## Topics

#### GET /topics/autocomplete?q=... [access_token]

For use in topic auto-completion

Query param: `q` (string)

Reply:

```js
{
	success: bool,
	topics: [<Topic>, ...]
}
```

#### GET /topics/suggest [access_token]

Suggests topics that might be added to profile by the user

Returns no more than 25 topics

```js
{
	success: bool,
	topics: [<Topic(stats)>, ...]
}
```

#### GET /topics/recent [access_token]

Recent topics used by user + recommended topics, ~10-20

```js
{
	success: bool,
	topics: [<Topic>, ...]
}
```

#### POST /topics/subscribe [access_token]

Subscribe. Add new topic to current user profile

Request:

```js
{
	name: string,
	suggest: bool // true if you want topic to be added to list of suggestions, rather than active in profile; defaults to `false`
}
```

Reply:

```js
{
	success: bool,
	topic: <Topic>
}
```

#### DELETE /topic/:topic_id [acccess_token]

Unsubscribe. Delete topic from user profile

Reply:

```js
{
	success: bool
}
```


## Cards

#### GET /card/:card_id [access_token]

Returns card description as in wsDashboard protocol `show`

Reply:

```js
{
	success: true,
	card: <Card(meta)>,
	topics: {<Topic>, ...},
	users: {<User>, ...}
}
```

#### POST /cards/rates [access_token]

Get estimation of reachable audience

Request:

```js
{
	topic_ids: [id],
	?language: string, // optional, 2-letter device lang
	?text: string // optional, card text, if any
}
```

Response:

```js
{
	success: true,
	rates: {<Estimate>, ...},
}
```

#### POST /cards [access_token]

Create new card

Request:

```js
{
	topic_ids: [id]
	text: string,
	rate: int, // price in cents
	?language: string // 2-letter language code can be provided by client, otherwise we will try autodetect
}
```

Reply:

```js
{
	success: bool,
	card_id: id
}
```

Next step: wss://.../cards/:id/radar and wait for call details

Card is not pushed out unless client uses radar to spread it

Special errors:

* text_invalid — when filled out text field contains invalid value (too short, too long, unable to detect languge, language does not match users profile supported languages)
* rate_invalid — rate field is invalid (too low)
* topic_invalid — unable to process topic fields due to some errors (permissions or whatever)
*


#### POST /card/:card_id/dial [access_token]

When you want to answer card

Reply:

```js
{
	success: bool,
	call: <Call>,
	session: <Session>
}
```

Next step: wss://../calls/:call_id

Errors:

* line_busy — another call in progress (lead user back to dashboard, might recieve live:false/busy:true update on card)
* no_reply — remote party did not reply to our call (lead user back to dashboard)

## Calls

#### POST /call/:call_id/rate [access_token]

Endorse / rate call.

Request:

```js
{
	rating: int, // 0, 1..5; 0 - user not rated
	conn_quality: int // 0, 1..5; 0 - user not rated
}
```

Reply:

```js
{
	success: bool
}
```


#### GET /call/:call_id/report [access_token]

Returns html page for report submitting on call_id

#### POST /call/:call_id/report [access_token]

```js
{
	type: string // one of : spam, nudity, bullying, dislike
}
```

## History

#### GET /history [access_token]

Params: page=<page-number>

Reply:

```js
{
	success: bool,
	has_unpaid_invoice: bool,
	items: [<HistoryItem>], // this array is unsorted (dont ask why), sort on client by started_at
	users: {<User>, ...}, // dict of callee/caller
	topics: {<Topic>, ...},
	page: <Paginator>,
}
```

#### GET /history/:id [access_token]

For Callee: rate*duration/60+base_fee

For Caller: rate*duration/60-conspiracy_fee

Reply:

```js
{
	success: bool,
	item: {
		cost: int, // total cost in cents, 0 if call was free
		rate: int, // proposed rate in cents, 0 if call was free
		currency: string, // 3-letter currency code
		duration: int, // total duration in seconds
		?base_fee: int, // CALLEE ONLY, only when applicable, base fee in cents, only for callee, in cents
		?conspiracy_fee: int, // CALLER ONLY, only when applicable, conspiracy fee, usually 10% of `cost`, specified in cents here
		?charge: { // ONLY FOR CALLEE
			paid: bool, // wether invoice is paid or not
			amount: int,
			currency: string,
			?card_last_four: string, // only if paid: true
			?card_brand: string, // only if paid: true
		},
	}
}
```


## Endorsement

#### POST /user/:user_id/endorse/:topic_id [access_token]

Endorse :user on :topic

Reply:

```js
{
	success: bool
}
```


## Notifications

#### POST /notifications [access_token]

Register device for current user with specified token

Request:

```js
{
	token: string // device token as provided by apple/android
	?frequency: string // always,hourly,daily,weekly (how ofter to notify customer)
}
```

Reply:

```js
{
	success: bool
}
```

#### DELETE /notifications [access_token]

Unregister (disable) specified token for current user

Request:

```js
{
	token: string // device token as provided by apple/android
}
```

Reply:

```js
{
	success: bool
}
```

#### POST /debug/pusher/:user_id/:card_id [access_token]

Instantly send push notification to user :user on card :card

User must have registered device tokens

Reply:

```js
{
	success: bool
}
```
