// pusher := NewPusher()
// go pusher.Work()
package main

import (
	"fmt"
	"log"
	"net/http"
	"sync"
	"time"

	"github.com/RobotsAndPencils/buford/certificate"
	"github.com/RobotsAndPencils/buford/payload"
	"github.com/RobotsAndPencils/buford/push"
	"github.com/Sirupsen/logrus"
)

// Pusher ...
type Pusher struct {
	w        *Worker
	prodPool sync.Pool
	devPool  sync.Pool
}

const (
	taskPushCardNotificationString = "push_card_notification"
	// taskPushCardClearNotificationString = "push_card_clear_notification"
)

var pusher *Pusher

func pusherSetup() {
	pusher = NewPusher()
}

// NewPusher ...
func NewPusher() *Pusher {
	p := &Pusher{}
	w, err := NewWorker("apns", 3)
	if err != nil {
		panic(err)
	}

	w.Register(taskPushCardNotificationString, taskPushCardNotification)
	// w.Register(taskPushCardClearNotificationString, taskPushCardClearNotification)

	p.w = w
	p.prodPool = sync.Pool{
		New: func() interface{} {
			cert, key, err := certificate.Load(config.APNS.ProdCert, config.APNS.ProdPass)
			if err != nil {
				log.Fatal(err)
			}

			client, err := push.NewClient(certificate.TLS(cert, key))
			if err != nil {
				log.Fatal(err)
			}

			logrus.Debugf("apns: new production client")

			return client
		},
	}

	p.devPool = sync.Pool{
		New: func() interface{} {
			cert, key, err := certificate.Load(config.APNS.DevCert, config.APNS.DevPass)
			if err != nil {
				log.Fatal(err)
			}

			client, err := push.NewClient(certificate.TLS(cert, key))
			if err != nil {
				log.Fatal(err)
			}

			logrus.Debugf("apns: new development client")

			return client
		},
	}

	return p
}

// Work runs worker async work task
func (p *Pusher) Work() {
	p.w.Work()
}

func taskPushCardNotification(userID uint, args JSON) error {
	log := logrus.WithFields(logrus.Fields{
		"type": "push_card_notification",
		"user": userID,
	})

	mu := locker.NewMutex(fmt.Sprintf("pusher:card_live:%v", userID), time.Minute)
	if err := mu.Lock(); err != nil {
		switch {
		case err == ErrRedLockFailed:
			// other resource locked this user, skip
			return nil
		default:
			// some redis / other internal error
			return err
		}
	}
	defer mu.Unlock()

	cardIDf, ok := args["card_id"].(float64)
	if !ok {
		log.Warnf("bad args: %#v (card id %v)\n", userID, args, cardIDf)
		return nil
	}
	cardID := uint(cardIDf)
	log = log.WithField("card", cardID)

	// check the expires argument if present
	if expires, ok := args["expires"].(float64); ok && time.Now().Unix() > int64(expires) {
		log.Debugf("p user%d notification expired\n", userID)
		return nil
	}

	user, err := MustGetUser(userID)
	if err != nil {
		return err
	}

	if !user.CanNotify() {
		nextAt := (user.NextNotificationAt.Sub(time.Now()) / time.Second) * time.Second
		if nextAt < 0 {
			nextAt = 0
		}
		log.Infof("p user%d skipped (notifications %v next in %s)\n", user.ID, user.Notifications, nextAt)
		return nil
	}

	card, err := MustGetCard(cardID)
	if err != nil {
		return err
	}

	devices, err := user.Devices()
	if err != nil {
		return err
	}

	notified := false

	for _, device := range devices {
		if !device.CanNotify() {
			log.Debugf("device%d skipped", device.ID)
			continue
		}

		if err := mu.Extend(); err != nil && err != ErrRedLockFailed {
			return err
		}

		var pool *sync.Pool

		host := push.Production
		if device.IsSandbox {
			host = push.Development
		}

		if device.IsSandbox {
			pool = &pusher.devPool
		} else {
			pool = &pusher.prodPool
		}

		// remember to pool.Put() it later
		client := pool.Get().(*http.Client)
		service := push.Service{Client: client, Host: host}

		p := payload.APS{ContentAvailable: true}
		pm := p.Map()
		pm["type"] = "card_live"
		data, err := card.ToFullJSON()
		if err != nil {
			pool.Put(client)
			return err
		}
		pm["data"] = data

		headers := &push.Headers{
			Expiration: time.Now().Add(20 * time.Second),
			Topic:      config.APNS.Topic,
		}

		id, err := service.Push(device.Token, headers, pm)
		// pool.Put() must be here to avoid leaking of clients in case of errors
		pool.Put(client)

		if err != nil {
			switch err {
			// delete token on these errors
			case push.ErrBadDeviceToken, push.ErrMissingDeviceToken, push.ErrUnregistered:
				log.Infof("device%d token error: %v", device.ID, err)
				if err = user.DisableNotifications(device.Token); err != nil {
					log.Warnf("error disabling notifications")
				}

				// retry request on these errors (gets pushed back into nsqd)
			case push.ErrIdleTimeout, push.ErrShutdown, push.ErrInternalServerError, push.ErrServiceUnavailable:
				log.Warnf("device%d APNS error: %v (retry)", device.ID, err)
				return err

			default:
				log.Warnf("device%d unexpected error: %v (ignored)", device.ID, err)
			}
		} else {
			log.Debugf("device%d notification ok, id = %v", device.ID, id)

			if err := device.MarkAsNotified(); err != nil {
				log.Warnf("device%d error marking as notified: %v", device.ID, err)
			}

			notified = true
		}
	}

	if notified {
		if err := user.MarkAsNotified(); err != nil {
			log.Warnf("error marking user as notified (ignored): %v", err)
		}
	}

	return nil
}

func taskPushCardNotificationAsync(userID, cardID uint) error {
	args := JSON{
		"card_id": cardID,
		"expires": time.Now().Add(10 * time.Second).Unix(),
	}
	return pusher.w.PerformAsync(taskPushCardNotificationString, userID, args)
}

// func taskPushCardClearNotification(deviceID uint, args JSON) error {
// 	mu := locker.NewMutex(fmt.Sprintf("pusher:card_clear:%v", deviceID), time.Minute)
// 	if err := mu.Lock(); err != nil {
// 		switch {
// 		case err == ErrRedLockFailed:
// 			// other resource locked this user, skip
// 			return nil
// 		default:
// 			// some redis / other internal error
// 			return err
// 		}
// 	}
// 	defer mu.Unlock()
//
// 	device, err := MustGetDevice(deviceID)
// 	if err != nil {
// 		return err
// 	}
//
// 	if !device.IsEnabled {
// 		log.Printf("clear: device%d disabled", deviceID)
// 		return nil
// 	}
//
// 	// choose pool
// 	var pool *sync.Pool
//
// 	host := push.Production
// 	if device.IsSandbox {
// 		host = push.Development
// 	}
//
// 	if device.IsSandbox {
// 		pool = &pusher.devPool
// 	} else {
// 		pool = &pusher.prodPool
// 	}
//
// 	// remember to pool.Put() it later
// 	client := pool.Get().(*http.Client)
// 	service := push.Service{Client: client, Host: host}
//
// 	p := payload.APS{ContentAvailable: true}
// 	pm := p.Map()
// 	pm["type"] = "card_clear"
//
// 	headers := &push.Headers{
// 		Expiration: time.Now().Add(2 * time.Minute),
// 		Topic:      config.APNS.Topic,
// 	}
//
// 	id, err := service.Push(device.Token, headers, pm)
// 	// pool.Put() must be here to avoid leaking of clients in case of errors
// 	pool.Put(client)
//
// 	return nil
// }
//
// func taskPushCardClearNotificationAsync(deviceID uint) error {
// 	return pusher.w.PerformAsync(taskPushCardClearNotificationString, deviceID, nil)
// }
