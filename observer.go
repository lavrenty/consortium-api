package main

import (
	"math/rand"
	"sync"

	"github.com/Sirupsen/logrus"
)

// Observer ...
type Observer struct {
	mu sync.Mutex

	token     int64
	dashboard *Dashboard
	log       *logrus.Entry
	ids       map[uint]bool

	M chan Meta // meta on observed cards
}

// NewObserver ...
func (d *Dashboard) NewObserver() (*Observer, error) {
	o := &Observer{
		token:     rand.Int63(),
		dashboard: d,
		ids:       make(map[uint]bool, 0),
		M:         make(chan Meta, 10),
	}

	o.log = d.log.WithFields(logrus.Fields{
		"otoken":  o.token,
		"context": "observer",
	})

	return o, nil
}

// Add card to watchlist, updates will be sent over .C
// TODO: avoid duplicates!
func (o *Observer) Add(ids ...uint) {
	// o.log.Debugf("observer add %+v", ids)

	o.dashboard.omu.Lock()
	for _, id := range ids {
		o.dashboard.observers[id] = append(o.dashboard.observers[id], o)
	}
	o.dashboard.omu.Unlock()

	// mark as observed
	o.mu.Lock()
	for _, id := range ids {
		o.ids[id] = true
	}
	o.mu.Unlock()

}

// Remove card from watchlist, updates will be sent over .C
func (o *Observer) Remove(ids ...uint) {
	// o.log.Debugf("observer remove %+v", ids)

	o.dashboard.omu.Lock()
	for _, id := range ids {
		for i, oo := range o.dashboard.observers[id] {
			if oo == o {
				o.dashboard.observers[id] = append(o.dashboard.observers[id][:i], o.dashboard.observers[id][i+1:]...)
				break
			}
		}
	}
	o.dashboard.omu.Unlock()

	o.mu.Lock()
	for _, id := range ids {
		o.ids[id] = false
		delete(o.ids, id)
	}
	o.mu.Unlock()

}

// Stop ...
func (o *Observer) Stop() {
	// o.log.Debugf("observer stop")

	ids := []uint{}

	o.mu.Lock()
	for id := range o.ids {
		if o.ids[id] {
			ids = append(ids, id)
		}
	}
	o.mu.Unlock()

	o.Remove(ids...)

	// close(o.M)
}
