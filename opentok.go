package main

import (
	"crypto/hmac"
	"crypto/sha1"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"
)

const (
	opentokTokenPrefix = "T1=="
)

// OpenTok data
type OpenTok struct {
	SessionID string
}

// GetOpenTok creates a new opentok session through opentok API
//
func GetOpenTok() (OpenTok, error) {
	opentok := OpenTok{}
	client := &http.Client{}
	req, err := http.NewRequest("POST", "https://api.opentok.com/session/create", strings.NewReader("p2p.preference=enabled"))
	if err != nil {
		return opentok, err
	}
	req.Header.Add("Accept", "application/json")
	req.Header.Add("X-TB-PARTNER-AUTH", fmt.Sprintf("%s:%s", config.Opentok.APIKey, config.Opentok.Secret))

	res, err := client.Do(req)
	if err != nil {
		return opentok, err
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return opentok, err
	}

	if res.StatusCode != 200 {
		return opentok, fmt.Errorf("opentok failed with code %d (%s)", res.StatusCode, body)
	}

	type opentokJSON struct {
		SessionID string `json:"session_id"`
		PartnerID string `json:"parnter_id"`
		CreateDT  string `json:"create_dt"`
		URL       string `json:"media_server_url"`
	}

	toks := []opentokJSON{}

	if err := json.Unmarshal(body, &toks); err != nil {
		return opentok, err
	}

	if len(toks) == 0 || len(toks[0].SessionID) == 0 {
		return opentok, fmt.Errorf("opentok invalid session")
	}
	opentok.SessionID = toks[0].SessionID

	return opentok, nil
}

// NewOpenTokFromString creates new opentok instance from string
func NewOpenTokFromString(sessionID string) OpenTok {
	return OpenTok{SessionID: sessionID}
}

// Token generates token based on session id
// ref https://github.com/opentok/Opentok-Ruby-SDK/tree/master/lib/opentok
//
// TOKEN_SENTINEL = "T1=="
//  ROLES = { subscriber: "subscriber", publisher: "publisher", moderator: "moderator" }
//
func (o OpenTok) Token(userID uint, d time.Duration) string {

	data := url.Values{
		"role":            {"publisher"},
		"session_id":      {o.SessionID},
		"create_time":     {strconv.FormatInt(time.Now().Unix(), 10)},
		"nonce":           {strconv.FormatFloat(rand.Float64(), 'E', -1, 64)},
		"expire_time":     {strconv.FormatInt(time.Now().Add(d).Unix(), 10)},
		"connection_data": {fmt.Sprintf("{\"user_id\": %d}", userID)},
	}

	sig := hmac.New(sha1.New, []byte(config.Opentok.Secret))
	sig.Write([]byte(data.Encode()))

	meta := url.Values{
		"partner_id": {config.Opentok.APIKey},
		"sig":        {hex.EncodeToString(sig.Sum(nil))},
	}

	token := fmt.Sprintf("%s%s", opentokTokenPrefix, base64.StdEncoding.EncodeToString([]byte(fmt.Sprintf("%s:%s", meta.Encode(), data.Encode()))))

	return token
}
