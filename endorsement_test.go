package main

import "testing"

func testCheckEndorsement(t *testing.T, user1 *User, user2 *User, topic *Topic, val bool) {
	es, err := user1.GetEndorsementsBy(user2)
	if err != nil {
		t.Fatal(err)
	}
	ok := false
	for _, e := range es {
		if e.TopicID == topic.ID && e.IsEndorsed.Valid && e.IsEndorsed.Bool == val {
			ok = true
		}
	}
	if !ok {
		t.Fatal("endorsement failed")
	}
}

func TestUserEndorse(t *testing.T) {
	user1 := testGetUser(t, 1)
	user2 := testGetUser(t, 2)
	topic := testGetTopic(t, 2)

	vt := true
	vf := false

	// should be success
	if err := user1.Endorse(user2, topic, &vt); err != nil {
		t.Fatal(err)
	}
	testCheckEndorsement(t, user2, user1, topic, true)

	// endorsing again must produce error
	if err := user1.Endorse(user2, topic, &vt); err != nil {
		t.Fatal(err)
	}
	testCheckEndorsement(t, user2, user1, topic, true)

	// changing value must not produce error
	if err := user1.Endorse(user2, topic, &vf); err != nil {
		t.Fatal(err)
	}
	testCheckEndorsement(t, user2, user1, topic, false)

	// changing value must not produce error
	if err := user1.Endorse(user2, topic, nil); err != nil {
		t.Fatal(err)
	}
	testCheckEndorsement(t, user2, user1, topic, false)
}

func TestRebuildEndorsements(t *testing.T) {
	u := testGetUser(t, 1)

	if err := u.RebuildEndorsements(); err != nil {
		t.Fatal(err)
	}

	t1 := testGetTopic(t, 1)
	t2 := testGetTopic(t, 2)
	t3 := testGetTopic(t, 3)

	f := testCreateUser(t)
	if err := f.Subscribe(t1, false); err != nil {
		t.Fatal(err)
	}
	if err := f.Subscribe(t2, false); err != nil {
		t.Fatal(err)
	}
	if err := f.Subscribe(t3, false); err != nil {
		t.Fatal(err)
	}

	u.AddFriend(f, "fb")
	u.AddFriend(testGetUser(t, 3), "fb")
	u.AddFriend(testGetUser(t, 4), "fb")

	if err := u.RebuildEndorsements(); err != nil {
		t.Fatal(err)
	}
}
