package main

import (
	"fmt"
	"time"

	"github.com/Sirupsen/logrus"
	"github.com/gin-gonic/gin"
	"github.com/stripe/stripe-go"
	"github.com/stripe/stripe-go/event"
)

func handleStripeAccountWebhook(c *gin.Context) {
	logger := c.MustGet("logger").(*logrus.Entry)

	key := c.Param("key")
	if key != config.Stripe.WebhookKey {
		c.HTML(403, "403.html", nil)
		return
	}

	untrustedEvent := stripe.Event{}
	if err := c.BindJSON(&untrustedEvent); err != nil {
		c.HTML(500, "500.html", nil)
		return
	}

	logger.Debugf("event %v %v from stripe: %#v", untrustedEvent.ID, untrustedEvent.Type, untrustedEvent)

	evt, err := event.Get(untrustedEvent.ID, &stripe.Params{})
	if err != nil {
		logger.Warnf("invaild event from stripe %v: %#v", untrustedEvent.ID, err)

		apiErrorInternal(c, err)
		return
	}

	switch evt.Type {
	// Occurs whenever a new customer is created, updated
	case "customer.updated", "customer.created":
		// not interested, ignore

		// Occurs whenever a new source is created for the customer
	case "customer.source.created", "customer.source.updated":
		// not interested, ignore

		// everything else
	default:
		msg := fmt.Sprintf("event %v not handled", evt.Type)
		sentry.CaptureMessage(msg, map[string]string{
			"type":          "webhook-account",
			"event-id":      evt.ID,
			"event-type":    evt.Type,
			"event-account": evt.UserID,
			"event-live":    fmt.Sprintf("%v", evt.Live),
		})
		logger.Warn(msg)
	}

	apiSuccess(c)
}

func handleStripeConnectWebhook(c *gin.Context) {
	logger := c.MustGet("logger").(*logrus.Entry)

	key := c.Param("key")
	if key != config.Stripe.WebhookKey {
		c.HTML(403, "403.html", nil)
		return
	}

	untrustedEvent := stripe.Event{}
	if err := c.BindJSON(&untrustedEvent); err != nil {
		c.HTML(500, "500.html", nil)
		return
	}

	logger.Debugf("event %v %v from stripe: %#v", untrustedEvent.ID, untrustedEvent.Type, untrustedEvent)

	sa, err := GetStripeAccount(untrustedEvent.UserID)
	if err != nil {
		apiErrorInternal(c, err)
		return
	}

	if sa == nil {
		apiError(c, 404, "stripe account not found", err)
		return
	}
	user, err := MustGetUser(sa.UserID)
	if err != nil {
		apiErrorInternal(c, err)
		return
	}

	sc := sa.GetStripeClient()

	evt, err := sc.Events.Get(untrustedEvent.ID, &stripe.Params{})
	if err != nil {
		logger.Warnf("invaild event from stripe %v: %#v", untrustedEvent.ID, err)

		apiErrorInternal(c, err)
		return
	}

	switch evt.Type {
	// Occurs whenever an account status or property has changed
	case "account.updated":
		// here we just grab the latest data from the server and ignore everything that came in with the event
		update, err := sc.Account.Get()
		if err != nil {
			apiErrorInternal(c, err)
			return
		}

		sa.DetailsSubmitted = update.DetailsSubmitted
		sa.TransfersEnabled = update.TransfersEnabled

		if update.Verification.DisabledReason == "" {
			sa.DisabledReason.Scan(nil)
		} else {
			sa.DisabledReason.Scan(update.Verification.DisabledReason)
		}

		if update.Verification.Due != nil {
			sa.DueBy.Scan(time.Unix(*update.Verification.Due, 0))
		} else {
			sa.DueBy.Scan(nil)
		}

		if err := updateFields("stripe_accounts", sa, []string{"transfers_enabled", "details_submitted", "disabled_reason", "due_by"}); err != nil {
			apiErrorInternal(c, err)
			return
		}

		// Occurs whenever an external account is created
		// describes an external account (e.g., card or bank account)
	case "account.external_account.created", "account.external_account.deleted", "account.external_account.updated":
		// here we just grab the latest data from the server and ignore everything that came in with the event
		update, err := sc.Account.Get()
		if err != nil {
			apiErrorInternal(c, err)
			return
		}

		sa.DetailsSubmitted = update.DetailsSubmitted
		sa.TransfersEnabled = update.TransfersEnabled

		if err := updateFields("stripe_accounts", sa, []string{"transfers_enabled", "details_submitted"}); err != nil {
			apiErrorInternal(c, err)
			return
		}

		// Occurs whenever your Stripe balance has been updated (e.g. when a charge collected is available to be paid out). By default, Stripe will automatically transfer any funds in your balance to your bank account on a daily basis.
	case "balance.available":
		// just update current account balance with latest data. ignore event data.
		update, err := sc.Balance.Get(&stripe.BalanceParams{})
		if err != nil {
			apiErrorInternal(c, err)
			return
		}

		for _, amount := range update.Available {
			user.Balance = int(amount.Value)
			user.BalanceCurrency = string(amount.Currency)
		}

		for _, amount := range update.Pending {
			user.PendingBalance = int(amount.Value)
			user.PendingBalanceCurrency = string(amount.Currency)
		}

		if err := updateFields("users", user, []string{"balance", "balance_currency", "pending_balance", "pending_balance_currency"}); err != nil {
			apiErrorInternal(c, err)
			return
		}

		// everything else
	default:
		msg := fmt.Sprintf("event %v not handled", evt.Type)
		sentry.CaptureMessage(msg, map[string]string{
			"type":          "webhook-connect",
			"event-id":      evt.ID,
			"event-type":    evt.Type,
			"event-account": evt.UserID,
			"event-live":    fmt.Sprintf("%v", evt.Live),
		})
		logger.Warn(msg)
	}

	apiSuccess(c)
}
