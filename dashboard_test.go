package main

import (
	"testing"
	"time"
)

func TestDashboard(t *testing.T) {
	user1 := testGetUser(t, 1)
	user2 := testGetUser(t, 2)
	user3 := testGetUser(t, 3)
	card1 := testCreateCard(t, user1, []string{"Bitcoin", "Internet"})
	card2 := testCreateCard(t, user2, []string{"Technology", "Movies"})

	d1, err := NewDashboard(R)
	if err != nil {
		t.Fatal(err)
	}
	defer d1.Stop()

	r1, err := d1.NewRadar(card1.ID)
	if err != nil {
		t.Fatal(err)
	}
	defer r1.Stop()

	d2, err := NewDashboard(R)
	if err != nil {
		t.Fatal(err)
	}
	defer d2.Stop()

	o, err := d2.NewObserver()
	if err != nil {
		t.Fatal(err)
	}
	defer o.Stop()
	o.Add(card1.ID)

	r2, err := d2.NewRadar(card2.ID)
	if err != nil {
		t.Fatal(err)
	}

	o.Add(card2.ID)

	r2.Stop()

	r2, err = d2.NewRadar(card2.ID)
	if err != nil {
		t.Fatal(err)
	}

	// fetch data from d1
	for i := 0; i < 10; i++ {
		_, err := d1.Fetch(&FetchOptions{
			User:   user2,
			Ignore: make(map[uint]bool, 0),
			Topics: []Topic{},
			Limit:  5,
		})
		if err != nil {
			t.Fatal(err)
		}
	}

	// fetch more from d2
	for i := 0; i < 10; i++ {
		_, err := d2.Fetch(&FetchOptions{
			User:   user3,
			Ignore: make(map[uint]bool, 0),
			Topics: []Topic{},
			Limit:  5,
		})
		if err != nil {
			t.Fatal(err)
		}
	}

	r2.Stop()
	time.Sleep(100 * time.Millisecond)

	d1.mmu.RLock()
	d2.mmu.RLock()

	// compare d1.metas <> d2.metas
	for cardID, m := range d1.metas {
		if _, ok := d2.metas[cardID]; !ok {
			t.Fatalf("d2 missing card %v (d1.metas: %+v, d2.metas: %+v)\n", cardID, d1.metas, d2.metas)
		}

		if m.Views != d2.metas[cardID].Views {
			t.Fatalf("d1.metas[%d] != d2.metas[%d]: %+v vs. %+v\n", cardID, cardID, *m, *d2.metas[cardID])
		}

		t.Logf("meta card %d ok: %+v\n", cardID, *m)
	}

	t.Logf("d1.metas == d2.metas: %+v == %+v\n", d1.metas, d2.metas)

	d1.mmu.RUnlock()
	d2.mmu.RUnlock()
	// t.Log("dashboard test done\n")
}
