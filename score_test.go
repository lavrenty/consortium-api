package main

import "testing"

func TestScore(t *testing.T) {
	user := testGetUser(t, 1)
	score, err := GetScore(user.ID)

	if err != nil {
		t.Fatal(err)
	}

	if err := score.Rebuild(); err != nil {
		t.Fatal(err)
	}
}
