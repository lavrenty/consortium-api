package main

import (
	"database/sql"
	"time"
)

// TxTypeCall ...
const (
	TxTypeCall = "call"
	// TxTypeRefund = "refund"
	// TxTypeAdjustment = "adjustment"
	// TxTypeTransfer   = "transfer"
)

// Transaction is for account balance
// `call` - earnings for your call
// `refund` - refund in case user is fined
// `adjustment` - internal funds transfer to top up user account in case there is not enough real amount in users balance
// `transfer*` - payouts
type Transaction struct {
	ID     uint
	UserID uint

	Amount   int
	Net      int
	Currency string

	Type        string // Type of the transaction, one of: call, refund, adjustment, transfer, transfer_cancel, transfer_refund, or transfer_failure
	Description string

	CallID    sql.NullInt64
	InvoiceID sql.NullInt64

	UpdatedAt time.Time
	CreatedAt time.Time
}

// // NewTransaction is the only way to create new transaction properly
// func NewTransaction(user *User, amount int, net int) *Transaction {
// 	if user == nil {
// 		panic("NewTransaction: user == nil")
// 	}
//
// 	if user.ID == 0 {
// 		panic("NewTransaction: user.ID == 0")
// 	}
// 	if call.ID == 0 {
// 		panic("NewTransaction: call.ID == 0")
// 	}
// 	if invoice.ID == 0 {
// 		panic("NewTransaction: invoice.ID == 0")
// 	}
//
// 	return &Transaction{
// 		initialized: true,
// 		UserID:      user.ID,
// 		CallID:      call.ID,
// 		InvoiceID:   invoice.ID,
// 		Amount:      amount,
// 		Currency:    "usd",
// 	}
// }

// GetTransaction reads tx object from the database
func GetTransaction(ID interface{}) (*Transaction, error) {
	tx := new(Transaction)
	if err := db.Get(tx, "SELECT * FROM transactions WHERE id = $1", ID); err != nil {
		switch {
		case err == sql.ErrNoRows:
			return nil, nil
		default:
			return nil, err
		}
	}

	return tx, nil
}

// GetTransactionByCallID ...
func GetTransactionByCallID(callID uint) (*Transaction, error) {
	tx := new(Transaction)
	if err := db.Get(tx, "SELECT * FROM transactions WHERE call_id = $1", callID); err != nil {
		switch {
		case err == sql.ErrNoRows:
			return nil, nil
		default:
			return nil, err
		}
	}

	return tx, nil
}

// Save transaction to database
func (tx *Transaction) Save() error {
	return save("transactions", tx)
}
