// well, this is not red lock as proposed by antirez, but
// refs http://redis.io/topics/distlock, https://github.com/go-redsync/redsync/tree/v1
package main

import (
	"encoding/base64"
	"errors"
	"fmt"
	"math/rand"
	"strconv"
	"sync"
	"time"

	"gopkg.in/redis.v3"
)

// ErrRedLockFailed ...
var ErrRedLockFailed = errors.New("redlock: failed to acquire lock")

const redScriptDelete = `if redis.call("get", KEYS[1]) == ARGV[1] then
return redis.call("del", KEYS[1])
else
return 0
end`

const redScriptTouch = `if redis.call("GET", KEYS[1]) == ARGV[1] then
	return redis.call("SET", KEYS[1], ARGV[1], "XX", "PX", ARGV[2])
else
	return "ERR"
end
`

// RedLock ...
type RedLock struct {
	redis *redis.Client

	namespace string

	deleteSha string
	touchSha  string
}

// NewRedLock ...
func NewRedLock(r *redis.Client, namespace string) (*RedLock, error) {
	l := &RedLock{redis: r, namespace: namespace}

	sha, err := r.ScriptLoad(redScriptDelete).Result()
	if err != nil {
		return nil, err
	}
	l.deleteSha = sha

	sha, err = r.ScriptLoad(redScriptTouch).Result()
	if err != nil {
		return nil, err
	}
	l.touchSha = sha

	return l, nil
}

// RedMutex ...
type RedMutex struct {
	mu sync.Mutex

	lock *RedLock

	name   string
	value  string
	expiry time.Duration
}

// NewMutex ...
func (l *RedLock) NewMutex(name string, expiry time.Duration) *RedMutex {
	return &RedMutex{lock: l, name: fmt.Sprintf("%s:%s", l.namespace, name), expiry: expiry}
}

func (m *RedMutex) genValue() (string, error) {
	b := make([]byte, 16)
	_, err := rand.Read(b)
	if err != nil {
		return "", err
	}
	return base64.StdEncoding.EncodeToString(b), nil
}

func (m *RedMutex) acquire(value string) (bool, error) {
	return m.lock.redis.SetNX(m.name, value, m.expiry).Result()
}

func (m *RedMutex) release(value string) error {
	return R.EvalSha(m.lock.deleteSha, []string{m.name}, []string{value}).Err()
}

func (m *RedMutex) touch(value string) (bool, error) {
	expiry := strconv.FormatInt(int64(m.expiry/time.Millisecond), 10)
	res, err := R.EvalSha(m.lock.touchSha, []string{m.name}, []string{value, expiry}).Result()
	if err != nil {
		return false, err
	}

	return res.(string) == "OK", nil
}

// Lock ...
func (m *RedMutex) Lock() error {
	m.mu.Lock()
	defer m.mu.Unlock()

	val, err := m.genValue()
	if err != nil {
		return err
	}

	m.value = val
	ok, err := m.acquire(m.value)
	if err != nil {
		return err
	}

	if !ok {
		return ErrRedLockFailed
	}

	return nil
}

// Unlock ...
func (m *RedMutex) Unlock() error {
	m.mu.Lock()
	defer m.mu.Unlock()

	return m.release(m.value)
}

// Extend ...
func (m *RedMutex) Extend() error {
	m.mu.Lock()
	defer m.mu.Unlock()

	ok, err := m.touch(m.value)
	if err != nil {
		return err
	}

	if !ok {
		return ErrRedLockFailed
	}

	return nil
}
