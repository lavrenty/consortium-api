package main

import (
	"database/sql"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
)

// Topic object
type Topic struct {
	ID          uint
	Name        string
	Subscribers uint

	RefID    sql.NullInt64
	ParentID sql.NullInt64

	IsPublic   bool
	IsReviewed bool

	// user_topics relation embedded fields
	IsSuggested bool
	CallsCount  uint
	Rating      int
	Score       int

	CreatedAt time.Time
}

// ToJSON convers topic to json
func (t *Topic) ToJSON() JSON {
	j := JSON{
		"id":   t.ID,
		"type": "topic",
		"name": t.Name,
	}

	return j
}

// ToStatsJSON is ToJSON extension to include personalized statistics
func (t *Topic) ToStatsJSON() JSON {
	j := t.ToJSON()
	j["is_suggested"] = t.IsSuggested
	j["calls_count"] = t.CallsCount
	j["score"] = t.Score

	return j
}

// GetTopic returns topic by it's ID
// error is returned with there is connection / db internal error
// error is not returned when object is not found
// topic is nil if not found
func GetTopic(ID interface{}) (*Topic, error) {
	topic := new(Topic)
	if err := db.Get(topic, "SELECT * FROM topics WHERE id = $1", ID); err != nil {
		switch {
		case err == sql.ErrNoRows:
			return nil, nil
		default:
			return nil, err
		}
	}

	return topic, nil
}

// MustGetTopic ...
func MustGetTopic(ID interface{}) (*Topic, error) {
	topic, err := GetTopic(ID)
	if err != nil {
		return nil, err
	}
	if topic == nil {
		return nil, fmt.Errorf("topic %#v not found", ID)
	}
	return topic, nil
}

// GetTopics fetches multiple topics by their id and returns an array of results
func GetTopics(ids []uint) ([]Topic, error) {
	topics := []Topic{}

	// empty query
	if len(ids) <= 0 {
		return []Topic{}, nil
	}

	query, args, err := sqlx.In("SELECT DISTINCT * FROM topics WHERE id IN (?)", ids)
	if err != nil {
		return topics, err
	}
	if err := db.Select(&topics, db.Rebind(query), args...); err != nil {
		switch {
		case err == sql.ErrNoRows:
			return topics, nil
		default:
			return topics, err
		}
	}
	return topics, nil
}

// GetTopTopics does exactly what it means: returns 10 most recently popular topics
func GetTopTopics(limit int) ([]Topic, error) {
	topics := []Topic{}

	err := db.Select(&topics, "SELECT * FROM topics ORDER BY subscribers DESC LIMIT $1", limit)

	return topics, err
}

// GetTopTopicIds does exactly what it means: returns 10 most recently popular topics
func GetTopTopicIds(limit int) ([]uint, error) {
	topicIds := []uint{}

	if err := db.Select(&topicIds, "SELECT id FROM topics ORDER BY subscribers DESC LIMIT $1", limit); err != nil {
		return nil, err
	}

	return topicIds, nil
}

// FindOrCreateTopicByName create or find topic by name
func FindOrCreateTopicByName(name string) (*Topic, error) {
	topic := new(Topic)

	err := transaction(func(tx *sqlx.Tx) (err error) {
		if err = db.Get(topic, "SELECT * FROM topics WHERE name = $1", name); err != nil {
			switch {
			case err == sql.ErrNoRows:
				err = db.Get(topic, "INSERT INTO topics (name, is_public, is_reviewed) VALUES ($1, 't', 't') RETURNING *", name)
			default:
				topic = nil
			}
		}
		return
	})

	return topic, err
}

// TopicsAutocomplete returns array of found topics, but up to `limit`
// Select most relevant topics for provided query, ordered by popularity
func TopicsAutocomplete(query string, limit uint) ([]Topic, error) {
	topics := []Topic{}

	if err := db.Select(&topics, "SELECT * FROM topics WHERE is_public = 't' AND name ILIKE $1 ORDER BY subscribers DESC LIMIT $2",
		fmt.Sprintf("%s%%", query), limit); err != nil {
		return nil, err
	}

	return topics, nil
}

// IncCallsCount on user/topic relation
func (t *Topic) IncCallsCount(user *User) error {
	if _, err := db.Exec("UPDATE users_topics SET calls_count = calls_count + 1 WHERE user_id = $1 AND topic_id = $2", user.ID, t.ID); err != nil {
		return err
	}

	return nil
}

// Estimate ...
type Estimate struct {
	Rate       uint
	UsersCount uint
	Score      int
}

// ToJSON ...
func (e *Estimate) ToJSON() JSON {
	j := JSON{
		"type":        "estimate",
		"rate":        e.Rate,
		"currency":    "usd",
		"users_count": e.UsersCount,
		"score":       e.Score,
	}

	return j
}

// TopicsRates ...
// estimate = [
//   { rate: 0.0, count: 100 },
//   { rate: 0.1, count: 30 },
// ]
func TopicsRates(topicIds []uint, lang string) ([]Estimate, error) {
	var res []struct {
		ID                 uint `db:"id"`
		CallerScore        int  `db:"caller_score"`
		TopicsAverageScore int  `db:"topics_average_score"`
	}

	minimumTopicsRequired := len(topicIds) / 2
	if minimumTopicsRequired <= 0 {
		minimumTopicsRequired = 1
	}

	query, args, err := sqlx.In(`
		SELECT
		  u.id AS id,
		  u.score AS caller_score,
		  ut.score AS topics_average_score
		FROM (
		  SELECT
		    users.id AS id,
		    scores.caller_score AS score
		  FROM users
		  LEFT JOIN scores ON
		    users.id = scores.user_id
		  WHERE
		    scores.caller_score > 0 AND
		    users.is_disabled = false AND
		    users.languages LIKE ? AND
		    users.id IN (
		      SELECT
		        user_id
		      FROM users_topics
		      WHERE
		        is_deleted = false AND
		        is_suggested = false AND
		        score >= 0 AND
		        topic_id in (?)
		      GROUP BY user_id
		      HAVING count(*) >= ?
		    )
		  ) AS u
		LEFT JOIN LATERAL (
		  SELECT
		    sum(score) AS score
		  FROM users_topics
		  WHERE
		    user_id = u.id AND
		    topic_id in (?)
		  GROUP BY
		    user_id
		) ut ON true
		`, fmt.Sprintf("%%%s%%", lang), topicIds, minimumTopicsRequired, topicIds)
	if err != nil {
		return nil, err
	}
	if err := db.Select(&res, db.Rebind(query), args...); err != nil {
		switch {
		case err == sql.ErrNoRows:
			return nil, nil
		default:
			return nil, err
		}
	}

	// create and fill basic array
	estimates := make([]Estimate, len(DefaultRates))
	for i, rate := range DefaultRates {
		estimates[i].Rate = rate
	}

	if len(res) == 0 {
		return estimates, nil
	}

	// get min/max
	maxScore := -1
	minScore := -1
	for _, r := range res {
		if r.TopicsAverageScore > maxScore {
			maxScore = r.TopicsAverageScore
		}
		if minScore < 0 || r.TopicsAverageScore < minScore {
			minScore = r.TopicsAverageScore
		}
	}

	// count by segments
	for i := range DefaultRates {
		var requiredMinimumScore = minScore + ((maxScore-minScore)/len(DefaultRates))*i

		estimates[i].Score = requiredMinimumScore
		for _, r := range res {
			if r.TopicsAverageScore >= requiredMinimumScore {
				estimates[i].UsersCount++
			}
		}
	}

	return estimates, nil
}

// RebuildScore ...
func (t *Topic) RebuildScore(user *User) error {
	ut := &UserTopic{}

	if err := db.Get(ut, "SELECT * FROM users_topics WHERE user_id = $1 AND topic_id = $2", user.ID, t.ID); err != nil {
		switch {
		case err == sql.ErrNoRows:
			return nil
		default:
			return err
		}
	}

	// TODO: does this work?
	if err := db.Get(&ut.Rating, "SELECT COALESCE(sum(caller_rating - 3), 0) FROM calls LEFT JOIN cards_topics ON calls.card_id = cards_topics.card_id WHERE cards_topics.topic_id = $1 AND calls.duration > $2 AND calls.caller_id = $3 AND calls.is_hangup = true AND calls.caller_rating > 0", t.ID, CallTimeFree, user.ID); err != nil {
		return err
	}

	ePositive, eNegative := 0, 0
	if err := db.Get(&ePositive, "SELECT count(*) FROM endorsements WHERE user_id = $1 AND topic_id = $2 AND is_endorsed = true", user.ID, t.ID); err != nil {
		return err
	}
	if err := db.Get(&eNegative, "SELECT count(*) FROM endorsements WHERE user_id = $1 AND topic_id = $2 AND is_endorsed = false", user.ID, t.ID); err != nil {
		return err
	}

	ut.Score = ePositive*5 + -(eNegative * 3) + ut.Rating + int(ut.CallsCount)
	// logrus.Debugf("rebuild score user%d topic%d: rating %d, e+ %d, e- %d, score %d", user.ID, t.ID, ut.Rating, ePositive, eNegative, ut.Score)

	if _, err := db.NamedExec("UPDATE users_topics SET rating = :rating, score = :score WHERE user_id = :user_id AND topic_id = :topic_id", ut); err != nil {
		return err
	}

	return nil
}
