package main

import (
	"database/sql"
	"time"
)

// Report for spam / abuse etc
type Report struct {
	ID uint

	UserID uint
	CallID sql.NullInt64
	CardID sql.NullInt64

	Obj string

	ReportType string
	ReportText sql.NullString
	ReportIP   string

	IsResolved bool

	UpdatedAt time.Time
	CreatedAt time.Time
}

// Save report
func (r *Report) Save() error {
	return save("reports", r)
}
