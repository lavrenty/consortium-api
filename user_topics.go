package main

import (
	"database/sql"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/lib/pq"
)

// TopicSuggestableScore 2 endorsements or 10 calls or ~5 positive calls
const TopicSuggestableScore = 10

// UserTopic reflection from DB
type UserTopic struct {
	UserID            uint
	TopicID           uint
	IsDeleted         bool
	IsSuggested       bool
	SendNotifications sql.NullBool

	CallsCount uint
	Rating     int
	Score      int

	CardSeenAt pq.NullTime

	CreatedAt time.Time
}

// Topics returns all user topics excluding deleted (e.g. active + suggested)
func (u *User) Topics() ([]Topic, error) {
	topics := []Topic{}
	if err := db.Select(&topics, "SELECT t.*,ut.is_suggested,ut.calls_count,ut.rating,ut.score FROM topics AS t LEFT JOIN users_topics AS ut ON ut.topic_id = t.id WHERE ut.user_id = $1 AND ut.is_deleted = false", u.ID); err != nil {
		return nil, err
	}

	return topics, nil
}

// ActiveTopics returns all user topics excluding deleted (e.g. active + suggested)
func (u *User) ActiveTopics() ([]Topic, error) {
	topics := []Topic{}
	if err := db.Select(&topics, "SELECT t.*,ut.is_suggested,ut.calls_count,ut.rating,ut.score FROM topics AS t LEFT JOIN users_topics AS ut ON ut.topic_id = t.id WHERE ut.user_id = $1 AND ut.is_deleted = false AND ut.is_suggested = false", u.ID); err != nil {
		return nil, err
	}

	return topics, nil
}

// ActiveTopicIds returns ids of all user topics excluding deleted (e.g. active + suggested)
func (u *User) ActiveTopicIds() ([]uint, error) {
	topicIds := []uint{}
	if err := db.Select(&topicIds, "SELECT topic_id FROM users_topics WHERE user_id = $1 AND is_deleted = false AND is_suggested = false", u.ID); err != nil {
		return nil, err
	}

	return topicIds, nil
}

// SuggestedTopics returns up to 25 most scored suggested topics
func (u *User) SuggestedTopics(limit int) ([]Topic, error) {
	topics := []Topic{}

	if err := db.Select(&topics, "SELECT t.*,ut.is_suggested,ut.calls_count,ut.rating,ut.score FROM topics AS t LEFT JOIN users_topics AS ut ON ut.topic_id = t.id WHERE ut.user_id = $1 AND ut.is_deleted = false AND ut.is_suggested = true ORDER BY ut.score DESC LIMIT $2", u.ID, limit); err != nil {
		return nil, err
	}

	return topics, nil
}

// TopicsRecent returns recently used topics by this user on previous cards
func (u *User) TopicsRecent(limit int) ([]Topic, error) {
	topics := []Topic{}

	if err := db.Select(&topics, "SELECT t.* FROM topics AS t LEFT JOIN cards_topics AS tc ON tc.topic_id = t.id LEFT JOIN cards AS c ON tc.card_id = c.id WHERE c.user_id = $1 AND c.created_at > now()-INTERVAL '2 months' GROUP BY t.id LIMIT $2", u.ID, limit); err != nil {
		return nil, err
	}

	return topics, nil
}

// Subscribe user
func (u *User) Subscribe(t *Topic, suggest bool) error {
	return transaction(func(tx *sqlx.Tx) error {
		ut := &UserTopic{}

		if err := tx.Get(ut, "SELECT * FROM users_topics WHERE user_id = $1 AND topic_id = $2", u.ID, t.ID); err != nil {
			switch {
			case err == sql.ErrNoRows:
				// no user topics
				if _, err = tx.Exec("INSERT INTO users_topics (user_id, topic_id, is_deleted, is_suggested) VALUES ($1, $2, $3, $4)", u.ID, t.ID, false, suggest); err != nil {
					return err
				}
				t.Subscribers++
				if _, err = tx.Exec("UPDATE topics SET subscribers = subscribers + 1 WHERE id = $1", t.ID); err != nil {
					return err
				}
				return nil
			default:
				// internal error
				return err
			}
		}

		// there is already a users_topics entry, so update it

		// unhide if hidden is set to false
		if !suggest {
			ut.IsSuggested = false
		}
		if _, err := tx.NamedExec("UPDATE users_topics SET is_deleted = 'f', is_suggested = :is_suggested WHERE user_id = :user_id AND topic_id = :topic_id", ut); err != nil {
			return err
		}
		// if it was deleted, increment subscribers counter
		if ut.IsDeleted {
			t.Subscribers++
			if _, err := tx.Exec("UPDATE topics SET subscribers = subscribers + 1 WHERE id = $1", t.ID); err != nil {
				return err
			}
		}
		return nil
	})
}

// Unsubscribe user from topic
// We never delete users_topics associated with the user so we could always keep their rating / earnings on the topic
func (u *User) Unsubscribe(t *Topic) error {
	return transaction(func(tx *sqlx.Tx) (err error) {
		res, err := tx.Exec("UPDATE users_topics SET is_deleted = 't' WHERE user_id = $1 AND topic_id = $2", u.ID, t.ID)
		if err != nil {
			return err
		}

		ra, err := res.RowsAffected()
		if err != nil {
			return err
		}

		if ra > 0 {
			t.Subscribers--
			_, err = tx.Exec("UPDATE topics SET subscribers = subscribers - 1 WHERE id = $1", t.ID)
		}

		return
	})
}

// GetSuggestedTopic returns single topic that is suitable for suggesting for a user (in card mode only!)
// e.g. it does not return all suggestable topics for user, it just returns those that are ok to be suggested
// with a system card and follows the system card presentation logic with the card_seen_at timestamp
func (u *User) GetSuggestedTopic() (*Topic, error) {
	topic := new(Topic)

	if err := db.Get(topic, "SELECT t.*,ut.is_suggested,ut.calls_count,ut.rating,ut.score FROM topics AS t LEFT JOIN users_topics AS ut ON t.id = ut.topic_id WHERE ut.user_id = $1 AND ut.is_suggested = true AND ut.is_deleted = false AND (ut.card_seen_at IS NULL OR (ut.card_seen_at + interval '1 month') < now()) AND score >= $2 ORDER BY score DESC LIMIT 1", u.ID, TopicSuggestableScore); err != nil {
		switch {
		case err == sql.ErrNoRows:
			return nil, nil
		default:
			return nil, err
		}
	}

	return topic, nil
}

// UpdateSuggestedTopicCardAsSeen ...
func (u *User) UpdateSuggestedTopicCardAsSeen(t *Topic) error {
	if _, err := db.Exec("UPDATE users_topics SET card_seen_at = now() WHERE user_id = $1 AND topic_id = $2", u.ID, t.ID); err != nil {
		return err
	}
	return nil
}
