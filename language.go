package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/Sirupsen/logrus"
)

//go:generate go-bindata -o bindata.go languages.json

// Language map ISO 639-1
type Language struct {
	code2name map[string]string
	name2code map[string]string
	data      []struct {
		Code string
		Name string
	}
}

var languages = LoadLanguages("languages.json")

// LoadLanguages loads json file with langauges
func LoadLanguages(name string) *Language {
	l := &Language{}

	if err := json.Unmarshal(MustAsset("languages.json"), &l.data); err != nil {
		panic(err)
	}

	l.code2name = make(map[string]string, len(l.data))
	l.name2code = make(map[string]string, len(l.data))

	for _, item := range l.data {
		for i, name := range strings.Split(item.Name, "; ") {
			if i == 0 {
				if _, ok := l.code2name[item.Code]; ok {
					log.Panicf("languages: code %s is already set", item.Code)
				}
				l.code2name[item.Code] = name
			}

			if _, ok := l.name2code[name]; ok {
				log.Panicf("languages: name %s is already set", name)
			}
			l.name2code[name] = item.Code
		}
	}

	return l
}

// IsSupported ...
func (l Language) IsSupported(code string) bool {
	_, ok := l.code2name[code]
	return ok
}

// GetNameByCode ru -> Russian
func (l Language) GetNameByCode(code string) (string, bool) {
	name, ok := l.code2name[code[:2]]
	return name, ok
}

// GetCodeByName ...
func (l Language) GetCodeByName(name string) (string, bool) {
	code, ok := l.name2code[name]
	return code, ok
}

// Detect language by text ...
// TODO: send userIp for google to properly manage limits
func (l Language) Detect(text string) (string, error) {
	if config.Debug {
		return "en", nil
	}

	u, _ := url.Parse("https://www.googleapis.com/language/translate/v2/detect")
	q := u.Query()
	q.Set("key", config.Google.APIKey)
	q.Set("q", text)
	u.RawQuery = q.Encode()

	client := http.Client{
		Timeout: 5 * time.Second,
	}

	res, err := client.Get(u.String())
	if err != nil {
		return "", err
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return "", err
	}

	if res.StatusCode != 200 {
		logrus.Debugf("body: \n%s", body)
		return "", fmt.Errorf("got error from google translate api: %s", res.Status)
	}

	var detection struct {
		Data struct {
			Detections [][]struct {
				Language   string
				Confidence float64
			}
		}
	}
	if err := json.Unmarshal(body, &detection); err != nil {
		return "", err
	}
	if len(detection.Data.Detections) < 1 || len(detection.Data.Detections[0]) < 1 {
		return "", fmt.Errorf("buggy reply from transalate.detect %v", body)
	}
	lang := detection.Data.Detections[0][0].Language
	if _, ok := l.GetNameByCode(lang); !ok {
		return "", fmt.Errorf("detected language '%s' is unknown", lang)
	}

	return lang, nil
}
