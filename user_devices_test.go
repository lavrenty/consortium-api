package main

import "testing"

func TestUserDevices(t *testing.T) {
	u := testGetUser(t, 1)

	token := "1209342lkfjwwrkjl3;24j"
	if err := u.EnableNotifications(token); err != nil {
		t.Fatal(err)
	}

	devices, err := u.Devices()
	if err != nil {
		t.Fatal(err)
	}

	if len(devices) != 1 {
		t.Fatal("expected one device active")
	}

	if err = u.DisableNotifications(token); err != nil {
		t.Fatal(err)
	}

	devices, err = u.Devices()
	if err != nil {
		t.Fatal(err)
	}

	if len(devices) != 0 {
		t.Fatal("expected no devices")
	}
}

func TestUserDevicesCanNotify(t *testing.T) {
	u := testGetUser(t, 1)

	token := "90132847109328473129847"
	if err := u.EnableNotifications(token); err != nil {
		t.Fatal(err)
	}

	devices, err := u.Devices()
	if err != nil {
		t.Fatal(err)
	}

	for _, device := range devices {
		if device.Token == token {
			if device.CanNotify() != true {
				t.Fatal("expected that can notify")
			}

			if err := device.MarkAsNotified(); err != nil {
				t.Fatal(err)
			}

			if device.CanNotify() != false {
				t.Fatal("exepcted that can not notify")
			}
		}
	}
}
