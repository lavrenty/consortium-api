package main

import (
	"database/sql"
	"fmt"
	"strings"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/lib/pq"
)

// User struct as in DB
type User struct {
	ID         uint
	Name       sql.NullString
	FirstName  sql.NullString
	MiddleName sql.NullString
	LastName   sql.NullString
	Birthday   pq.NullTime // TODO: not implemented
	Gender     sql.NullString
	Locale     string
	Languages  string
	Email      sql.NullString // TODO: not implemented
	Picture    sql.NullString // TODO: badly implemented

	QuestionsCount uint
	AnswersCount   uint

	Balance         int
	BalanceCurrency string

	PendingBalance         int
	PendingBalanceCurrency string

	HasUnpaidInvoice sql.NullInt64 // this is solely for user notification and not real flag that keeps invoice status (e.g. user can have an legitimaly unpaid invoice)

	FbID          sql.NullString
	FbAccessToken sql.NullString
	// this is list of facebook perms (a comma separated list) that clients must request
	// when initiating fb login process
	FbPermissions string

	// system card for facebook
	FbCardSeenAt pq.NullTime
	FbCardCount  int

	// stripe customer
	StripeCustomer     sql.NullString
	StripeCardBrand    sql.NullString
	StripeCardLastFour sql.NullString

	AccountKitID          sql.NullString
	AccountKitNumber      sql.NullString
	AccountKitAccessToken sql.NullString

	IsDisabled     bool
	DisabledReason sql.NullString
	DisabledBy     sql.NullInt64

	Notifications           bool // global notifications toggler
	NextNotificationAt      time.Time
	NotificationsFrequency  string      // enum: always, hourly, daily, weekly (on all topics, e.g. not per-topic), default: daily
	NotificationsCardCount  int         // this is used by system_card_notifications
	NotificationsCardSeenAt pq.NullTime // this is used by system_card_notifications

	LastSeenAt time.Time
	UpdatedAt  time.Time
	CreatedAt  time.Time
}

// ToJSON returns minimal public json ...
func (u *User) ToJSON() JSON {
	j := JSON{
		"type":        "user",
		"id":          u.ID,
		"name":        u.Name.String,
		"first_name":  u.FirstName.String,
		"middle_name": u.MiddleName.String,
		"last_name":   u.LastName.String,
		"languages":   strings.Split(u.Languages, ","),
		"is_verified": u.IsVerified(),
	}

	if u.Gender.Valid {
		j["gender"] = u.Gender.String
	}
	if u.Picture.Valid {
		j["picture"] = u.Picture.String
	}

	return j
}

// CondensedName returns "Andrew Z." instead of "Andrew Zaitsev-Zotov"
func (u *User) CondensedName() string {
	ln := u.LastName.String
	if len(ln) > 1 {
		ln = ln[:1] + "."
	}
	n := ""
	if len(u.FirstName.String) > 0 {
		n = u.FirstName.String + " " + ln
	}

	return n
}

// ToCondensedJSON returns details about user with most personal information stripped out
func (u *User) ToCondensedJSON() JSON {
	ln := u.LastName.String
	if len(ln) > 1 {
		ln = ln[:1] + "."
	}
	n := ""
	if len(u.FirstName.String) > 0 {
		n = u.FirstName.String + " " + ln
	}

	j := JSON{
		"type":        "user",
		"id":          u.ID,
		"name":        n,
		"first_name":  u.FirstName.String,
		"last_name":   ln,
		"languages":   strings.Split(u.Languages, ","),
		"is_verified": u.IsVerified(),
	}

	if u.Gender.Valid {
		j["gender"] = u.Gender.String
	}
	if u.Picture.Valid {
		j["picture"] = u.Picture.String
	}

	return j
}

// ToFullJSON JSON user
func (u *User) ToFullJSON() JSON {
	j := u.ToJSON()

	j["questions_count"] = u.QuestionsCount
	j["answers_count"] = u.AnswersCount

	// private fields
	j["locale"] = u.Locale
	j["fb_permissions"] = u.FbPermissions
	j["has_facebook"] = u.FbID.Valid
	j["has_mobile"] = true
	j["has_linkedin"] = false
	j["has_twitter"] = false

	if u.Balance > 0 {
		j["balance"] = u.Balance
		j["balance_currency"] = u.BalanceCurrency
	}
	if u.PendingBalance > 0 {
		j["pending_balance"] = u.PendingBalance
		j["pending_balance_currency"] = u.PendingBalanceCurrency
	}

	sa, err := u.GetStripeAccount()
	if err == nil && sa != nil {
		j["payout"] = sa.ToJSON()
	}

	j["notifications"] = u.Notifications
	j["notifications_frequency"] = u.NotificationsFrequency
	j["is_payable"] = u.IsPayable()
	j["is_billable"] = u.IsBillable()
	j["has_unpaid_invoice"] = u.HasUnpaidInvoice.Valid
	if u.StripeCustomer.Valid {
		j["card_brand"] = u.StripeCardBrand.String
		j["card_last4"] = u.StripeCardLastFour.String
	}

	return j
}

// IsVerified ...
func (u *User) IsVerified() bool {
	return u.FbID.Valid
}

// CanCreate ...
func (u *User) CanCreate(card *Card) (bool, string) {
	if u.IsDisabled {
		return false, "disabled"
	}

	if !u.IsVerified() {
		return false, "not verified"
	}

	if card.Rate > 0 {
		if !u.IsBillable() {
			return false, "not billable"
		}

		if u.HasUnpaidInvoice.Valid {
			return false, "has unpaid invoice"
		}
	}

	return true, ""
}

// CanAnswer ...
func (u *User) CanAnswer(card *Card) bool {
	if card.Rate == 0 {
		return u.IsVerified()
	}
	return u.IsVerified() && u.IsPayable() && !u.IsDisabled
}

// PublicName for stripe
func (u *User) PublicName() string {
	if u.Name.Valid {
		return u.Name.String
	}

	if u.FirstName.Valid {
		return u.FirstName.String + " " + u.LastName.String
	}

	return fmt.Sprintf("user%d", u.ID)
}

// IsBillable ...
func (u *User) IsBillable() bool {
	return u.StripeCustomer.Valid && u.StripeCardLastFour.Valid
}

// IsPayable ...
func (u *User) IsPayable() bool {
	sa, err := u.GetStripeAccount()
	if err != nil {
		return false
	}

	if sa == nil {
		return false
	}

	if !sa.IsPayable() {
		return false
	}

	return true
}

// GetUser by ID
// Error will be nil if not found
func GetUser(ID interface{}) (*User, error) {
	user := new(User)
	if err := db.Get(user, "SELECT * FROM users WHERE id = $1", ID); err != nil {
		switch {
		case err == sql.ErrNoRows:
			return nil, nil
		default:
			return nil, err
		}
	}

	return user, nil
}

// MustGetUser ...
func MustGetUser(ID interface{}) (*User, error) {
	user, err := GetUser(ID)
	if err != nil {
		return nil, err
	}
	if user == nil {
		return nil, fmt.Errorf("user %#v not found", ID)
	}
	return user, nil
}

// GetUsers fetches multiple users by their id and returns an array of results
func GetUsers(ids []uint) ([]User, error) {
	users := []User{}

	if len(ids) <= 0 {
		return users, nil
	}

	query, args, err := sqlx.In("SELECT DISTINCT * FROM users WHERE id IN (?)", ids)
	if err != nil {
		return users, err
	}
	if err := db.Select(&users, db.Rebind(query), args...); err != nil {
		switch {
		case err == sql.ErrNoRows:
			return users, nil
		default:
			return users, err
		}
	}
	return users, nil
}

// NewUser ...
func NewUser() *User {
	return &User{
		FbPermissions: DefaultFbPermissions,
		Languages:     DefaultLanguage,
	}
}

// FindOrCreateUserFromAccountKit (facebook account kit)
// func FindOrCreateUserFromAccountKit(accountID string) (*User, error) {
// 	user := NewUser()
//
// 	err := transaction(func(tx *sqlx.Tx) (err error) {
// 		err = tx.Get(user, "SELECT * FROM users WHERE account_kit_id = $1", accountID)
//
// 		// if user not found, create one
// 		if err != nil && err == sql.ErrNoRows {
// 			err = tx.Get(user, "INSERT INTO users (account_kit_id, languages, fb_permissions) VALUES ($1, $2, $3) RETURNING *", accountID, user.Languages, user.FbPermissions)
// 		}
// 		return
// 	})
// 	return user, err
// }

// FindOrCreateUserFromFacebook ..
func FindOrCreateUserFromFacebook(facebookID string) (*User, error) {
	user := NewUser()

	err := transaction(func(tx *sqlx.Tx) (err error) {
		err = tx.Get(user, "SELECT * FROM users WHERE fb_id = $1", facebookID)

		if err != nil && err == sql.ErrNoRows {
			err = tx.Get(user, "INSERT INTO users (fb_id, languages, fb_permissions) VALUES ($1, $2, $3) RETURNING *", facebookID, user.Languages, user.FbPermissions)
		}
		return
	})

	return user, err
}

// AccessToken generate for this user
func (u *User) AccessToken(issuer string) (string, error) {
	return createToken(u, issuer)
}

// SpeaksLanguage returns true if user speaks specified langauge code
func (u *User) SpeaksLanguage(code string) bool {
	if len(code) != 2 {
		panic("invalid code " + code)
	}
	for _, lang := range strings.Split(u.Languages, ",") {
		if lang == code {
			return true
		}
	}
	return false
}

// AddLanguage to users user.Language array
// `lang` must conform to ISO 639-1
// Returns without error if language already present
func (u *User) AddLanguage(code string) error {
	if len(code) != 2 {
		return fmt.Errorf("invalid language: %s", code)
	}
	if _, ok := languages.GetNameByCode(code); !ok {
		return fmt.Errorf("invalid language: %s", code)
	}

	if len(u.Languages) > 0 {
		langs := strings.Split(u.Languages, ",")
		for _, l := range langs {
			if l == code {
				return nil
			}
		}

		langs = append(langs, code)
		u.Languages = strings.Join(langs, ",")
	} else {
		u.Languages = code
	}

	if _, err := db.NamedExec("UPDATE users SET languages = :languages WHERE id = :id", u); err != nil {
		return err
	}

	return nil
}

// DeleteLanguage removes langauge form user.Language array
// If no language is left, "en" is enabled
func (u *User) DeleteLanguage(code string) error {
	oldLanguages := u.Languages
	langs := strings.Split(u.Languages, ",")
	newLangs := []string{}
	for _, l := range langs {
		if l != code {
			newLangs = append(newLangs, l)
		}
	}

	// same, nothing deleted
	if len(newLangs) == len(langs) {
		return nil
	}

	u.Languages = strings.Join(newLangs, ",")
	if len(u.Languages) == 0 {
		u.Languages = DefaultLanguage

		// [language[_territory][.codeset][@modifier]]
		if languages.IsSupported(u.Locale[:2]) {
			u.Languages = u.Locale[:2]
		}
	}

	return db.QueryRowx("UPDATE users SET languages = $1, updated_at = now() WHERE id = $2 AND languages = $3 RETURNING languages", u.Languages, u.ID, oldLanguages).Scan(&u.Languages)
}

// SetLanguages to specified list
func (u *User) SetLanguages(langs []string) error {
	oldLanguages := u.Languages

	// if empty set provided, restore to locale
	if len(langs) == 0 {
		deflang := DefaultLanguage

		// [language[_territory][.codeset][@modifier]]
		if languages.IsSupported(u.Locale[:2]) {
			deflang = u.Locale[:2]
		}
		langs = append(langs, deflang)
	}

	// validate langs
	for _, lang := range langs {
		if !languages.IsSupported(lang) {
			return fmt.Errorf("sorry, lang %v is not supported", lang)
		}
	}

	u.Languages = strings.Join(langs, ",")

	return db.QueryRowx("UPDATE users SET languages = $1, updated_at = now() WHERE id = $2 AND languages = $3 RETURNING languages", u.Languages, u.ID, oldLanguages).Scan(&u.Languages)
}

// UpdateLastSeenAt marks user as logged in
func (u *User) UpdateLastSeenAt() error {
	return db.QueryRowx("UPDATE users SET last_seen_at = now() WHERE id = $1 RETURNING last_seen_at", u.ID).Scan(&u.LastSeenAt)
}

// AddFriend ...
func (u *User) AddFriend(friend *User, source string) error {
	if source != "fb" {
		return fmt.Errorf("friendship source '%s' not supported", source)
	}

	return transaction(func(tx *sqlx.Tx) (err error) {
		var fid uint
		if err = tx.Get(&fid, "SELECT id FROM friends WHERE user_id = $1 AND friend_id = $2 AND source = $3", u.ID, friend.ID, source); err != nil {
			switch {
			case err == sql.ErrNoRows:
				if _, err = tx.Exec("INSERT INTO friends (user_id, friend_id, source) VALUES ($1, $2, $3)", u.ID, friend.ID, source); err != nil {
					return
				}

			default:
				return
			}
		}
		// mutual
		if err = tx.Get(&fid, "SELECT id FROM friends WHERE user_id = $1 AND friend_id = $2 AND source = $3", friend.ID, u.ID, source); err != nil {
			switch {
			case err == sql.ErrNoRows:
				if _, err = tx.Exec("INSERT INTO friends (user_id, friend_id, source) VALUES ($1, $2, $3)", friend.ID, u.ID, source); err != nil {
					return
				}

			default:
				return err
			}
		}
		return
	})
}

// SetUnpaidInvoice ...
func (u *User) SetUnpaidInvoice(invoiceID uint) error {
	if u.HasUnpaidInvoice.Valid && u.HasUnpaidInvoice.Int64 == int64(invoiceID) {
		return nil
	}
	u.HasUnpaidInvoice.Scan(invoiceID)
	_, err := db.NamedExec("UPDATE users SET has_unpaid_invoice = :has_unpaid_invoice WHERE id = :id", u)

	return err
}

// ClearUnpaidInvoice ...
func (u *User) ClearUnpaidInvoice() error {
	if !u.HasUnpaidInvoice.Valid {
		return nil
	}
	u.HasUnpaidInvoice.Scan(nil)

	_, err := db.NamedExec("UPDATE users SET has_unpaid_invoice = :has_unpaid_invoice WHERE id = :id", u)

	return err
}

// IncQuestionsCount increments user.questions_count in database
func (u *User) IncQuestionsCount() error {
	return db.Get(&u.QuestionsCount, "UPDATE users SET questions_count = questions_count + 1 WHERE id = $1 RETURNING questions_count", u.ID)
}

// IncAnswersCount increments user.answers_count in database
func (u *User) IncAnswersCount() error {
	return db.Get(&u.AnswersCount, "UPDATE users SET answers_count = answers_count + 1 WHERE id = $1 RETURNING answers_count", u.ID)
}

// Disable user account
func (u *User) Disable(reason string, by *User) error {
	u.IsDisabled = true
	u.DisabledReason.Scan(reason)
	if by != nil {
		u.DisabledBy.Scan(by.ID)
	}
	_, err := db.NamedExec("UPDATE users SET is_disabled = :is_disabled, disabled_reason = :disabled_reason, disabled_by = :disabled_by WHERE id = :id", u)

	return err
}

// SetNotificationsFrequency updates `notifications_frequency` field
func (u *User) SetNotificationsFrequency(freq string) error {
	switch freq {
	case NotificationsAlways, NotificationsDaily, NotificationsHourly, NotificationsWeekly:
		// ok
	default:
		freq = NotificationsDefault
	}

	u.NotificationsFrequency = freq
	return db.Get(&u.NotificationsFrequency, "UPDATE users SET notifications_frequency = $1 WHERE id = $2 RETURNING notifications_frequency", u.NotificationsFrequency, u.ID)
}
