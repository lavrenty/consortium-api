package main

import (
	"fmt"
	"strings"
	"time"

	"github.com/Sirupsen/logrus"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

// returns private key to be used for token authentication
func getTokenKey(token *jwt.Token) (interface{}, error) {
	if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
		return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
	}

	return jwtSecret, nil
}

// create token for use with requireToken()
func createToken(user *User, issuer string) (string, error) {
	token := jwt.New(jwt.SigningMethodHS512)

	token.Claims["uid"] = user.ID
	token.Claims["iss"] = fmt.Sprintf("cnsprc-%s", issuer)
	token.Claims["iat"] = time.Now().Unix()
	token.Claims["exp"] = time.Now().AddDate(5, 0, 0).Unix()
	return token.SignedString(jwtSecret)
}

// validate and authorize user through bearer token issued with createToken method
func requireToken() gin.HandlerFunc {
	return func(c *gin.Context) {
		auth := c.Request.Header.Get("Authorization")
		if auth == "" || len(auth) < 10 || strings.ToLower(auth[0:7]) != "bearer " {
			apiError(c, 401, "missing authorization bearer header", nil)
			return
		}

		token, err := jwt.Parse(auth[7:], getTokenKey)

		if err == nil && token.Valid {
			// TODO: validate token.Claims to contain int
			user := &User{}
			if user, err = GetUser(token.Claims["uid"]); err != nil {
				apiErrorInternal(c, err)
			} else if user == nil {
				apiError(c, 401, "user not found or disabled", nil)
			} else if user.IsDisabled {
				apiErrorWithType(c, ErrAPIContactSupport, "account disabled")
			} else {
				if user.LastSeenAt.Before(time.Now().AddDate(0, 0, -1)) {
					if err = user.UpdateLastSeenAt(); err != nil {
						apiErrorInternal(c, err)
						return
					}
				}

				c.Set("current", user)
				c.Set("logger", c.MustGet("logger").(*logrus.Entry).WithField("current", user.ID))
				c.Next()
			}
		} else {
			apiError(c, 401, "invalid access token", err)
		}
	}
}

// preloads user from :user param in url and makes sure it's valid
func preloadUser() gin.HandlerFunc {
	return func(c *gin.Context) {
		userParam := c.Param("user")
		if user, err := GetUser(userParam); err != nil {
			apiErrorInternal(c, err)
		} else if user == nil {
			apiError(c, 404, "user not found or disabled", nil)
		} else {
			c.Set("user", user)
			c.Set("logger", c.MustGet("logger").(*logrus.Entry).WithField("user", user.ID))
			c.Next()
		}
	}
}

// preloads topic from :topic param in url and makes sure it's valid
func preloadTopic() gin.HandlerFunc {
	return func(c *gin.Context) {
		topicParam := c.Param("topic")
		if topic, err := GetTopic(topicParam); err != nil {
			apiErrorInternal(c, err)
		} else if topic == nil {
			apiError(c, 404, "topic not found or disabled", nil)
		} else {
			c.Set("topic", topic)
			c.Set("logger", c.MustGet("logger").(*logrus.Entry).WithField("topic", topic.ID))
			c.Next()
		}
	}
}

// preloads card from :card param in url and makes sure it's valid
func preloadCard() gin.HandlerFunc {
	return func(c *gin.Context) {
		cardParam := c.Param("card")
		if card, err := GetCard(cardParam); err != nil {
			apiErrorInternal(c, err)
		} else if card == nil {
			apiError(c, 404, "card not found or disabled", nil)
		} else {
			c.Set("card", card)
			c.Set("logger", c.MustGet("logger").(*logrus.Entry).WithField("card", card.ID))
			c.Next()
		}
	}
}

// preload call from :call param in url and make sure it's valid
func preloadCall() gin.HandlerFunc {
	return func(c *gin.Context) {
		callParam := c.Param("call")
		if call, err := GetCall(callParam); err != nil {
			apiErrorInternal(c, err)
		} else if call == nil {
			apiError(c, 404, "call not found or disabled", nil)
		} else {
			c.Set("call", call)
			c.Set("logger", c.MustGet("logger").(*logrus.Entry).WithField("call", call.ID))
			c.Next()
		}
	}
}

// validates :lang value in param
func preloadLang() gin.HandlerFunc {
	return func(c *gin.Context) {
		lang := c.Param("lang")
		if len(lang) != 2 {
			apiError(c, 404, "lang not found or supported", nil)
		} else {
			c.Set("lang", lang)
			c.Set("logger", c.MustGet("logger").(*logrus.Entry).WithField("lang", lang))
			c.Next()
		}
	}
}
