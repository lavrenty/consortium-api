package main

import (
	"testing"
)

func TestUserCreate(t *testing.T) {
	_, err := GetUser(66666)
	if err != nil {
		t.Fatal(err)
	}
}

func TestFindOrCreateUserFromFacebook(t *testing.T) {
	id := "1234567"
	andy, err := FindOrCreateUserFromFacebook(id)

	if err != nil || andy == nil {
		t.Fatal(err, andy)
	}

	if andy.FbID.Valid && andy.FbID.String != id {
		t.Fatalf("fb_id != id (%+v)", andy.FbID)
	}

	andyClone, err := FindOrCreateUserFromFacebook(id)
	if err != nil || andyClone == nil || andyClone.ID != andy.ID {
		t.Fatal(err, andyClone)
	}
}

func TestMustGetUser(t *testing.T) {
	_, err := MustGetUser(12341234)
	if err == nil {
		t.Fatal("expected error")
	}
}

func TestGetUsers(t *testing.T) {
	ids := []uint{1, 1, 2, 3, 2, 3, 6, 100}

	users, err := GetUsers(ids)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(users)
	if len(users) <= 2 {
		t.Fatal(len(users))
	}
}

func TestUserTopics(t *testing.T) {
	u := testGetUser(t, 1)
	topics, err := u.Topics()
	if err != nil {
		t.Fatal(err)
	}
	if topics == nil {
		t.Fatal("topics is nil")
	}
}

func TestUserSpeaksLanguage(t *testing.T) {
	user := testCreateUser(t)
	if err := user.AddLanguage("ru"); err != nil {
		t.Fatal(err)
	}
	if err := user.AddLanguage("ja"); err != nil {
		t.Fatal(err)
	}
	if user.SpeaksLanguage("ja") != true {
		t.Fatal("expected user to speak jp")
	}
	if user.SpeaksLanguage("uz") != false {
		t.Fatal("expected user not to speak uz")
	}
}

func TestUserLanguages(t *testing.T) {
	user := testCreateUser(t)

	t.Log(user.Languages)
	if err := user.AddLanguage("ru"); err != nil {
		t.Fatal(err)
	}

	t.Log(user.Languages)
	if err := user.AddLanguage("ru"); err != nil {
		t.Fatal(err)
	}
	t.Log(user.Languages)
	if err := user.AddLanguage("en"); err != nil {
		t.Fatal(err)
	}
	t.Log(user.Languages)
	if err := user.AddLanguage("es"); err != nil {
		t.Fatal(err)
	}

	t.Log(user.Languages)
	if user.Languages != "en,ru,es" {
		t.Fatal(user.Languages)
	}

	other := testGetUser(t, user.ID)
	t.Log(other.Languages)
	if other.Languages != user.Languages {
		t.Fatal(other.Languages, user.Languages)
	}

	if err := user.DeleteLanguage("en"); err != nil {
		t.Fatal(err)
	}
	if err := user.DeleteLanguage("es"); err != nil {
		t.Fatal(err)
	}
	if err := user.DeleteLanguage("es"); err != nil {
		t.Fatal(err)
	}
	if user.Languages != "ru" {
		t.Fatal(user.Languages)
	}
	t.Log(user.Languages)

	if err := user.DeleteLanguage("en"); err != nil {
		t.Fatal(err)
	}
	t.Log(user.Languages)

	if err := user.DeleteLanguage("ru"); err != nil {
		t.Fatal(err)
	}
	t.Log(user.Languages)

	if user.Languages != DefaultLanguage {
		t.Fatal(user.Languages)
	}
}

func TestUserSetLanguages(t *testing.T) {
	u := testGetUser(t, 1)

	if err := u.SetLanguages([]string{"en", "fr", "es"}); err != nil {
		t.Fatal(err)
	}
	if u.Languages != "en,fr,es" {
		t.Fatal("failed to set")
	}

	if err := u.SetLanguages([]string{}); err != nil {
		t.Fatal(err)
	}
	if u.Languages != DefaultLanguage {
		t.Fatal("failed to set")
	}

	if err := u.SetLanguages([]string{"en", "error"}); err == nil {
		t.Fatal("expected error")
	}
}

func TestUserUpdateLastSeenAt(t *testing.T) {
	u := testGetUser(t, 1)
	if err := u.UpdateLastSeenAt(); err != nil {
		t.Fatal(err, u)
	}
}

func TestUserAddFriend(t *testing.T) {
	u := testGetUser(t, 1)
	f := testGetUser(t, 2)

	if err := u.AddFriend(f, "fb"); err != nil {
		t.Fatal(err, u, f)
	}

	if err := f.AddFriend(u, "fb"); err != nil {
		t.Fatal(err, f, u)
	}
}

func TestUserDisable(t *testing.T) {
	user := testCreateUser(t)

	if err := user.Disable("asshole", nil); err != nil {
		t.Fatal(err)
	}

	user = testGetUser(t, user.ID)
	if !user.IsDisabled {
		t.Fatal("not disabled")
	}
}

func TestUserDisableBy(t *testing.T) {
	user := testCreateUser(t)
	admin := testGetUser(t, 1)

	if err := user.Disable("asshole", admin); err != nil {
		t.Fatal(err)
	}

	user = testGetUser(t, user.ID)
	if !user.IsDisabled {
		t.Fatal("not disabled")
	}

	if uint(user.DisabledBy.Int64) != admin.ID {
		t.Fatal("invalid disabled_by")
	}
}

// you need invoice for that
// func TestUserSetUnpaidInvoice(t *testing.T) {
// 	user := testGetUser(t, 1)
//
// 	if err := user.SetUnpaidInvoice(0); err != nil {
// 		t.Fatal(err)
// 	}
//
// 	if user.HasUnpaidInvoice.Valid {
// 		t.Fatal("expected invalid")
// 	}
//
// 	if err := user.SetUnpaidInvoice(1); err != nil {
// 		t.Fatal(err)
// 	}
// }
