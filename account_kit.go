// // reference https://developers.facebook.com/docs/accountkit/graphapi
package main

// DISABLED
//
// import (
// 	"crypto/hmac"
// 	"crypto/sha256"
// 	"encoding/json"
// 	"fmt"
// 	"io/ioutil"
// 	"net/http"
// 	"net/url"
//
// 	"github.com/Sirupsen/logrus"
// )
//
// // AccountKit is a way to interact with fb account kit
// type AccountKit struct {
// 	AccountID   string
// 	AccessToken string
// }
//
// // NewAccountKitWithCode ...
// func NewAccountKitWithCode(code string) (*AccountKit, error) {
// 	k := &AccountKit{}
// 	if err := k.ExchangeAuthorizationCode(code); err != nil {
// 		return nil, err
// 	}
//
// 	return k, nil
// }
//
// // NewAccountKitWithToken ...
// func NewAccountKitWithToken(token string) (*AccountKit, error) {
// 	return &AccountKit{
// 		AccessToken: token,
// 	}, nil
// }
//
// // AppSecretProof ...
// func (k *AccountKit) AppSecretProof(token string) string {
// 	mac := hmac.New(sha256.New, []byte(config.Facebook.Secret))
// 	return fmt.Sprintf("%x", mac.Sum([]byte(token)))
// }
//
// // ExchangeAuthorizationCode ...
// func (k *AccountKit) ExchangeAuthorizationCode(code string) error {
// 	u, _ := url.Parse("https://graph.accountkit.com/v1.0/access_token")
// 	q := url.Values{}
// 	q.Set("grant_type", "authorization_code")
// 	q.Set("code", code)
// 	// The final argument of the URL query string is the App Access Token described above.
// 	// u.RawQuery = fmt.Sprintf("%s&access_token=AA|%s|%s", q.Encode(), config.Facebook.AppID, config.Facebook.AccountKitSecret)
// 	q.Set("access_token", fmt.Sprintf("AA|%s|%s", config.Facebook.AppID, config.Facebook.AccountKitSecret))
// 	u.RawQuery = q.Encode()
//
// 	res, err := http.Get(u.String())
// 	if err != nil {
// 		return err
// 	}
// 	defer res.Body.Close()
//
// 	if res.StatusCode != http.StatusOK {
// 		logrus.Debugf("request %+v", u.String())
// 		body, _ := ioutil.ReadAll(res.Body)
// 		return fmt.Errorf("accountkit reply error %+v (%+v)", res.Status, string(body))
// 	}
//
// 	// var msg struct {
// 	// 	ID    string
// 	// 	Phone struct {
// 	// 		Number         string
// 	// 		CountryPrefix  string `json:"country_prefix"`
// 	// 		NationalNumber string `json:"national_number"`
// 	// 	}
// 	// }
//
// 	var msg struct {
// 		ID                      string
// 		AccessToken             string `json:"access_token"`
// 		TokenRefreshIntervalSec int    `json:"token_refresh_interval_sec"`
// 	}
// 	decoder := json.NewDecoder(res.Body)
// 	if err := decoder.Decode(&msg); err != nil {
// 		return err
// 	}
//
// 	logrus.Debugf("account kit, got id %+v, token %+v", msg.ID, msg.AccessToken)
//
// 	k.AccessToken = msg.AccessToken
// 	k.AccountID = msg.ID
//
// 	return nil
// }
