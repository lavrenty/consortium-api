package main

import (
	"fmt"
	"log"

	"github.com/Sirupsen/logrus"
	fb "github.com/huandu/facebook"
)

// Facebook connection
type Facebook struct {
	Token        string
	TokenExpires int

	hasPermissions bool
	permissions    []struct {
		Name   string `facebook:"permission,required"`
		Status string `facebook:",required"`
	}
}

// NewFacebookFromToken validates access token and on success returns new facebook instance
func NewFacebookFromToken(accessToken string) (*Facebook, error) {
	f := &Facebook{Token: accessToken}
	s := f.getSession()
	if err := s.Validate(); err != nil {
		return nil, err
	}

	return f, nil
}

// NewFacebookFromUserToken takes short-lived userToken (usually obtained by client side sdk) and exchanges
// it for long-term token that can be used for up to 60 days
func NewFacebookFromUserToken(userToken string) (*Facebook, error) {
	client := fb.New(config.Facebook.AppID, config.Facebook.Secret)
	client.EnableAppsecretProof = true

	token, expires, err := client.ExchangeToken(userToken)
	if err != nil {
		return nil, err
	}

	return &Facebook{Token: token, TokenExpires: expires}, nil
}

func (f *Facebook) getSession() *fb.Session {
	client := fb.New(config.Facebook.AppID, config.Facebook.Secret)
	client.EnableAppsecretProof = true

	session := client.Session(f.Token)
	if config.Debug {
		session.SetDebug(fb.DEBUG_ALL)
	}
	return session
}

// FindOrCreateUserFromToken ...
func (f *Facebook) FindOrCreateUserFromToken() (*User, error) {
	var fbID string

	s := f.getSession()
	// we need public_profile
	res, err := s.Get("/me", fb.Params{
		"fields": "id",
	})
	if err != nil {
		return nil, err
	}
	if err := res.DecodeField("id", &fbID); err != nil {
		return nil, err
	}

	return FindOrCreateUserFromFacebook(fbID)
}

// UpdateBasicProfile ...
func (f *Facebook) UpdateBasicProfile(user *User) (err error) {
	if err := f.getPermissions(); err != nil {
		return err
	}

	// update basic profile
	if err := f.updateProfile(user, true); err != nil {
		return err
	}

	// update languages
	if f.CheckPermission("user_likes", FacebookIsGranted) {
		if err := f.updateLanguages(user); err != nil {
			return err
		}
	} else if f.CheckPermission("user_likes", FacebookIsDeclined) {
		// TODO: tell user what he is loosing
		logrus.Warnf("Facebook.UpdateBasicProfile: user%d user_likes declined", user.ID)
	} else {
		logrus.Warnf("Facebook.UpdateBasicProfile: user%d user_likes not returned (%#v)", user.ID, f.permissions)
	}

	// update picture
	return f.updatePicture(user)
}

// UpdateOther ...
func (f *Facebook) UpdateOther(user *User) (err error) {
	if err := f.getPermissions(); err != nil {
		return err
	}

	// fetch friends data
	if f.CheckPermission("user_friends", FacebookIsGranted) {
		if err := f.updateFriends(user); err != nil {
			return err
		}
	} else if f.CheckPermission("user_friends", FacebookIsDeclined) {
		logrus.Warnf("Facebook.UpdateOther: user%d user_friends declined", user.ID)
		// TODO: tell user what he is missing
	} else {
		logrus.Warnf("Facebook.UpdateOther: user%d user_friends not returned (%#v)", user.ID, f.permissions)
	}

	return nil
}

// UpdateProfile should always work
// //
// me/permissions
//
//
// first_name, last_name, middle_name (name_format, name)
//
// The person's birthday. This is a fixed format string, like MM/DD/YYYY. However, people can control who can see the year they were born separately from the month and day so this string can be only the year (YYYY) or the month + day (MM/DD)
//
// The age segment for this person expressed as a minimum and maximum age. For example, more than 18, less than 21.
//   age: { min uint32, max uint32 }
//
// currency: {
// 	 currency_offset uint32
// 	 usd_exchange float64
// 	 usd_exchange_inverse float64
// 	 user_currency string // The ISO-4217-3 code for the person's preferred currency (defaulting to USD if the person hasn't set one)
// }
//
// gender
// The gender selected by this person, male or female. This value will be omitted if the gender is set to a custom value
//
// installed
// Is the app making the request installed?
//
// is_verified bool People with large numbers of followers can have the authenticity of their identity manually verified by Facebook. This field indicates whether the person's profile is verified in this way. This is distinct from the verified field
//
// languages { id string, name string } Facebook Pages representing the languages this person knows
//
// verified Indicates whether the account has been verified. This is distinct from the is_verified field. Someone is considered verified if they take any of the following actions:
//   - Register for mobile
//   - Confirm their account via SMS
//   - Enter a valid credit card
//
// TODO: birthday,age_range
//
func (f *Facebook) updateProfile(user *User, force bool) error {
	var fbProfile struct {
		ID         string
		Email      string
		Name       string
		FirstName  string `facebook:"first_name"`
		MiddleName string `facebook:"middle_name"`
		LastName   string `facebook:"last_name"`
		Gender     string
		Locale     string
		Verified   bool
		IsVerified bool
		Installed  bool
	}
	s := f.getSession()

	// we need public_profile
	res, err := s.Get("/me", fb.Params{
		"fields": "id,email,age_range,birthday,name,first_name,middle_name,last_name,gender,locale,verified,is_verified,installed",
	})
	if err != nil {
		return err
	}
	if err = res.Decode(&fbProfile); err != nil {
		return err
	}

	// check that user is unique
	// TODO: wrap the whole thing in tx
	other, err := GetUserByFbID(fbProfile.ID)
	if err != nil {
		return err
	}
	if other != nil && other.ID != user.ID {
		return fmt.Errorf("user with fb_id %v already registered with id %v (and not %v)", fbProfile.ID, other.ID, user.ID)
	}

	// scan fields
	if !user.FbID.Valid || force {
		user.FbID.Scan(fbProfile.ID)
		user.FbAccessToken.Scan(f.Token)
	}
	if !user.Email.Valid || force {
		user.Email.Scan(fbProfile.Email)
	}
	if !user.Name.Valid || force {
		user.Name.Scan(fbProfile.Name)
	}
	if !user.FirstName.Valid || force {
		user.FirstName.Scan(fbProfile.FirstName)
	}
	if !user.MiddleName.Valid || force {
		user.MiddleName.Scan(fbProfile.MiddleName)
	}
	if !user.LastName.Valid || force {
		user.LastName.Scan(fbProfile.LastName)
	}
	if !user.Gender.Valid || force {
		user.Gender.Scan(fbProfile.Gender)
	}

	// update language from locale
	if force {
		user.Locale = fbProfile.Locale
	}

	if _, err = db.NamedExec("UPDATE users SET fb_id = :fb_id, fb_access_token = :fb_access_token, name = :name, first_name = :first_name, middle_name = :middle_name, last_name = :last_name, gender = :gender, locale = :locale, updated_at = now() WHERE id = :id", user); err != nil {
		return err
	}

	if force {
		// locale string is [language[_territory][.codeset][@modifier]]
		lang := user.Locale[:2]
		if _, ok := languages.GetNameByCode(lang); ok {
			if err = user.AddLanguage(lang); err != nil {
				return err
			}
		}
	}

	// update score data
	score, err := GetScore(user.ID)
	if err != nil {
		return err
	}
	score.FbVerified.Scan(fbProfile.Verified)
	score.FbIsVerified.Scan(fbProfile.IsVerified)
	if err := score.Save(); err != nil {
		return err
	}

	return nil
}

// UpdateLanguages updates array of known user languages from facebook
// languages need user_likes perms
func (f *Facebook) updateLanguages(user *User) error {
	s := f.getSession()

	// we need public_profile
	// languages need user_likes perms
	res, err := s.Get("/me", fb.Params{
		"fields": "languages",
	})
	if err != nil {
		return err
	}

	// copy languages for user
	var langs []struct {
		Name string `facebook:"name,required"`
	}
	if err := res.DecodeField("languages", &langs); err != nil {
		// assume english
		if err := user.AddLanguage("en"); err != nil {
			return err
		}
		return nil
	}

	for _, lang := range langs {
		if code, ok := languages.GetCodeByName(lang.Name); ok {
			if err := user.AddLanguage(code); err != nil {
				return err
			}
		} else {
			// TODO: log unsupported langauges from facebook!
		}
	}

	return nil
}

// GetFriends ...
// me/friends
// needs user_friends permission
// {
//   "data": [
//     {
//       "name": "Konstantin Anoshkin",
//       "id": "788416754600318"
//     }
//   ],
//   "paging": {
//     "next": "https://graph.facebook.com/v2.5/10153809662157265/friends?format=json&access_token=CAACsD2TyXVEBANkdvhij5hxObKRUWIkwZAZAGtcUHE6JqWZAtq1NF0t02dbn86Q3eINjbvpLvaHT5KAPzrT4oGaXZAwl91ph6JapK9ZBcGZCQcULTttJHcfaNiMsSxA0HdPhBomsF5XIAK6DZCOeJYwzrgozuUUmOmZCe3no5ZBk9IYbEX0C13IjzPyPpSWa2B11zhSE332S3fwZDZD&limit=25&offset=25&__after_id=enc_AdCZBhYHTqtc0XyZCBGC1ChbfIuNi3IkVYQu8ZB62iG2SM3ZAkewz1uAdq7IVIT0gvOycDz3ncKRrKIHrKZBsV4BblBJl"
//   },
//   "summary": {
//     "total_count": 741
//   }
// }
func (f *Facebook) getFriends() ([]string, int, error) {
	var fids []string
	var friendsCount int

	s := f.getSession()

	res, err := s.Get("/me/friends", fb.Params{"fields": "id"})
	if err != nil {
		return nil, 0, err
	}

	// this is used for scoring
	if err = res.DecodeField("summary.total_count", &friendsCount); err != nil {
		return nil, 0, err
	}

	results, err := res.Paging(s)
	if err != nil {
		return nil, 0, err
	}
	done := false
	for !done {
		friends := results.Data()
		for _, friend := range friends {
			var fid string
			if err = friend.DecodeField("id", &fid); err != nil {
				return nil, 0, err
			}
			fids = append(fids, fid)
		}

		done, err = results.Next()
		if err != nil {
			return nil, 0, err
		}
	}

	return fids, friendsCount, nil
}

// UpdateFriends tries to find matching friends and store their relations in db
func (f *Facebook) updateFriends(user *User) error {
	fbids, friendsCount, err := f.getFriends()
	if err != nil {
		return err
	}

	score, err := GetScore(user.ID)
	if err != nil {
		return err
	}
	score.FbFriendsCount.Scan(friendsCount)
	if err := score.Save(); err != nil {
		return err
	}

	for _, fbid := range fbids {
		friend, err := GetUserByFbID(fbid)
		if err != nil {
			return err
		}

		if friend == nil {
			logrus.Debugf("Facebook.UpdateFriends: for user %d could not find friend with fb id %s", user.ID, fbid)
			continue
		}

		if err := user.AddFriend(friend, "fb"); err != nil {
			return err
		}
	}

	if err := user.RebuildEndorsements(); err != nil {
		return err
	}

	return nil
}

// updatePicture updates user profile picture from facebook
// if user has no picture on facebook, nothing happens
// return error on fb / db errors
func (f *Facebook) updatePicture(user *User) error {
	pic, err := f.getPicture("large")
	if err != nil || len(pic) == 0 {
		return err
	}

	user.Picture.Scan(pic)

	_, err = db.Exec("UPDATE users SET picture = $1 WHERE id = $2", pic, user.ID)
	return err
}

// getPicture returns picture url with speicfied size
// if there is no picture, empty string is returned with no error
// size can be one of: large, small, normal, square
func (f *Facebook) getPicture(size string) (string, error) {
	s := f.getSession()

	res, err := s.Get("/me/picture", fb.Params{"type": size, "redirect": "false"})
	if err != nil {
		return "", err
	}

	var isSilhouette bool
	var pic string

	if err := res.DecodeField("data.is_silhouette", &isSilhouette); err != nil {
		return "", err
	}

	// no picture
	if isSilhouette {
		return "", nil
	}

	if err := res.DecodeField("data.url", &pic); err != nil {
		return "", err
	}

	return pic, nil
}

// Facebook permissions
const (
	FacebookIsGranted  = "granted"
	FacebookIsDeclined = "declined"
)

// CheckPermission checks wether specified permission is granted or declined
// f.CheckPermission("user_likes", FacebookIsGranted)
// f.CheckPermission("user_firends", FacebookIsDeclined)
func (f *Facebook) CheckPermission(name string, status string) bool {
	if !f.hasPermissions {
		log.Panicf("call to CheckPermission(%s, %s) before getPermissions()", name, status)
	}
	for _, p := range f.permissions {
		if p.Name == name && p.Status == status {
			return true
		}
	}
	return false
}

// GetPermissions returns list of token permissions
func (f *Facebook) getPermissions() error {
	if f.hasPermissions {
		return nil
	}

	s := f.getSession()

	res, err := s.Get("/me/permissions", fb.Params{})
	if err != nil {
		return err
	}
	if err := res.DecodeField("data", &f.permissions); err != nil {
		return err
	}
	f.hasPermissions = true

	return nil
}
