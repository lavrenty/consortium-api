package main

import (
	"bufio"
	"encoding/json"
	"errors"
	"net/http"
	"net/url"
	"os"
	"strings"
	"sync/atomic"
	"time"

	"github.com/Sirupsen/logrus"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
)

type wsMessage struct {
	Method string
	Bytes  []byte
	Raw    JSON
}

type wsConn struct {
	*websocket.Conn
	remoteIP string
	logger   *logrus.Entry
}

func (ws *wsConn) Logger() *logrus.Entry {
	if ws.logger == nil {
		ws.logger = logrus.WithField("remote_ip", ws.remoteIP)
	}

	return ws.logger
}

func wsMethod(msg []byte) *wsMessage {
	var msgJSON JSON

	if err := json.Unmarshal(msg, &msgJSON); err != nil {
		return nil
	}

	method, ok := msgJSON["method"].(string)
	if !ok {
		return nil
	}

	return &wsMessage{Method: method, Bytes: msg, Raw: msgJSON}
}

// Makes sure connection is keep alive, sends `true` when timed out
// Use on server-side only
//
// timeout := wsPinger(ws)
//
// loop:
// for {
//   select {
//   ...
//   case <-timeout:
//     break loop
//   }
// }
//
func wsPinger(ws *wsConn) <-chan bool {
	var pingCounter int64

	timeout := make(chan bool)

	ws.SetPongHandler(func(data string) error {
		atomic.SwapInt64(&pingCounter, 0)
		return nil
	})

	go func(timeout chan bool) {
		defer close(timeout)

		// logrus.Debugf("[WS-PINGER] %s ON", ws.RemoteAddr().String())

	loop:
		for {
			select {
			case <-time.After(DashboardPingerTimeout):
				counter := atomic.AddInt64(&pingCounter, 1)
				if counter > 2 {
					wsError(ws, "ping timeout", nil)
					break loop
				}
				if err := ws.WriteControl(websocket.PingMessage, []byte("ping-pong"), time.Now().Add(10*time.Second)); err != nil {
					// logrus.Debugf("p %s ping failed, %s", ws.RemoteAddr().String(), err.Error())
					break loop
				}
			}
		}

		// logrus.Debugf("[WS-PINGER] %s OFF", ws.RemoteAddr().String())

	}(timeout)

	return timeout
}

// wsClient recieves client messages over websocket and forward them to newly created channel
// Client-side & Server-side usable
//
// TODO: add select{} loop timeout?
func wsClient(ws *wsConn, isServerSide bool) chan wsMessage {
	client := make(chan wsMessage)

	go func(client chan wsMessage) {
		defer func() {
			// logrus.Debugf("c %s close", ws.RemoteAddr().String())
			ws.Close()
		}()
		defer close(client)

		// logrus.Debugf("c %s on", ws.RemoteAddr().String())

	loop:
		for {
			_, data, err := ws.ReadMessage()
			if err != nil {
				break loop
			}

			msg := wsMethod(data)
			if msg == nil {
				// logrus.Debugf("c %s bad message: %s", ws.RemoteAddr().String(), string(data))
				if isServerSide {
					wsError(ws, "invalid message format", nil)
				}
				break loop
			}

			select {
			case client <- *msg:
				// logrus.Debugf("[WS-CLIENT] %s recv %d bytes", ws.RemoteAddr().String(), len(msg.Bytes))
			case <-time.After(10 * time.Second):
				// logrus.Debugf("c %s timeout writing", ws.RemoteAddr().String())
				break loop
			}
		}

		// logrus.Debugf("c %s off", ws.RemoteAddr().String())
	}(client)

	return client
}

// wsError
// Send conspiracy protocol error over channel (non-fatal)
//
// Server-socket only
func wsError(ws *wsConn, message string, err error) {
	if err == nil {
		err = errors.New("")
	}
	ws.Logger().WithError(err).Error(message)
	_ = ws.WriteJSON(gin.H{"method": "error", "message": message})
}

// wsCheckOrigin
// verify origin
func wsCheckOrigin(r *http.Request) bool {
	origin := r.Header["Origin"]
	if len(origin) == 0 {
		return true
	}
	u, err := url.Parse(origin[0])
	if err != nil {
		logrus.Debugf("Websocket origin url: failed to parse: %s (error %s)", origin[0], err)
		return false
	}

	res := u.Host == config.Origin
	if !res {
		logrus.Debugf("Websocket bad origin: %s (vs %s)", u.Host, config.Origin)
	}
	return res
}

// console reader
//
// console := wsConsole()
// ...
// msg := <-console
// ...
func wsConsole() chan []string {
	out := make(chan []string)

	go func(out chan []string) {
		scanner := bufio.NewScanner(os.Stdin)

		for scanner.Scan() {
			out <- strings.Split(scanner.Text(), " ")
		}
	}(out)

	return out
}
