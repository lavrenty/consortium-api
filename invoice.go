package main

import (
	"database/sql"
	"fmt"
	"math"
	"time"

	"github.com/Sirupsen/logrus"
	"github.com/lib/pq"
	"github.com/stripe/stripe-go"
	"github.com/stripe/stripe-go/charge"
	"github.com/stripe/stripe-go/currency"
)

// Invoice object
type Invoice struct {
	initialized bool

	ID     uint
	UserID uint
	CallID uint

	// Text description that will be displayed in the invoice when emailed / shown to user
	Description string

	// Extra information about an invoice for the customer’s credit card statement
	// This may be up to 22 characters
	// An arbitrary string to be displayed on your customer's credit card statement. This may be up to 22 characters. As an example, if your website is RunClub and the item you're charging for is a race ticket, you may want to specify a statement_descriptor of RunClub 5K race ticket. The statement description may not include <>"' characters, and will appear on your customer's statement in capital letters. Non-ASCII characters are automatically stripped. While most banks display this information consistently, some may display it incorrectly or not at all.
	StatementDescriptor string

	// Final amount due at this time for this invoice. If the invoice’s total is smaller than the minimum charge amount, for example, or if there is account credit that can be applied to the invoice, the amount_due may be 0. If there is a positive starting_balance for the invoice (the customer owes money), the amount_due will also take that into account. The charge that gets generated for the invoice will be for the amount specified in amount_due
	AmountDue uint
	Currency  string
	Fee       uint

	// Number of payment attempts made for this invoice, from the perspective of the payment retry schedule. Any payment attempt counts as the first attempt, and subsequently only automatic retries increment the attempt count. In other words, manual payment attempts after the first attempt do not affect the retry schedule.
	AttemptCount uint
	// Whether or not the invoice is still trying to collect payment. An invoice is closed if it’s either paid or it has been marked closed. A closed invoice will no longer attempt to collect payment
	Closed bool
	// The time at which payment will next be attempted
	NextPaymentAttempt pq.NullTime

	Paid bool

	// StartingBalance uint64
	// EndingBalance   sql.NullInt64

	CardLastFour string
	CardBrand    string

	UpdatedAt time.Time
	CreatedAt time.Time
}

// NewInvoice generates generic invoice object suitable for saving later
func NewInvoice(call *Call, amount uint) *Invoice {
	if call == nil {
		panic("NewInvoice: call == nil")
	}
	if call.ID == 0 {
		panic("NewInvoice: call.ID == 0")
	}

	return &Invoice{
		initialized: true,
		UserID:      call.CalleeID,
		CallID:      call.ID,
		AmountDue:   amount,
		Fee:         uint(math.Ceil(float64(amount) * BillingServiceFee)),
		Currency:    "usd",
		Closed:      false,
		Paid:        false,
	}
}

// GetInvoice fetches invoice object from database
func GetInvoice(ID interface{}) (*Invoice, error) {
	iv := new(Invoice)
	if err := db.Get(iv, "SELECT * FROM invoices WHERE id = $1", ID); err != nil {
		switch {
		case err == sql.ErrNoRows:
			return nil, nil
		default:
			return nil, err
		}
	}

	iv.initialized = true

	return iv, nil
}

// GetInvoiceByCallID ...
func GetInvoiceByCallID(CallID uint) (*Invoice, error) {
	iv := new(Invoice)
	if err := db.Get(iv, "SELECT * FROM invoices WHERE call_id = $1", CallID); err != nil {
		switch {
		case err == sql.ErrNoRows:
			return nil, nil
		default:
			return nil, err
		}
	}

	iv.initialized = true

	return iv, nil
}

// GetNextPendingInvoice returns next pending invoice for payout
func GetNextPendingInvoice() (*Invoice, error) {
	iv := new(Invoice)
	if err := db.Get(iv, "SELECT * FROM invoices WHERE next_payment_attempt < now() ORDER BY next_payment_attempt ASC LIMIT 1"); err != nil {
		switch {
		case err == sql.ErrNoRows:
			return nil, nil
		default:
			return nil, err
		}
	}

	iv.initialized = true

	return iv, nil
}

// Call returns call from CallID
func (iv *Invoice) Call() (*Call, error) {
	return MustGetCall(iv.CallID)
}

// Merchant returns merchant user id, basically invoice.call.caller_id
func (iv *Invoice) Merchant() (*User, error) {
	call, err := iv.Call()
	if err != nil {
		return nil, err
	}
	return call.Caller()
}

// Charge customer card
func (iv *Invoice) Charge() error {
	if !iv.initialized {
		panic("invoice.Charge: not initialized")
	}
	if iv.Closed || iv.Paid {
		panic(fmt.Sprintf("invoice.Charge() on closed or paid invoice#%d", iv.ID))
	}

	user, err := MustGetUser(iv.UserID)
	if err != nil {
		return err
	}

	// TODO: this should never happen
	if !user.StripeCustomer.Valid {
		user.SetUnpaidInvoice(iv.ID)
		return fmt.Errorf("customer has no stripe account set up %v", user)
	}

	merchant, err := iv.Merchant()
	if err != nil {
		return err
	}
	sa, err := merchant.GetStripeAccount()
	if err != nil {
		return err
	}
	if sa == nil {
		return fmt.Errorf("merchant has no stripe account set up")
	}

	params := &stripe.ChargeParams{
		Amount:    uint64(iv.AmountDue),
		Currency:  currency.USD, // FIXME: always USD here
		Customer:  user.StripeCustomer.String,
		Desc:      iv.Description,
		Statement: iv.StatementDescriptor,
		Fee:       uint64(iv.Fee),
		Dest:      sa.StripeAccountID,
	}
	params.IdempotencyKey = stripe.NewIdempotencyKey()

	iv.AttemptCount++

	// TODO: probably validate that stripe will charge exactly this card
	iv.CardBrand = user.StripeCardBrand.String
	iv.CardLastFour = user.StripeCardLastFour.String

	_, err = charge.New(params)
	if err != nil {
		logrus.Debugf("invoice.Charge() #%d failed with error %#v", iv.ID, err)

		// TODO: https://stripe.com/docs/api#errors handle errors properly, not just pure retry
		// https://github.com/stripe/stripe-go/blob/master/error.go
		stripeErr := err.(*stripe.Error)
		if stripeErr.Type == stripe.CardErr {
			switch stripeErr.Code {
			case stripe.CardDeclined:
				// TODO: implement fraud checks as in here: https://stripe.com/docs/fraud
				// card declined is either insufficient funds or other reason
				// 4000 0000 0000 0341 and 4000 0000 0000 0002 are designed to simulate
				// real declines from a bank.  We don't know for sure whether the card is
				// not good at all, or whether it may just be declined for this
				// particular transaction.  For instance, you could be attempting to
				// charge $200 to a debit card, where the customer has a current balance
				// below $200 (or the same for credit limit).  So, essentially as you
				// said "You can't temporarily charge on that card (no funds, for
				// example)".
				if iv.AttemptCount > 5 {
					// not our first attempt to charge this invoice
					// do nothing and leave this up to user to fix
					iv.NextPaymentAttempt.Scan(nil)
					iv.Save()
					user.SetUnpaidInvoice(iv.ID)
				} else {
					// maybe the first attempt to charge this invoice
					iv.NextPaymentAttempt.Scan(time.Now().AddDate(0, 0, int(iv.AttemptCount)))
					iv.Save()
					user.SetUnpaidInvoice(iv.ID)
				}

			case stripe.ProcessingErr:
				// 4000 0000 0000 0119 this is meant to simulate the card networks
				// telling us that there was a processing error.  When this happens, it
				// can be reasonable to retry the card at a later time.  It does not
				// indicate a technical problem with Stripe (for that, we would return a
				// 500), but may be indicative of a temporary technical issue with one of
				// our banking partners
				if iv.AttemptCount > 3 {
					// do not try more than 3 times
					iv.NextPaymentAttempt.Scan(nil)
					iv.Save()

					// only set as unpaid
					user.SetUnpaidInvoice(iv.ID)
				} else {
					iv.NextPaymentAttempt.Scan(time.Now().Add(time.Duration(iv.AttemptCount) * time.Hour))
					iv.Save()
				}

			default:
				// there is no reason to retry this card as it is bogus
				iv.Save()
				user.SetUnpaidInvoice(iv.ID)
			}
		}
		return err
	}

	// logrus.Debugf("invoice.Charge() successful: %#v", ch)

	iv.Paid = true
	iv.Closed = true
	iv.NextPaymentAttempt.Scan(nil)

	user.ClearUnpaidInvoice()
	return iv.Save()
}

// Save ...
// TODO: update postgres
func (iv *Invoice) Save() error {
	if !iv.initialized {
		panic("use of unitialized &Invoice{}")
	}
	if iv.Description == "" {
		panic("invoice.Save(): use of unitialized .description")
	}
	if iv.StatementDescriptor == "" {
		panic("invoice.Save(): use of unitialized .statementDescriptor")
	}

	return save("invoices", iv)
}
