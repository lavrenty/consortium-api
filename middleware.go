package main

import (
	"fmt"
	"time"

	"net/http"

	"github.com/Sirupsen/logrus"
	"github.com/gin-gonic/gin"
	"github.com/satori/go.uuid"
)

// RequestID ...
func RequestID() gin.HandlerFunc {
	return func(c *gin.Context) {
		id := uuid.NewV4().String()
		c.Set("requestId", id)
		c.Writer.Header().Set("X-Request-Id", id)

		c.Next()
	}
}

// Logger ...
func Logger() gin.HandlerFunc {
	return func(c *gin.Context) {
		logger := logrus.WithFields(logrus.Fields{
			"id":        c.MustGet("requestId").(string),
			"url":       c.Request.URL.String(),
			"method":    c.Request.Method,
			"remote_ip": c.ClientIP(),
		})
		c.Set("logger", logger)
		logger.Infof("Started %s %s", c.Request.Method, c.Request.URL)
		start := time.Now()

		c.Next()

		code := c.Writer.Status()
		logger = c.MustGet("logger").(*logrus.Entry).WithField("duration", time.Since(start).Nanoseconds()/int64(time.Millisecond))
		msg := fmt.Sprintf("Completed %d %s in %v", code, http.StatusText(code), time.Since(start))
		if code >= 500 {
			logger.Error(msg)
		} else if code >= 400 {
			logger.Warn(msg)
		} else {
			logger.Info(msg)
		}
	}
}

// Recovery ...
func Recovery() gin.HandlerFunc {
	return func(c *gin.Context) {
		defer func() {
			err := recover()
			if err != nil {
				// TODO: Implement gin-like stack trace
				c.MustGet("logger").(*logrus.Entry).Error(err)
				c.AbortWithStatus(500)
			}
		}()

		c.Next()
	}
}
