package main

import (
	"database/sql"
	"fmt"
	"time"

	"github.com/lib/pq"
)

// Endorsement struct as in DB
type Endorsement struct {
	UserID     uint
	TopicID    uint
	EndorserID uint

	IsEndorsed sql.NullBool
	CardSeenAt pq.NullTime

	Priority int

	UpdatedAt time.Time
	CreatedAt time.Time
}

// GetEndorsement :of user :by user on :topic
func GetEndorsement(of *User, by *User, topic *Topic) (*Endorsement, error) {
	if of == nil || by == nil || topic == nil {
		panic("invalid arguments")
	}
	res := &Endorsement{}
	err := db.Get(res, "SELECT * FROM endorsements WHERE user_id = $1 AND endorser_id = $2 AND topic_id = $3", of.ID, by.ID, topic.ID)
	switch {
	case err == nil:
		return res, nil
	case err == sql.ErrNoRows:
		return nil, nil
	default:
		return nil, err
	}
}

// MustGetEndorsement ...
func MustGetEndorsement(of *User, by *User, topic *Topic) (*Endorsement, error) {
	res, err := GetEndorsement(of, by, topic)
	if err != nil {
		return nil, err
	}
	if res == nil {
		return nil, fmt.Errorf("endorsement (%d, %d, %d) not found", of.ID, by.ID, topic.ID)
	}
	return res, nil
}

// UpdateCardSeenAt ...
func (e *Endorsement) UpdateCardSeenAt() (err error) {
	_, err = db.NamedExec("UPDATE endorsements SET card_seen_at = now() WHERE user_id = :user_id AND topic_id = :topic_id AND endorser_id = :endorser_id", e)

	return
}
