package main

import "strconv"

// Paginator for page?=... requests
type Paginator struct {
	Page       int
	Limit      int
	Offset     int
	HasMore    bool
	TotalCount int
}

// ToJSON exports paginator data
func (p *Paginator) ToJSON() JSON {
	return JSON{
		"page":        p.Page,
		"has_more":    p.HasMore,
		"total_count": p.TotalCount,
	}
}

// NewPaginator creates paginator object
func NewPaginator(pageStr string, limit int, totalCount int) *Paginator {
	p := &Paginator{Page: 1, Limit: limit, TotalCount: totalCount}

	if pageStr != "" {
		p.Page, _ = strconv.Atoi(pageStr)
		if p.Page <= 0 {
			p.Page = 1
		}
		p.Offset = (p.Page - 1) * p.Limit
		if p.Offset < 0 {
			p.Offset = 0
		}
	}

	if p.TotalCount > p.Offset+p.Limit {
		p.HasMore = true
	}

	return p
}
