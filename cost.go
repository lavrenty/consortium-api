package main

import "math"

// Cost defines price structure of a call
type Cost struct {
	CallerCost     uint
	CalleeCost     uint
	CallCost       uint
	ApplicationFee uint

	Hangup   bool
	Rate     uint
	Duration uint
}

func isFreeTime(duration uint) bool {
	return duration <= CallTimeFree
}

// GetCost returns cost structure for call that lasted `duration` seconds at `rate` price per minute
// duration is in seconds
// rate is in cents
// TODO: currency
func GetCost(duration uint, rate uint, hangup bool) *Cost {
	cost := &Cost{Duration: duration, Rate: rate, Hangup: hangup}

	if rate > 0 && !isFreeTime(duration) {
		minutes := float64(duration) / 60
		if !hangup {
			// if call is not hangup, we round cost calculation to minutes, rather than
			//  updating counter every second. this is purely for the interface
			minutes = math.Floor(minutes)
		}

		cost.CallCost = uint(minutes * float64(rate))

		// we add service base cost (currently $1) only on hangup
		if hangup {
			cost.CalleeCost = BillingServiceBase
		}
		cost.CalleeCost += cost.CallCost
		cost.CallerCost = cost.CallCost - uint(float64(cost.CallCost)*BillingServiceFee)

		// application fee is relevant only on hangup calls (as it includes also base)
		if hangup {
			cost.ApplicationFee = cost.CalleeCost - cost.CallerCost
		}
	}

	return cost
}
