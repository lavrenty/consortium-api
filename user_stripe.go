package main

import (
	"fmt"
	"strconv"

	"github.com/stripe/stripe-go"
	"github.com/stripe/stripe-go/card"
	"github.com/stripe/stripe-go/customer"
)

// GetStripeAccount ... why we need this?
func (u *User) GetStripeAccount() (*StripeAccount, error) {
	return GetStripeAccountByUserID(u.ID)
}

// DeleteStripeAccount ...
// TODO: accounts are not deletable?
func (u *User) DeleteStripeAccount() error {
	a, err := u.GetStripeAccount()
	if err != nil {
		return err
	}
	if a == nil {
		return nil
	}

	return fmt.Errorf("stripe account deletion not implemented")
}

// CreateStripeCustomer ...
func (u *User) CreateStripeCustomer() error {
	if u.StripeCustomer.Valid {
		return nil
	}

	params := &stripe.CustomerParams{
		Desc: u.PublicName(),
	}
	if u.Email.Valid {
		params.Email = u.Email.String
	}
	params.AddMeta("id", strconv.Itoa(int(u.ID)))

	cus, err := customer.New(params)
	if err != nil {
		return err
	}

	u.StripeCustomer.Scan(cus.ID)
	if _, err := db.NamedExec("UPDATE users SET stripe_customer = :stripe_customer WHERE id = :id", u); err != nil {
		return err
	}

	return nil
}

// AddCard by token
func (u *User) AddCard(cardParams *stripe.CardParams) error {
	if err := u.CreateStripeCustomer(); err != nil {
		return err
	}

	cardParams.Customer = u.StripeCustomer.String
	cardParams.Default = true
	c, err := card.New(cardParams)
	if err != nil {
		return err
	}

	u.StripeCardBrand.Scan(string(c.Brand))
	u.StripeCardLastFour.Scan(c.LastFour)

	if _, err := db.NamedExec("UPDATE users SET stripe_card_brand = :stripe_card_brand, stripe_card_last_four = :stripe_card_last_four WHERE id = :id", u); err != nil {
		return err
	}

	// remove others
	params := &stripe.CardListParams{Customer: u.StripeCustomer.String}
	i := card.List(params)
	for i.Next() {
		sc := i.Card()

		if sc.ID != c.ID {
			if _, err := card.Del(sc.ID, &stripe.CardParams{Customer: u.StripeCustomer.String}); err != nil {
				return err
			}
		}
	}

	if u.HasUnpaidInvoice.Valid {
		iv, err := GetInvoice(u.HasUnpaidInvoice.Int64)

		if err != nil {
			// TODO: FIXME: log error
			return nil
		}

		if err := iv.Charge(); err != nil {
			// TODO: FIXME: log error?
			return nil
		}
	}

	return nil
}

// DelCard from user
func (u *User) DelCard() error {
	if u.StripeCustomer.Valid {
		params := &stripe.CardListParams{Customer: u.StripeCustomer.String}
		i := card.List(params)
		for i.Next() {
			sc := i.Card()

			if _, err := card.Del(sc.ID, &stripe.CardParams{Customer: u.StripeCustomer.String}); err != nil {
				return err
			}
		}
	}

	u.StripeCardBrand.Scan(nil)
	u.StripeCardLastFour.Scan(nil)

	if _, err := db.NamedExec("UPDATE users SET stripe_card_brand = :stripe_card_brand, stripe_card_last_four = :stripe_card_last_four WHERE id = :id", u); err != nil {
		return err
	}

	return nil
}
