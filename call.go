package main

import (
	"database/sql"
	"errors"
	"fmt"
	"log"
	"math"
	"time"

	"github.com/Sirupsen/logrus"
	"github.com/lib/pq"
)

// Call is DB reflection
type Call struct {
	ID     uint
	CardID uint

	CallerID uint // merchant
	CalleeID uint // customer

	Duration uint

	Rate uint

	// this is opaque session id from conn provider (opentok, webrtc, ...)
	SessionID string

	CallerConnQuality uint
	CalleeConnQuality uint

	CallerRating uint // Attention! this is set by callee
	CalleeRating uint // Attention! this is set by caller

	StartedAt pq.NullTime
	EndedAt   pq.NullTime

	IsHangup bool

	CallerCost     uint
	CalleeCost     uint
	ApplicationFee uint

	UpdatedAt time.Time
	CreatedAt time.Time
}

// ToJSON returns json
func (c Call) ToJSON() JSON {
	j := JSON{
		"type":      "call",
		"id":        c.ID,
		"caller_id": c.CallerID,
		"callee_id": c.CalleeID,
		"rate":      c.Rate,
		"is_hangup": c.IsHangup,
	}

	if c.IsHangup {
		j["duration"] = c.Duration
		j["started"] = c.StartedAt.Time.Unix()
		j["ended"] = c.EndedAt.Time.Unix()
	}

	return j
}

// GetCall fetches Call object form DB by ID (string, int, uint, whatever)
func GetCall(ID interface{}) (*Call, error) {
	call := new(Call)
	if err := db.Get(call, "SELECT * FROM calls WHERE id = $1", ID); err != nil {
		switch {
		case err == sql.ErrNoRows:
			return nil, nil
		default:
			return nil, err
		}
	}
	return call, nil
}

// MustGetCall ...
func MustGetCall(ID interface{}) (*Call, error) {
	call, err := GetCall(ID)
	if err != nil {
		return nil, err
	}
	if call == nil {
		return nil, fmt.Errorf("call %#v not found", ID)
	}
	return call, nil
}

// GetCallsByCardID returns calls that have this specific card ID
func GetCallsByCardID(ID interface{}) ([]Call, error) {
	calls := []Call{}

	if err := db.Select(&calls, "SELECT * FROM calls WHERE card_id = $1", ID); err != nil {
		switch {
		case err == sql.ErrNoRows:
			return calls, nil
		default:
			return calls, err
		}
	}

	return calls, nil
}

// Create new call in Database
//
func (c *Call) Create() error {
	if c.CardID == 0 || c.CallerID == 0 || c.CalleeID == 0 || c.SessionID == "" {
		return errors.New("call.Create: invalid params")
	}
	rows, err := db.NamedQuery("INSERT INTO calls (card_id, caller_id, callee_id, rate, session_id, started_at) VALUES (:card_id, :caller_id, :callee_id, :rate, :session_id, now()) RETURNING *", c)
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		if err := rows.StructScan(c); err != nil {
			return err
		}
	}
	return nil
}

// Card — get card by its card_id
func (c *Call) Card() (*Card, error) {
	return MustGetCard(c.CardID)
}

// IsCallerID ...
func (c *Call) IsCallerID(userID uint) bool {
	return c.CallerID == userID
}

// IsCalleeID ...
func (c *Call) IsCalleeID(userID uint) bool {
	return c.CalleeID == userID
}

// Caller is consultant / merchant, returns person who is making the call
// Return error when there is database error (internal, connectivity)
// User might be nil if not found
// Payment recipient
func (c *Call) Caller() (*User, error) {
	return MustGetUser(c.CallerID)
}

// Callee is customer returns user who is recieving the call
// Return error when there is database error (internal, connectivity)
// User might be nil if not found
// Payee
func (c *Call) Callee() (*User, error) {
	return MustGetUser(c.CalleeID)
}

// Other ...
func (c *Call) Other(user *User) (*User, error) {
	if c.IsCalleeID(user.ID) {
		return c.Caller()
	} else if c.IsCallerID(user.ID) {
		return c.Callee()
	}

	panic(fmt.Sprintf("call.Other: neither caller nor callee (call %d, user %d)", c.ID, user.ID))
}

// OtherID ...
func (c *Call) OtherID(userID uint) uint {
	if c.CallerID == userID {
		return c.CalleeID
	} else if c.CalleeID == userID {
		return c.CallerID
	}

	panic(fmt.Sprintf("call.OtherID: neither caller nor callee (call %d, user %d)", c.ID, userID))
}

// IsValidPeerID ...
func (c *Call) IsValidPeerID(userID uint) bool {
	return c.CallerID == userID || c.CalleeID == userID
}

// IsFree returns true if rate for the call was free
func (c *Call) IsFree() bool {
	return c.Rate == 0
}

// IsFreeTime returns true if call was stopped before free time expired
// please use this to determine wether or not to bill the call accordingly, do not use any manual comparsion with c.Duration!
func (c *Call) IsFreeTime() bool {
	return isFreeTime(c.Duration)
}

// CostFor calculates how much it would cost for specified user
func (c *Call) CostFor(userID uint) uint {
	if !c.IsValidPeerID(userID) {
		log.Fatalf("call.CostFor invalid user, call #%d user #%d", c.ID, userID)
	}
	if !c.IsHangup {
		log.Fatalf("call.CostFor invalid call #%d state (not hangup)", c.ID)
	}

	if c.IsCalleeID(userID) {
		return c.CalleeCost
	}
	return c.CallerCost
}

// Hangup call in progress
// Panic's if call is already hung up
// TODO: race condition possible, implement red lock around this
// TODO: wrap in tx
func (c *Call) Hangup(d time.Duration) error {
	if c.IsHangup {
		return fmt.Errorf("could not hang up call %d: already hung up", c.ID)
	}

	c.IsHangup = true
	c.Duration = uint(math.Ceil(d.Seconds()))

	cost := GetCost(c.Duration, c.Rate, c.IsHangup)
	c.CallerCost = cost.CallerCost
	c.CalleeCost = cost.CalleeCost
	c.ApplicationFee = cost.ApplicationFee

	if err := db.Get(c, "UPDATE calls SET duration = $1, ended_at = now(), is_hangup = true, caller_cost = $2, callee_cost = $3, application_fee = $4 WHERE id = $5 RETURNING *", c.Duration, c.CallerCost, c.CalleeCost, c.ApplicationFee, c.ID); err != nil {
		return err
	}

	if err := c.incCounters(); err != nil {
		return err
	}

	// TODO: this is unreliable as hell
	if c.Rate > 0 && !c.IsFreeTime() {
		if err := c.ProcessPayment(); err != nil {
			logrus.Debugf("failed to process payment right away %#v", err)
			// hangup is successful even if process payment fails
			return nil
		}
	}

	return nil
}

// incCounters increments basic counters
func (c *Call) incCounters() error {
	if !c.IsHangup {
		panic("call.incCounters on non-hangup call")
	}

	// free time does not count in statistics, but not free calls!
	if c.IsFreeTime() {
		return nil
	}

	callee, err := c.Callee()
	if err != nil {
		return err
	}
	if err = callee.IncQuestionsCount(); err != nil {
		return err
	}

	caller, err := c.Caller()
	if err != nil {
		return err
	}
	if err = caller.IncAnswersCount(); err != nil {
		return err
	}

	card, err := c.Card()
	if err != nil {
		return err
	}
	topics, err := card.Topics()
	if err != nil {
		return err
	}

	for _, topic := range topics {
		if err := caller.Subscribe(&topic, true); err != nil {
			return err
		}
		if err := topic.IncCallsCount(caller); err != nil {
			return err
		}
	}

	taskScoreRebuildAsync(c.CalleeID)
	taskScoreRebuildAsync(c.CallerID)

	return nil
}

// ProcessPayment is the financial heart of this app
// 1. we create customer invoice, which basically states that customer owes us amount
// 2. we create consultant transaction, basically stating that we owe his certain amount
// 3. charge customer
// TODO: wrap in single tx
func (c *Call) ProcessPayment() error {
	if !c.IsHangup {
		panic("call.incCounters on non-hangup call")
	}

	// customer side: create invoice & history item
	iv, err := c.CreateInvoice()
	if err != nil {
		// TODO: this is bad
		return err
	}

	// consultant side: create transaction
	_, err = c.CreateTransaction(iv)
	if err != nil {
		// TODO: this is bad
		return err
	}

	// now as everything is set up, charge customer
	// TODO: this can be (must be ?) async
	if err := iv.Charge(); err != nil {
		return err
	}

	return nil
}

// CreateTransaction for consultant
func (c *Call) CreateTransaction(iv *Invoice) (*Transaction, error) {
	if !c.IsHangup {
		panic("call.incCounters on non-hangup call")
	}

	user, err := c.Caller()
	if err != nil {
		return nil, err
	}

	d := time.Duration(c.Duration) * time.Second

	tx := &Transaction{
		UserID:      user.ID,
		Amount:      int(c.CallerCost),
		Net:         int(c.CallerCost),
		Type:        TxTypeCall,
		Description: fmt.Sprintf("Call %d for %s at $%.02f", c.ID, d.String(), float64(c.Rate)/100),
		Currency:    "usd",
	}
	tx.CallID.Scan(c.ID)

	if err := tx.Save(); err != nil {
		return nil, err
	}

	// TODO: move out to user_balance.go
	if _, err := db.Exec("UPDATE users SET balance = balance + $1 WHERE id = $2", c.CallerCost, user.ID); err != nil {
		return nil, err
	}

	return tx, nil
}

// CreateInvoice for customer
// TODO: check for possible double-invoicing issue?
func (c *Call) CreateInvoice() (*Invoice, error) {
	if !c.IsHangup {
		panic("call.incCounters on non-hangup call")
	}

	d := time.Duration(c.Duration) * time.Second
	iv := NewInvoice(c, uint(c.CalleeCost))
	iv.Description = fmt.Sprintf("Call %d for %s at %.02f usd", c.ID, d.String(), float64(c.Rate)/100)
	iv.StatementDescriptor = fmt.Sprintf("CNSPRC #%d %s", c.ID, d.String())
	if err := iv.Save(); err != nil {
		return nil, err
	}

	return iv, nil
}

// SetRating updates caller_rating/callee_rating depending on user
// user - user taking action, e.g. setting rating (so if he is caller, rating will be set for callee and vice versa)
func (c *Call) SetRating(user *User, rating uint) error {
	changed := false
	field := "caller_rating"

	if c.IsCallerID(user.ID) {
		field = "callee_rating"
		if c.CalleeRating != rating {
			c.CalleeRating = rating
			changed = true
		}
	} else {
		if c.CallerRating != rating {
			c.CallerRating = rating
			changed = true
		}
	}

	if !changed {
		return nil
	}

	query := fmt.Sprintf("UPDATE calls SET %s = $1 WHERE id = $2", field)
	if _, err := db.Exec(query, rating, c.ID); err != nil {
		return err
	}

	taskScoreRebuildAsync(c.OtherID(user.ID))
	taskCallTopicsScoreRebuildAsync(c.ID)

	return nil
}

// SetConnQuality updates conn quality field for specified user
func (c *Call) SetConnQuality(user *User, quality uint) error {
	changed := false
	field := "callee_conn_quality"

	if c.IsCallerID(user.ID) {
		field = "caller_conn_quality"
		if c.CallerConnQuality != quality {
			c.CallerConnQuality = quality
			changed = true
		}
	} else {
		if c.CalleeConnQuality != quality {
			c.CalleeConnQuality = quality
			changed = true
		}
	}

	if !changed {
		return nil
	}

	query := fmt.Sprintf("UPDATE calls SET %s = $1 WHERE id = $2", field)
	if _, err := db.Exec(query, quality, c.ID); err != nil {
		return err
	}

	return nil
}

// EndorseCaller ...
// endorse user on topics if rating > 2 and unendorse if its lower
func (c *Call) EndorseCaller(endorse bool) error {
	card, err := c.Card()
	if err != nil {
		return err
	}

	callee, err := c.Callee()
	if err != nil {
		return err
	}

	// endorsee
	caller, err := c.Caller()
	if err != nil {
		return err
	}

	// this will also trigger score rebuilding
	if err := card.Endorse(callee, caller, endorse); err != nil {
		return err
	}

	taskCallTopicsScoreRebuildAsync(c.ID)

	return nil
}

// TopicsScoreRebuild ...
func (c *Call) TopicsScoreRebuild() error {
	caller, err := c.Caller()
	if err != nil {
		return err
	}

	card, err := c.Card()
	if err != nil {
		return err
	}

	topics, err := card.Topics()
	if err != nil {
		return err
	}
	for _, topic := range topics {
		if err := topic.RebuildScore(caller); err != nil {
			return err
		}
	}

	return nil
}
