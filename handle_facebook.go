package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func handleFacebookConnect(c *gin.Context) {
	var msg struct {
		Token     string   `json:"token" binding:"required"`
		Locale    string   `json:"locale" binding:"required"`
		Languages []string `json:"languages" binding:"required"`
	}

	if err := c.BindJSON(&msg); err != nil {
		apiError(c, 400, "could not parse request params", err)
		return
	}

	f, err := NewFacebookFromUserToken(msg.Token)
	if err != nil {
		apiError(c, 500, "failed to exchange token with facebook", err)
		return
	}

	user, err := f.FindOrCreateUserFromToken()
	if err != nil {
		apiError(c, 500, "failed to get user from facebook", err)
		return
	}

	err = f.UpdateBasicProfile(user)
	if err != nil {
		apiError(c, 500, "failed to update profile data from facebook", err)
		return
	}

	user.FbAccessToken.Scan(f.Token)
	user.Locale = msg.Locale // FIXME: validate msg.Locale input
	if err = save("users", user); err != nil {
		apiErrorInternal(c, err)
		return
	}

	if err = worker.PerformAsync(taskUpdateOtherProfileFacebookString, user.ID, nil); err != nil {
		apiError(c, 500, "internal error", err)
		return
	}

	// process languages
	if err = user.SetLanguages(msg.Languages); err != nil {
		apiErrorInternal(c, err)
		return
	}

	// issue access token
	accessToken, err := user.AccessToken("fb")
	if err != nil {
		apiErrorInternal(c, err)
		return
	}

	c.JSON(http.StatusOK, gin.H{"success": true, "access_token": accessToken, "user": user.ToFullJSON()})
}

// func handleFacebookDisconnect(c *gin.Context) {
// 	user := c.MustGet("current").(*User)
//
// 	if err := user.FacebookDisconnect(); err != nil {
// 		apiError(c, 500, "disconnect error", err)
// 		return
// 	}
//
// 	c.JSON(http.StatusOK, gin.H{"success": true})
// }
