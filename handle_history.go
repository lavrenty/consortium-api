package main

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// GET v1/history - history of all user calls
func handleHistory(c *gin.Context) {
	user := c.MustGet("current").(*User)
	calls := []Call{}

	// set up paginator
	totalCount := 0
	if err := db.Get(&totalCount, "SELECT count(*) FROM calls WHERE (caller_id = $1 OR callee_id = $1) AND is_hangup = true", user.ID); err != nil {
		apiErrorInternal(c, err)
		return
	}
	p := NewPaginator(c.Query("page"), 10, totalCount)

	// load data
	if err := db.Select(&calls, "SELECT * FROM calls WHERE (caller_id = $1 OR callee_id = $1) AND is_hangup = true ORDER BY started_at DESC LIMIT $2 OFFSET $3", user.ID, p.Limit, p.Offset); err != nil {
		apiErrorInternal(c, err)
		return
	}

	// import card and user data
	jsonItems := []JSON{}
	cardIds := []uint{}
	userIds := []uint{}
	for _, call := range calls {
		cardIds = append(cardIds, call.CardID)
		if user.ID != call.CallerID {
			userIds = append(userIds, call.CallerID)
		}
		if user.ID != call.CalleeID {
			userIds = append(userIds, call.CalleeID)
		}
	}

	users, err := GetUsers(userIds)
	if err != nil {
		apiErrorInternal(c, err)
		return
	}
	jsonUsers := map[string]JSON{}
	mapUsers := map[uint]User{}
	for _, u := range users {
		jsonUsers[strconv.Itoa(int(u.ID))] = u.ToJSON()
		mapUsers[u.ID] = u
	}

	cards, err := GetCards(cardIds)
	if err != nil {
		apiErrorInternal(c, err)
		return
	}
	mapCards := map[uint]Card{}
	for _, card := range cards {
		mapCards[card.ID] = card
	}

	jsonTopics := map[string]JSON{}
	for _, call := range calls {
		card := mapCards[call.CardID]
		topics, err := card.Topics()
		if err != nil {
			apiErrorInternal(c, err)
			return
		}
		topicIds := []uint{}
		for _, t := range topics {
			topicIds = append(topicIds, t.ID)
			tid := strconv.Itoa(int(t.ID))
			if _, ok := jsonTopics[tid]; !ok {
				jsonTopics[tid] = t.ToJSON()
			}
		}

		callCost := uint(0)
		if !call.IsFree() && !call.IsFreeTime() {
			callCost = uint(float64(call.Rate) * (float64(call.Duration) / 60))
		}

		j := JSON{
			"id":        call.ID,
			"type":      "history_item",
			"call_id":   call.ID,
			"callee_id": call.CalleeID,
			"caller_id": call.CallerID,
			"duration":  call.Duration,
			"rate":      call.Rate,
			// "effective_rate": card.Rate,
			"text":      card.Text,
			"language":  card.Language,
			"topic_ids": topicIds,
			"cost":      call.CostFor(user.ID),
			"call_cost": callCost,
			"currency":  "usd",
			"started":   call.StartedAt.Time.Unix(),
			"ended":     call.EndedAt.Time.Unix(),
		}

		// quality
		if call.CalleeID == user.ID {
			j["conn_quality"] = call.CalleeConnQuality
		} else {
			j["conn_quality"] = call.CallerConnQuality
		}

		// helpful
		if call.CalleeID == user.ID {
			j["rating"] = call.CallerRating // rate for other caller by callee
		} else {
			j["rating"] = call.CalleeRating
		}

		// invoice
		if call.IsCalleeID(user.ID) && !call.IsFree() && !call.IsFreeTime() {
			iv, err := GetInvoiceByCallID(call.ID)
			if err != nil {
				apiErrorInternal(c, err)
				return
			}
			if iv != nil {
				charge := JSON{
					"paid":     iv.Paid,
					"amount":   iv.AmountDue,
					"currency": iv.Currency,
				}

				if iv.Paid {
					charge["card_last_four"] = iv.CardLastFour
					charge["card_brand"] = iv.CardBrand
				}

				j["charge"] = charge
			}
		}

		if !call.IsFree() && !call.IsFreeTime() {
			if call.IsCalleeID(user.ID) {
				j["base_fee"] = BillingServiceBase
			} else {
				j["conspiracy_fee"] = call.ApplicationFee - BillingServiceBase
			}
		}

		jsonItems = append(jsonItems, j)
	}

	c.JSON(http.StatusOK, gin.H{"success": true, "has_unpaid_invoice": user.HasUnpaidInvoice.Valid, "items": jsonItems, "users": jsonUsers, "topics": jsonTopics, "page": p.ToJSON()})
}

// чек для платящего (callee)
// BREAKDOWN
// total ........... 30$ (cost, currency)
// minutes ......... 20 (duration)
// rate ............ 1.3$ (rate)
// base fee ........ 1.00$ (application fee)
// CHARGE
// paid ........... true (paid)
// VISA ****5233    30$  (card brand, card_last_four, amount, currency)
//
// чек для получателя (caller)
// net amount ...... 26$ (cost, currency)
// minutes ......... 20 (duration)
// rate ............ 1.3$ (rate)
// conspiracy fee .. -2.6$ (application fee)
//
func handleHistoryItem(c *gin.Context) {
	user := c.MustGet("current").(*User)
	call := c.MustGet("call").(*Call)

	if !call.IsHangup || !call.IsValidPeerID(user.ID) {
		apiError(c, http.StatusForbidden, "permission denied", nil)
		return
	}

	callCost := uint(0)
	if !call.IsFree() && !call.IsFreeTime() {
		callCost = uint(float64(call.Rate) * (float64(call.Duration) / 60))
	}

	jsonInvoice := JSON{
		"cost":      call.CostFor(user.ID),
		"call_cost": callCost,
		"currency":  "usd",
		"started":   call.StartedAt.Time.Unix(),
		"ended":     call.EndedAt.Time.Unix(),
		"duration":  call.Duration,
		"rate":      call.Rate,
		"callee_id": call.CalleeID,
		"caller_id": call.CallerID,
	}

	if call.IsCalleeID(user.ID) && !call.IsFree() && !call.IsFreeTime() {
		iv, err := GetInvoiceByCallID(call.ID)
		if err != nil {
			apiErrorInternal(c, err)
			return
		}
		if iv != nil {
			charge := JSON{
				"paid":     iv.Paid,
				"amount":   iv.AmountDue,
				"currency": iv.Currency,
			}

			if iv.Paid {
				charge["card_last_four"] = iv.CardLastFour
				charge["card_brand"] = iv.CardBrand
			}

			jsonInvoice["charge"] = charge
		}
	}

	if !call.IsFree() && !call.IsFreeTime() {
		if call.IsCalleeID(user.ID) {
			jsonInvoice["base_fee"] = BillingServiceBase
		} else {
			jsonInvoice["conspiracy_fee"] = call.ApplicationFee - BillingServiceBase
		}
	}

	c.JSON(http.StatusOK, gin.H{"success": true, "item": jsonInvoice})
}
