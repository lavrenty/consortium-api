package main

import "testing"

func TestDbUnderscore(t *testing.T) {
	r := dbUnderscore("test")
	if r != "test" {
		t.Fatal(r)
	}

	r = dbUnderscore("ThisIsTest")
	if r != "this_is_test" {
		t.Fatal(r)
	}

	r = dbUnderscore("ID")
	if r != "id" {
		t.Fatal(r)
	}

	r = dbUnderscore("UserID")
	if r != "user_id" {
		t.Fatal(r)
	}

	r = dbUnderscore("DigitsAccessTokenURL")
	if r != "digits_access_token_url" {
		t.Fatal(r)
	}
}

func TestDbFields(t *testing.T) {
	f := dbFields(&User{})
	for _, name := range f {
		switch name {
		case "id", "name", "first_name", "middle_name", "last_name", "birthday", "gender", "locale", "languages", "email", "picture", "fb_id",
			"fb_access_token", "fb_permissions", "account_kit_id", "account_kit_number", "account_kit_access_token", "last_seen_at", "updated_at", "created_at", "balance_currency", "pending_balance", "pending_balance_currency", "next_notification_at",
			"notifications_frequency",
			"fb_card_seen_at", "fb_card_count", "stripe_customer", "stripe_card_brand", "stripe_card_last_four", "balance", "has_unpaid_invoice",
			"questions_count", "answers_count", "is_disabled", "disabled_reason", "disabled_by", "notifications", "notifications_card_seen_at",
			"notifications_card_count":
		default:
			t.Fatal("bad field "+name+" in user", f)
		}
	}

	var s struct {
		ID               uint
		SomeName         uint
		SomeID           uint
		Visible          bool
		RenamedField     uint `db:"hello"`
		Ignored          uint `db:"-"`
		mustBeIgnoredToo uint
	}
	f = dbFields(s)
	for _, name := range f {
		switch name {
		case "id", "some_name", "some_id", "visible", "hello":
		default:
			t.Fatal("bad field "+name+" in user", f)
		}
	}
}

func TestDbFieldsFor(t *testing.T) {
	var s struct {
		ID      uint
		Val     string
		ignored uint
	}

	// forSelect
	r := dbFieldsFor(forSelect, &s, nil)
	if r != "id, val" {
		t.Fatalf("got %s", r)
	}

	r = dbFieldsFor(forSelect, &s, []string{"val"})
	if r != "val" {
		t.Fatalf("got %s", r)
	}

	// forUpdate
	r = dbFieldsFor(forUpdate, &s, nil)
	if r != "val = :val" {
		t.Fatalf("got %s", r)
	}

	r = dbFieldsFor(forUpdate, &s, []string{"val"})
	if r != "val = :val" {
		t.Fatalf("got %s", r)
	}

	// forInsertValues
	r = dbFieldsFor(forInsertValues, &s, nil)
	if r != ":val" {
		t.Fatalf("got %s", r)
	}

	r = dbFieldsFor(forInsertValues, &s, []string{"val"})
	if r != ":val" {
		t.Fatalf("got %s", r)
	}
}
