package main

import (
	"math/rand"
	"testing"
	"time"

	"github.com/stripe/stripe-go"
)

func TestInvoiceCharge(t *testing.T) {
	call := testCreateCall(t, uint(CardMinRate*rand.Intn(500)))

	if err := call.Hangup(time.Duration(CallTimeFree+rand.Intn(600)) * time.Second); err != nil {
		t.Fatal(err)
	}

	user, _ := call.Caller()
	if user.Balance == 0 {
		t.Fatal("expected positive balance")
	}
}

func TestGetNextPendingInvoice(t *testing.T) {
	_, err := GetNextPendingInvoice()

	if err != nil {
		t.Fatal(err)
	}
}

func TestInvoiceChargeDeclined(t *testing.T) {
	call := testCreateCall(t, uint(CardMinRate*rand.Intn(500)))

	user, _ := call.Callee()
	// 4000000000000341 Attaching this card to a Customer object will succeed, but attempts to charge the customer will fail.
	if err := user.AddCard(&stripe.CardParams{
		Number: "4000000000000341",
		CVC:    "234",
		Month:  "10",
		Year:   "2020",
		Name:   "Test User (card declined)",
	}); err != nil {
		t.Fatal(err)
	}
	if err := call.Hangup(time.Duration(CallTimeFree+rand.Intn(600)) * time.Second); err != nil {
		t.Fatal(err)
	}

	user, _ = call.Callee()
	if user.HasUnpaidInvoice.Valid != true {
		t.Fatal("expected unpaid invoice")
	}
	card, _ := call.Card()
	if ok, _ := user.CanCreate(card); ok {
		t.Fatal("expected restricted permissions for user")
	}
}

// func TestInvoiceChargeExpired(t *testing.T) {
// 	call := testCreateCall(t, uint(CardMinRate*rand.Intn(500)))
//
// 	user, _ := call.Callee()
// 	if err := user.AddCard(&stripe.CardParams{
// 		Number: "4000000000000069",
// 		CVC:    "234",
// 		Month:  "10",
// 		Year:   "2020",
// 		Name:   "Test User (card declined)",
// 	}); err != nil {
// 		t.Fatal(err)
// 	}
// 	if err := call.Hangup(time.Duration(CallTimeFree+rand.Intn(600)) * time.Second); err != nil {
// 		t.Fatal(err)
// 	}
// }
//
// func TestInvoiceChargeProcessingError(t *testing.T) {
// 	call := testCreateCall(t, uint(CardMinRate*rand.Intn(500)))
//
// 	user, _ := call.Callee()
// 	if err := user.AddCard(&stripe.CardParams{
// 		Number: "4000000000000119",
// 		CVC:    "234",
// 		Month:  "10",
// 		Year:   "2020",
// 		Name:   "Test User (card declined)",
// 	}); err != nil {
// 		t.Fatal(err)
// 	}
// 	if err := call.Hangup(time.Duration(CallTimeFree+rand.Intn(600)) * time.Second); err != nil {
// 		t.Fatal(err)
// 	}
// }
