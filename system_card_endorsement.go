package main

// SystemCardEndorsement ..
type SystemCardEndorsement struct {
	UserID     uint // Endorsed
	TopicID    uint
	EndorserID uint
}

// NewSystemCardEndorsement ...
func NewSystemCardEndorsement(e *Endorsement) SystemCardEndorsement {
	if e == nil {
		panic("invalid argument")
	}

	return SystemCardEndorsement{UserID: e.UserID, TopicID: e.TopicID, EndorserID: e.EndorserID}
}

// Type ...
func (c SystemCardEndorsement) Type() string {
	return "endorsement"
}

// ToJSON ...
// because it's result of pending endorsements, endorser == current user
func (c SystemCardEndorsement) ToJSON() JSON {
	topic, err := MustGetTopic(c.TopicID)
	if err != nil {
		panic(err)
	}
	endorsed, err := MustGetUser(c.UserID)
	if err != nil {
		panic(err)
	}

	return JSON{
		"user":  endorsed.ToJSON(),
		"topic": topic.ToJSON(),
	}
}

// Close ...
func (c SystemCardEndorsement) Close() error {
	user, err := MustGetUser(c.UserID)
	if err != nil {
		return err
	}
	endorser, err := MustGetUser(c.EndorserID)
	if err != nil {
		return err
	}
	topic, err := MustGetTopic(c.TopicID)
	if err != nil {
		return err
	}

	e, err := MustGetEndorsement(user, endorser, topic)
	if err != nil {
		return err
	}
	return e.UpdateCardSeenAt()
}
