// 5871

// user.notifications bool
//
// notifications ios_device_id = string (enabled true)
//
// /user/notifications post,delete (device id)
//
// /logout - with ios_device_id (disabled true)
//
// system card notification
//
// notifications themselves
//
// cardID, (text alert), category == cardID
//
// get /cards/:card_id/….

package main

import "github.com/gin-gonic/gin"

func handleNotificationsEnable(c *gin.Context) {
	user := c.MustGet("current").(*User)

	var args struct {
		Token     string `binding:"required"`
		Frequency string
	}

	if err := c.BindJSON(&args); err != nil {
		apiError(c, 403, "invalid arguments", err)
		return
	}

	if err := user.EnableNotifications(args.Token); err != nil {
		apiErrorInternal(c, err)
		return
	}

	if args.Frequency != "" {
		if err := user.SetNotificationsFrequency(args.Frequency); err != nil {
			apiErrorInternal(c, err)
			return
		}
	}

	// clear notifications and reset counters
	if err := user.MarkAsNotified(); err != nil {
		apiErrorInternal(c, err)
		return
	}

	apiSuccess(c)
}

func handleNotificationsDisable(c *gin.Context) {
	user := c.MustGet("current").(*User)

	var args struct {
		Token string `binding:"required"`
	}

	if err := c.BindJSON(&args); err != nil {
		apiError(c, 403, "invalid arguments", err)
		return
	}

	if err := user.DisableNotifications(args.Token); err != nil {
		apiErrorInternal(c, err)
		return
	}

	apiSuccess(c)
}

// POST /debug/pusher/:user/:card - send test push notification to user :user that card :card is available
func handleNotificationsPushDebug(c *gin.Context) {
	other := c.MustGet("user").(*User)
	card := c.MustGet("card").(*Card)

	if err := taskPushCardNotificationAsync(other.ID, card.ID); err != nil {
		apiErrorInternal(c, err)
		return
	}

	apiSuccess(c)
}
