// radar handles redis `card:%d` object and related notifications
package main

import (
	"errors"
	"fmt"
	"math/rand"
	"strconv"
	"time"

	"github.com/Sirupsen/logrus"

	"gopkg.in/redis.v3"
)

// ErrRadarBusy ...
var ErrRadarBusy = errors.New("radar busy")

// Radar ...
type Radar struct {
	dashboard *Dashboard
	redis     *redis.Client
	log       *logrus.Entry

	cardID uint
	C      chan uint // incoming call ids, e.g. Call.ID
	V      chan uint // viewer ids for radar

	stop chan struct{}
}

// NewRadar creates new radar entry for provided cardID
// once created, all dashboards are notified about new entry
// card object is kept alive in dashboard stack, to ensure two things:
//  - stale objects get removed automaticaly after expiration (dashboardKeepAlive period)
//  - so that you could get notified of upcoming calls (handled through dashboard.go)
// Created process must be stopped with r.Stop()
func (d *Dashboard) NewRadar(cardID uint) (*Radar, error) {
	card, err := MustGetCard(cardID)
	if err != nil {
		return nil, err
	}

	d.rmu.RLock()
	_, ok := d.radars[card.ID]
	d.rmu.RUnlock()

	if ok {
		return nil, ErrRadarBusy
	}

	radar := &Radar{
		dashboard: d,
		redis:     d.redis,
		log: d.log.WithFields(logrus.Fields{
			"card":    cardID,
			"context": "radar",
		}),
		cardID: card.ID,
		C:      make(chan uint),
		V:      make(chan uint),
		stop:   make(chan struct{}),
	}

	d.rmu.Lock()
	d.radars[card.ID] = radar
	d.rmu.Unlock()

	if err := radar.join(); err != nil {
		return nil, err
	}

	go radar.keepalive()

	return radar, nil
}

func (r *Radar) keepalive() {
	for {
		select {
		case <-r.stop:
			return
		case <-time.After(dashboardKeepAlive/2 + time.Duration(rand.Intn(5))*time.Second):
			// r.log.Debugf("keepalive card%d touch", r.cardID)
			if err := r.touch(); err != nil {
				r.log.Warnf("keepalive %v keepalive touch failed with %v\n", r.cardID, err)
			}
		}
	}
}

func (r *Radar) keyViews() string {
	return fmt.Sprintf("radar:views:%v", r.cardID)
}

func (r *Radar) keyID() string {
	return strconv.Itoa(int(r.cardID))
}

// create card and notify dashboard that we've joined
func (r *Radar) join() error {
	if err := r.redis.Incr(r.keyViews()).Err(); err != nil {
		return err
	}

	if err := r.redis.Expire(r.keyViews(), dashboardKeepAlive).Err(); err != nil {
		return err
	}

	score := float64(time.Now().Unix())
	if err := r.redis.ZAdd(dashboardListKey, redis.Z{Score: score, Member: r.keyID()}).Err(); err != nil {
		return err
	}

	return r.dashboard.localJoin(r.cardID)
}

// remove card from dashboard
func (r *Radar) leave() error {
	if err := r.redis.ZRem(dashboardListKey, r.keyID()).Err(); err != nil {
		return err
	}

	return r.dashboard.localLeave(r.cardID)
}

func (r *Radar) touch() error {
	score := float64(time.Now().Unix())
	if err := r.redis.ZAddXX(dashboardListKey, redis.Z{Score: score, Member: r.keyID()}).Err(); err != nil {
		return err
	}

	return r.redis.Expire(r.keyViews(), dashboardKeepAlive).Err()
}

// Stop keepalive routine and leave with the card
func (r *Radar) Stop() error {
	r.stop <- struct{}{}

	r.dashboard.rmu.Lock()
	delete(r.dashboard.radars, r.cardID)
	r.dashboard.rmu.Unlock()

	return r.leave()
}
