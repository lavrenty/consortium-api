package main

import (
	"database/sql"
	"time"

	"github.com/lib/pq"
	"github.com/stripe/stripe-go/client"
)

// StripeAccount ...
type StripeAccount struct {
	ID     uint
	UserID uint

	StripeAccountID string
	Country         string
	DefaultCurrency string

	TransfersEnabled bool
	DetailsSubmitted bool

	DisabledReason sql.NullString
	DueBy          pq.NullTime

	Secret  string
	Publish string

	UpdatedAt time.Time
	CreatedAt time.Time
}

// ToJSON exports
func (sa *StripeAccount) ToJSON() JSON {
	j := JSON{
		"id":                sa.StripeAccountID,
		"country":           sa.Country,
		"transfers_enabled": sa.TransfersEnabled,
		"details_submitted": sa.DetailsSubmitted,
		"default_currency":  sa.DefaultCurrency,
		"publish":           sa.Publish,
	}

	if sa.DisabledReason.Valid {
		j["disabled_reason"] = sa.DisabledReason.String
	}
	if sa.DueBy.Valid {
		j["due_by"] = sa.DueBy.Time
	}

	return j
}

// IsPayable returns wether we can accept payments on this stripe account ID
func (sa *StripeAccount) IsPayable() bool {
	return sa.TransfersEnabled
}

// GetStripeAccount from database
func GetStripeAccount(accountID string) (*StripeAccount, error) {
	sa := &StripeAccount{}
	if err := db.Get(sa, "SELECT * FROM stripe_accounts WHERE stripe_account_id = $1", accountID); err != nil {
		switch {
		case err == sql.ErrNoRows:
			return nil, nil
		default:
			return nil, err
		}
	}

	return sa, nil
}

// GetStripeAccountByUserID from database
func GetStripeAccountByUserID(userID uint) (*StripeAccount, error) {
	sa := &StripeAccount{}
	if err := db.Get(sa, "SELECT * FROM stripe_accounts WHERE user_id = $1", userID); err != nil {
		switch {
		case err == sql.ErrNoRows:
			return nil, nil
		default:
			return nil, err
		}
	}

	return sa, nil
}

// GetStripeClient returns initialized stripe API client to be used with this account
func (sa *StripeAccount) GetStripeClient() *client.API {
	sc := &client.API{}
	sc.Init(sa.Secret, nil)

	return sc
}

// Save object into database
func (sa *StripeAccount) Save() error {
	return save("stripe_accounts", sa)
}
