package main

import (
	"math/rand"
	"testing"
	"time"
)

func initTestCall(t *testing.T) (*User, *User, *Call) {
	andy := testGetUser(t, 1)
	jovan := testGetUser(t, 2)

	card := &Card{UserID: andy.ID, Text: "I would like to know more about Bitcoins, how they work?", Rate: 50}
	if err := card.Create(); err != nil {
		t.Fatal(err)
	}
	if err := card.Join(*testGetTopic(t, 1)); err != nil {
		t.Fatal(err)
	}
	if err := card.Join(*testGetTopic(t, 2)); err != nil {
		t.Fatal(err)
	}
	if err := card.Join(*testGetTopic(t, 3)); err != nil {
		t.Fatal(err)
	}

	call := &Call{CardID: card.ID, CallerID: jovan.ID, CalleeID: card.UserID, Rate: card.Rate, SessionID: "RANDOMSTUFF"}
	if err := call.Create(); err != nil {
		t.Fatal(err)
	}

	return andy, jovan, call
}

func runTestClientEmulator(sock chan string) {
	cmds := []string{
		"live",
		"live",
		"live",
		"live",
		"live",
		"live",
		"dead",
		"live",
		"live",
		"live",
		"dead",
		"live",
		"live",
		"live",
		"hangup",
		"live",
		"live",
		"live",
	}

	for {
		time.Sleep(time.Duration(rand.Intn(1000)) * time.Millisecond)

		sock <- cmds[rand.Intn(len(cmds))]
	}
}

func runTestCallEmulator(t *testing.T, call *Call, user *User) {
	s, sock := GetCallState(call, user)
	if s == nil {
		t.Fatal(user.ID, call.ID)
	}

	client := make(chan string)
	go runTestClientEmulator(client)

loop:
	for {
		select {
		case msg, ok := <-client:
			if !ok {
				break loop
			}
			t.Log(call.ID, user.ID, msg)

			switch msg {
			case "live":
				s.Live(user)
			case "dead":
				s.Dead(user)
			case "hangup":
				s.Hangup(user)
				break loop
			}
			t.Log(call.ID, user.ID, s.ToJSON(user))
		case _, ok := <-sock:
			if !ok {
				break loop
			}
			t.Log(call.ID, user.ID, s.ToJSON(user))
		}
	}

	t.Log(call.ID, user.ID, "done")
}

func validateStateJSON(t *testing.T, j JSON, live bool, hangup bool) {
	if j["live"].(bool) != live {
		t.Fatal("live is not ", live, j)
	}
	if j["hangup"].(bool) != hangup {
		t.Fatal("hangup is not ", hangup, j)
	}
}

func timedDiscard(t *testing.T, sock chan bool) {
	select {
	case <-sock:
		t.Log(sock, "read ok")
	case <-time.After(1 * time.Second):
		t.Fatal(sock, "timeout")
	}
}

func TestCallState(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping long test")
	}

	rand.Seed(time.Now().UnixNano())

	andy, jovan, call := initTestCall(t)

	go runTestCallEmulator(t, call, jovan)
	runTestCallEmulator(t, call, andy)

	callDup := testGetCall(t, call.ID)

	t.Log(callDup.Duration, call.Duration)
}

func TestCallStateBehaviour(t *testing.T) {
	rand.Seed(time.Now().UnixNano())

	u1, u2, call := initTestCall(t)

	s1, sock1 := GetCallState(call, u1)
	if s1 == nil {
		t.Fatal(call.ID, u1.ID)
	}
	t.Log(s1.ToJSON(u1))

	// check dead before live
	t.Log(1)
	s1.Dead(u1)
	validateStateJSON(t, s1.ToJSON(u1), false, false)
	t.Log(s1.ToJSON(u1))

	// check live before other
	t.Log(2)
	s1.Live(u1)
	validateStateJSON(t, s1.ToJSON(u1), false, false)
	t.Log(s1.ToJSON(u1))

	s2, sock2 := GetCallState(call, u2)
	if s2 == nil {
		t.Fatal(call.ID, u2.ID)
	}

	// live must not be false unless s2 issues live command
	t.Log(3)
	validateStateJSON(t, s1.ToJSON(u1), false, false)
	t.Log(s1.ToJSON(u1))

	// but s2 is loco, he says 'dead'
	t.Log(4)
	s2.Dead(u2)
	timedDiscard(t, sock1)
	validateStateJSON(t, s1.ToJSON(u1), false, false)
	t.Log(s1.ToJSON(u1))

	// s
	t.Log(5)
	s1.Live(u1)
	// timedDiscard(t, sock2)
	validateStateJSON(t, s2.ToJSON(u2), false, false)
	t.Log(s1.ToJSON(u1))

	t.Log(6)
	s2.Live(u2)
	timedDiscard(t, sock1)
	validateStateJSON(t, s1.ToJSON(u1), true, false)
	t.Log(s1.ToJSON(u1))

	//
	// on live, someone goes dead
	//
	t.Log(7)
	s2.Dead(u2)
	timedDiscard(t, sock1)
	validateStateJSON(t, s1.ToJSON(u1), false, false)
	validateStateJSON(t, s2.ToJSON(u2), false, false)
	t.Log(s1.ToJSON(u1))

	// and then back to live
	t.Log(8)
	s2.Live(u2)
	timedDiscard(t, sock1)
	validateStateJSON(t, s1.ToJSON(u1), true, false)
	validateStateJSON(t, s2.ToJSON(u2), true, false)
	t.Log(s1.ToJSON(u1))

	//
	// extra connect should fail
	//
	s3, _ := GetCallState(call, u1)
	if s3 != nil {
		t.Fatal(call.ID, u1.ID)
	}

	// now s2 thinks we're done and closes socket
	s1.Hangup(u1)
	t.Log(9)
	timedDiscard(t, sock2)
	validateStateJSON(t, s1.ToJSON(u1), false, true)
	validateStateJSON(t, s2.ToJSON(u2), false, true)
	t.Log(s1.ToJSON(u1))

}
