package main

// Meta ...
type Meta struct {
	CardID   uint
	UserID   uint
	CallerID uint
	Rate     uint
	Language string
	TopicIds []uint

	// views count for current meta
	Views uint

	// wether it's in radar or not
	Live bool
}

// ToMeta ...
func (c *Card) ToMeta() (*Meta, error) {
	m := &Meta{
		CardID:   c.ID,
		UserID:   c.UserID,
		Rate:     c.Rate,
		Language: c.Language,
		Views:    c.ViewsCount,
		Live:     false,
	}

	topicIds, err := c.TopicIds()
	if err != nil {
		return nil, err
	}
	m.TopicIds = topicIds

	return m, nil
}
