package main

import (
	"os"
	"os/signal"
	"time"

	"github.com/Sirupsen/logrus"

	"gopkg.in/matryer/try.v1"
)

func handleInvoiceSchedule() (bool, error) {
	iv, err := GetNextPendingInvoice()
	if err != nil {
		return false, err
	}

	if iv == nil {
		return false, nil
	}

	logrus.Debugf("handleInvoiceSchedule: processing invoice %d\n", iv.ID)

	if err := iv.Charge(); err != nil {
		return false, err
	}

	return true, nil
}

func schedWrapper(stopChan chan bool, handler func() (bool, error)) {
	for {
		ok, err := handler()

		// just display error
		if err != nil {
			ok = false
			logrus.Warnf("schedWrapper error %#v", err)
		}

		// means queue was empty or error
		if !ok {
			select {
			case <-time.After(10 * time.Second):
				break
			case <-stopChan:
				return
			}
		}
	}
}

// Scheduler pushes tasks at specified timestamps into async queue for immediate execution
func Scheduler() {
	mu := locker.NewMutex("scheduler", 10*time.Second)
	err := try.Do(func(attempt int) (bool, error) {
		err := mu.Lock()
		if err != nil {
			time.Sleep(time.Second)
		}

		return attempt < 10, err
	})
	if err != nil {
		logrus.Fatalf("locking failed %v\n", err)
	}
	defer mu.Unlock()

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt, os.Kill)

	stopChan := make(chan bool)
	go schedWrapper(stopChan, handleInvoiceSchedule)

loop:
	for {
		select {
		case <-stop:
			break loop
		case <-time.After(5 * time.Second):
			if err := mu.Extend(); err != nil {
				panic(err)
			}
		}
	}

	logrus.Debugf("scheduler: stopping")
	stopChan <- true

	return
}
