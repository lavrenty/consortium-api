package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"math/rand"
	"strconv"
	"sync"
	"time"

	"github.com/Sirupsen/logrus"

	"gopkg.in/redis.v3"
)

const dashboardKeepAlive = 60 * time.Second

const dashboardListKey = "dashboard"
const dashboardJoinChannel = "dashboard:joins"
const dashboardLeaveChannel = "dashboard:leaves"
const dashboardUpdateViewsChannel = "dashboard:updates:views"
const dashboardUpdateCallerChannel = "dashboard:upadtes:caller"
const dashboardCallChannel = "dashboard:calls"

// ErrDashboardCardBusy returns when someone locked card before you
var ErrDashboardCardBusy = errors.New("card busy")

// ErrDashboardCardNoResponse returned when
var ErrDashboardCardNoResponse = errors.New("no response")

// DashboardChannelsConfig ...
type DashboardChannelsConfig struct {
	join         string
	leave        string
	updateViews  string
	updateCaller string
	calls        string
}

// Dashboard ...
type Dashboard struct {
	token  int64
	redis  *redis.Client
	client *redis.Client
	pubsub *redis.PubSub
	log    *logrus.Entry

	channels DashboardChannelsConfig

	stopSyncer chan struct{}
	stop       int32

	// metas are managed by dashboard instance itself
	mmu   sync.RWMutex
	metas map[uint]*Meta // key is cardID

	// observers are managed by Observer struct
	omu       sync.RWMutex
	observers map[uint][]*Observer // key is cardID

	// radars are managed by Radar struct
	rmu    sync.RWMutex
	radars map[uint]*Radar
}

type dashboardMessageHeader struct {
	Token  int64
	CardID uint
}

type dashboardUpdateViewsMessage struct {
	dashboardMessageHeader

	Views  uint
	UserID uint
}

type dashboardUpdateCallerMessage struct {
	dashboardMessageHeader

	CallerID uint
}

type dashboardCallMessage struct {
	dashboardMessageHeader

	CallID uint
}

// NewDashboard ...
func NewDashboard(r *redis.Client) (*Dashboard, error) {
	d := &Dashboard{
		token:      rand.Int63(),
		redis:      r,
		metas:      make(map[uint]*Meta, 0),
		observers:  make(map[uint][]*Observer, 0),
		radars:     make(map[uint]*Radar, 0),
		stop:       0,
		stopSyncer: make(chan struct{}),
	}

	d.channels = DashboardChannelsConfig{
		join:         d.namespace(dashboardJoinChannel),
		leave:        d.namespace(dashboardLeaveChannel),
		updateViews:  d.namespace(dashboardUpdateViewsChannel),
		updateCaller: d.namespace(dashboardUpdateCallerChannel),
		calls:        d.namespace(dashboardCallChannel),
	}

	d.log = logrus.WithFields(logrus.Fields{
		"token":   d.token,
		"context": "dashboard",
	})

	// we need new instance of redis client as pubsub locks current connection
	d.client = redis.NewClient(&redis.Options{
		Addr:     config.Redis.Addrs[0],
		Password: config.Redis.Password,
		DB:       config.Redis.DB,
		PoolSize: config.Redis.PoolSize,
	})

	pubsub, err := d.client.Subscribe(d.channels.updateViews, d.channels.updateCaller, d.channels.calls, d.channels.join, d.channels.leave)
	if err != nil {
		return nil, err
	}
	d.pubsub = pubsub

	if err := d.prefetch(); err != nil {
		d.pubsub.Close()
		return nil, err
	}

	go d.syncer()
	go d.listener()

	return d, nil
}

func (d *Dashboard) namespace(name string) string {
	return fmt.Sprintf("%d:%s", config.Redis.DB, name)
}

// Stop dashboard process
// TODO: remove all observers and radars for clean shutdown
func (d *Dashboard) Stop() {
	// d.log.Debugf("d %v close", d.token)

	d.stopSyncer <- struct{}{}
	atomicStoreBool(&d.stop, true)

	d.pubsub.Close()
	d.client.Close()
}

func (d *Dashboard) list() ([]uint, error) {
	result := []uint{}

	min := strconv.Itoa(int(time.Now().Add(-dashboardKeepAlive).Unix()))
	max := "+inf"

	ids, err := d.redis.ZRangeByScore(dashboardListKey, redis.ZRangeByScore{Min: min, Max: max}).Result()
	if err != nil {
		return nil, err
	}

	for _, id := range ids {
		cardID, _ := strconv.Atoi(id)
		result = append(result, uint(cardID))
	}

	return result, nil
}

// prefetch loads all cards that are currently active on dashboard
func (d *Dashboard) prefetch() error {
	// d.log.Debug("prefetch start")

	ids, err := d.list()
	if err != nil {
		return err
	}

	for _, id := range ids {
		if err := d.add(id); err != nil {
			return err
		}
	}
	// d.log.Debug("prefetch done")

	return nil
}

// GetMeta returns a copy of up-to-date card meta
func (d *Dashboard) GetMeta(cardID uint) *Meta {
	d.mmu.RLock()
	defer d.mmu.RUnlock()

	meta, ok := d.metas[cardID]
	if !ok {
		return nil
	}

	m := *meta
	return &m
}

func (d *Dashboard) withMeta(cardID uint, handler func(m *Meta) error) error {
	d.mmu.Lock()
	defer d.mmu.Unlock()

	if meta, ok := d.metas[cardID]; ok {
		return handler(meta)
	}

	return nil
}

// Call card
// 1/ get a global card lock
// 2/ make sure card's not busy
// 3/ make sure card's live
// 4/ create opentok token
// 5/ create call object
// 6/ create user token
func (d *Dashboard) Call(user *User, card *Card) (*Call, error) {
	d.log.WithFields(logrus.Fields{
		"user": user.ID,
		"card": card.ID,
	}).Infof("new call on card%d by user%d", card.ID, user.ID)

	lock := locker.NewMutex(fmt.Sprintf("call:card:%d", card.ID), time.Minute)
	if err := lock.Lock(); err != nil {
		switch {
		case err == ErrRedLockFailed:
			return nil, ErrDashboardCardBusy
		default:
			return nil, err
		}
	}
	defer lock.Unlock()

	meta := d.GetMeta(card.ID)
	if meta == nil {
		return nil, ErrDashboardCardNoResponse
	}

	if !meta.Live {
		return nil, ErrDashboardCardNoResponse
	}

	opentok, err := GetOpenTok()
	if err != nil {
		return nil, err
	}

	call := &Call{
		CardID:    card.ID,
		CallerID:  user.ID,
		CalleeID:  card.UserID,
		Rate:      card.Rate,
		SessionID: opentok.SessionID,
	}
	if err := call.Create(); err != nil {
		return nil, err
	}

	msg, _ := json.Marshal(&dashboardCallMessage{
		dashboardMessageHeader: dashboardMessageHeader{
			Token:  d.token,
			CardID: card.ID,
		},
		CallID: call.ID,
	})
	if err := d.redis.Publish(d.channels.calls, string(msg)).Err(); err != nil {
		return nil, err
	}

	return call, nil
}

// adds card to list of local metas prefetching relevant information (such as topic ids, rate, etc)
func (d *Dashboard) add(cardID uint) error {
	// d.log.WithField("card", cardID).Debugf("add card%d", cardID)

	d.mmu.RLock()
	if _, ok := d.metas[cardID]; ok {
		d.mmu.RUnlock()
		return nil
	}
	d.mmu.RUnlock()

	card, err := MustGetCard(cardID)
	if err != nil {
		return err
	}

	meta, err := card.ToMeta()
	if err != nil {
		return err
	}
	meta.Live = true

	views, err := d.redis.Get(fmt.Sprintf("radar:views:%v", cardID)).Int64()
	if err != nil && err != redis.Nil {
		return err
	}
	meta.Views = uint(views)

	d.mmu.Lock()
	d.metas[cardID] = meta
	d.mmu.Unlock()

	return nil
}

// removes card from the list of local metas
func (d *Dashboard) remove(cardID uint) {
	// d.log.WithField("card", cardID).Debugf("remove card%d", cardID)

	d.mmu.Lock()
	delete(d.metas, cardID)
	d.mmu.Unlock()
}

func (d *Dashboard) incViews(cardID uint, count uint) {
	// d.log.WithField("card", cardID).Debugf("incViews card%d +%d", cardID, count)

	d.withMeta(cardID, func(m *Meta) error {
		m.Views += count

		return nil
	})
}

func (d *Dashboard) setViews(cardID uint, value uint) {
	// d.log.WithField("card", cardID).Debugf("setViews card%d to %d", cardID, value)

	d.withMeta(cardID, func(m *Meta) error {
		m.Views = value

		return nil
	})
}

// notify local and remote observers that card has joined this dashboard
func (d *Dashboard) notifyJoin(cardID uint) error {
	// d.log.WithField("card", cardID).Debugf("notifyJoin card%d", cardID)

	msg, _ := json.Marshal(&dashboardMessageHeader{Token: d.token, CardID: cardID})
	if err := d.redis.Publish(d.channels.join, string(msg)).Err(); err != nil {
		return err
	}

	d.notifyObservers(cardID)

	return nil
}

// notify local and remote observers that card has left this dashboard
func (d *Dashboard) notifyLeave(cardID uint) error {
	// d.log.WithField("card", cardID).Debugf("notifyLeave card%d", cardID)

	msg, _ := json.Marshal(&dashboardMessageHeader{Token: d.token, CardID: cardID})
	if err := d.redis.Publish(d.channels.leave, string(msg)).Err(); err != nil {
		return err
	}

	d.notifyObservers(cardID)

	return nil
}

// viewsCount - relative value, not absolute
func (d *Dashboard) notifyUpdateViews(cardID uint, viewsCount uint, userID uint) error {
	// d.log.WithFields(logrus.Fields{
	// 	"card": cardID,
	// 	"user": userID,
	// }).Debugf("notifyUpdateViews card%d, views +%d by user%d", cardID, viewsCount, userID)

	msg, _ := json.Marshal(&dashboardUpdateViewsMessage{
		dashboardMessageHeader: dashboardMessageHeader{
			Token:  d.token,
			CardID: cardID,
		},
		Views:  viewsCount,
		UserID: userID,
	})

	if err := d.redis.Publish(d.channels.updateViews, string(msg)).Err(); err != nil {
		return err
	}

	// d.notifyObservers(cardID)

	return nil
}

// notifyCaller
func (d *Dashboard) notifyCaller(cardID, callerID uint) error {
	// d.log.WithField("card", cardID).Debugf("notifyCaller card%d, user%d", cardID, callerID)

	msg, _ := json.Marshal(&dashboardUpdateCallerMessage{
		dashboardMessageHeader: dashboardMessageHeader{
			Token:  d.token,
			CardID: cardID,
		},
		CallerID: callerID,
	})

	if err := d.redis.Publish(d.channels.updateCaller, string(msg)).Err(); err != nil {
		return err
	}

	return nil
}

// notifies local observers on card change
func (d *Dashboard) notifyObservers(cardID uint) {
	// d.log.WithField("card", cardID).Debugf("notifyObservers card%d", cardID)

	meta := d.GetMeta(cardID)
	if meta == nil {
		d.log.WithField("card", cardID).Warnf("notifyObservers card%d missing", cardID)
		return
	}

	d.omu.RLock()
	defer d.omu.RUnlock()

	for _, o := range d.observers[cardID] {
		go func(m chan Meta, data Meta, log *logrus.Entry) {
			select {
			case m <- data:
				// log.Debugf("notify card%d", cardID)
				// ok

			case <-time.After(time.Second):
				log.Warnf("notify card%d chan %v skipped", cardID, m)
			}
		}(o.M, *meta, o.log.WithField("card", cardID))
	}

	// logrus.Debugf("d %v notifyObservers card %d done (%d notified)", d.token, cardID, count)
}

func (d *Dashboard) localJoin(cardID uint) error {
	// d.log.WithField("card", cardID).Debugf("localJoin card%d", cardID)

	if err := d.add(cardID); err != nil {
		return err
	}

	return d.notifyJoin(cardID)
}

func (d *Dashboard) localLeave(cardID uint) error {
	// d.log.WithField("card", cardID).Debugf("localLeave card%d", cardID)

	d.withMeta(cardID, func(m *Meta) error {
		m.Live = false

		return nil
	})

	if err := d.notifyLeave(cardID); err != nil {
		return err
	}

	d.remove(cardID)

	return nil
}

// syncer removes locally dead items
func (d *Dashboard) syncer() {
	for {
		select {
		case <-d.stopSyncer:
			d.log.Info("syncer stopped")

			return
		case <-time.After(dashboardKeepAlive + time.Duration(rand.Intn(10))*time.Second):
			ids, err := d.list()
			if err != nil {
				panic(err)
			}
			// d.log.Debugf("syncer eviction list check %+v", ids)
			idm := map[uint]bool{}
			for _, id := range ids {
				idm[id] = true
			}
			evicted := []uint{}

			d.mmu.RLock()
			for cardID := range d.metas {
				if v, ok := idm[cardID]; !ok || !v {
					evicted = append(evicted, cardID)
				}
			}
			d.mmu.RUnlock()

			if len(evicted) > 0 {
				// d.log.Debugf("syncer evicted %+v", evicted)

				for _, id := range evicted {
					d.withMeta(id, func(m *Meta) error {
						m.Live = false

						return nil
					})

					d.notifyObservers(id)

					d.remove(id)
				}
			}
		}
	}
}

// listen on remote updates
func (d *Dashboard) listener() {
	for {
		msg, err := d.pubsub.ReceiveMessage()
		if err != nil {
			if atomicLoadBool(&d.stop) {
				// d.log.Debug("listener stopped")
				return
			}
			panic(err)
		}

		// d.log.Debugf("got message %+v", msg)

		switch msg.Channel {
		case d.channels.updateCaller:
			if err := d.handleUpdateCaller(msg.Payload); err != nil {
				panic(err)
			}

		case d.channels.updateViews:
			if err := d.handleUpdateViews(msg.Payload); err != nil {
				panic(err)
			}

		case d.channels.join:
			if err := d.handleJoin(msg.Payload); err != nil {
				panic(err)
			}

		case d.channels.leave:
			if err := d.handleLeave(msg.Payload); err != nil {
				panic(err)
			}

		case d.channels.calls:
			if err := d.handleCall(msg.Payload); err != nil {
				panic(err)
			}

		default:
			d.log.Warnf("listener error, message in unknown channel %v: %v", msg.Channel, msg.Payload)
		}
	}
}

func (d *Dashboard) handleCall(payload string) error {
	var msg dashboardCallMessage

	log := d.log.WithFields(logrus.Fields{
		"card": msg.CardID,
		"call": msg.CallID,
	})

	if err := json.Unmarshal([]byte(payload), &msg); err != nil {
		return err
	}

	// update CallerID with potential caller
	call, err := MustGetCall(msg.CallID)
	if err != nil {
		// log.Debugf("handleCall card%d invalid call%d", msg.CardID, msg.CallID)
		return err
	}

	d.withMeta(msg.CardID, func(m *Meta) error {
		m.CallerID = call.CallerID

		return nil
	})

	d.rmu.RLock()
	defer d.rmu.RUnlock()

	r, ok := d.radars[msg.CardID]
	if !ok {
		return nil
	}

	go func(c chan uint) {
		select {
		case c <- msg.CallID:
			log.Infof("handleCall card%d incoming call%d", msg.CardID, msg.CallID)

		case <-time.After(time.Second):
			log.Warnf("handleCall card%d: incoming call%d failed (busy)", msg.CardID, msg.CallID)
		}
	}(r.C)

	return nil
}

// update caller id on card, if exists.
// NOTE: if card does not exits - do not try to recreate it. we can still recieve updates
// on cards that were removed from dashboard/radar long ago.
func (d *Dashboard) handleUpdateCaller(payload string) (err error) {
	var msg dashboardUpdateCallerMessage

	if err = json.Unmarshal([]byte(payload), &msg); err != nil {
		return
	}

	// d.log.WithFields(logrus.Fields{
	// 	"card": msg.CardID,
	// 	"user": msg.CallerID,
	// }).Debugf("handleUpdateCaller card%d caller%d", msg.CardID, msg.CallerID)

	d.withMeta(msg.CardID, func(m *Meta) error {
		m.CallerID = msg.CallerID

		return nil
	})

	d.notifyObservers(msg.CardID)

	return
}

func (d *Dashboard) handleUpdateViews(payload string) (err error) {
	var msg dashboardUpdateViewsMessage

	if err = json.Unmarshal([]byte(payload), &msg); err != nil {
		return
	}

	// // ignore own messages
	// if msg.Token == d.token {
	// 	logrus.Debugf("d %v handleUpdateViews card%d: ignored (self)", d.token, msg.CardID)
	// 	return
	// }

	log := d.log.WithField("card", msg.CardID)

	// log.Debugf("handleUpdateViews card%d views +%v", msg.CardID, msg.Views)

	// make sure meta exists
	meta := d.GetMeta(msg.CardID)
	if meta == nil {
		if err = d.add(msg.CardID); err != nil {
			log.Warnf("handleUpdateViews card%d does not exist", msg.CardID)
			return
		}
	}

	d.incViews(msg.CardID, msg.Views)

	d.notifyObservers(msg.CardID)

	// find and notify radar
	if msg.UserID != 0 {
		log = log.WithField("user", msg.UserID)

		d.rmu.RLock()
		defer d.rmu.RUnlock()

		r, ok := d.radars[msg.CardID]
		if !ok {
			return nil
		}

		go func(v chan uint) {
			select {
			case <-time.After(time.Second):
				log.Warnf("handleUpdateViews card%d viewed by user%d failed to send, busy", msg.CardID, msg.UserID)

			case v <- msg.UserID:
				// log.Debugf("handleUpdateViews card%d viewed by user%d", msg.CardID, msg.UserID)
			}
		}(r.V)
	}

	return
}

func (d *Dashboard) handleJoin(payload string) (err error) {
	var msg dashboardMessageHeader

	if err = json.Unmarshal([]byte(payload), &msg); err != nil {
		return
	}

	log := d.log.WithField("card", msg.CardID)

	// ignore own messages
	if msg.Token == d.token {
		// log.Debugf("handleJoin card%d ignored (self)", msg.CardID)
		return
	}

	// log.Debugf("handleJoin card%d", msg.CardID)

	if err = d.add(msg.CardID); err != nil {
		log.Warnf("handleJoin card%d: add failed", msg.CardID)
		return
	}

	d.notifyObservers(msg.CardID)

	return
}

func (d *Dashboard) handleLeave(payload string) (err error) {
	var msg dashboardMessageHeader

	if err = json.Unmarshal([]byte(payload), &msg); err != nil {
		return
	}

	// log := d.log.WithField("card", msg.CardID)

	// ignore own messages
	if msg.Token == d.token {
		// log.Debugf("handleLeave card%d ignored (self)", msg.CardID)
		return
	}

	// log.Debugf("handleLeave card%d", msg.CardID)

	d.withMeta(msg.CardID, func(m *Meta) error {
		m.Live = false

		return nil
	})

	d.notifyObservers(msg.CardID)

	d.remove(msg.CardID)

	return
}
