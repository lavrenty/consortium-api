package main

import (
	"testing"
	"time"
)

func TestCallGet(t *testing.T) {
	call := testCreateCall(t, CardMinRate)

	c, err := GetCall(call.ID)
	if err != nil {
		t.Fatal(err)
	}

	if c == nil {
		t.Fatal(err)
	}
}

func TestCallHangup(t *testing.T) {
	call := testCreateCall(t, CardMinRate)

	if err := call.Hangup(time.Duration(CallTimeFree+600) * time.Second); err != nil {
		t.Fatal(err)
	}
}

func TestCallSetRating(t *testing.T) {
	call := testCreateCall(t, CardMinRate)

	if err := call.Hangup(time.Duration(CallTimeFree+600) * time.Second); err != nil {
		t.Fatal(err)
	}

	caller, err := call.Caller()
	if err != nil {
		t.Fatal(err)
	}

	callee, err := call.Caller()
	if err != nil {
		t.Fatal(err)
	}

	if err := call.SetRating(caller, 5); err != nil {
		t.Fatal(err)
	}
	if err := call.SetRating(caller, 0); err != nil {
		t.Fatal(err)
	}
	if err := call.SetRating(caller, 1); err != nil {
		t.Fatal(err)
	}

	if err := call.SetRating(callee, 5); err != nil {
		t.Fatal(err)
	}
	if err := call.SetRating(callee, 0); err != nil {
		t.Fatal(err)
	}
	if err := call.SetRating(callee, 1); err != nil {
		t.Fatal(err)
	}
}

func TestCallSetRatingShort(t *testing.T) {
	call := testCreateCall(t, CardMinRate)

	if err := call.Hangup(time.Duration(30) * time.Second); err != nil {
		t.Fatal(err)
	}

	caller, err := call.Caller()
	if err != nil {
		t.Fatal(err)
	}

	if err := call.SetRating(caller, 5); err != nil {
		t.Fatal(err)
	}
}

func TestCallSetRatingFree(t *testing.T) {
	call := testCreateCall(t, 0)

	if err := call.Hangup(time.Duration(CallTimeFree+600) * time.Second); err != nil {
		t.Fatal(err)
	}

	caller, err := call.Caller()
	if err != nil {
		t.Fatal(err)
	}

	if err := call.SetRating(caller, 5); err != nil {
		t.Fatal(err)
	}
}

func TestCallSetRatingFreeShort(t *testing.T) {
	call := testCreateCall(t, 0)

	if err := call.Hangup(time.Duration(30) * time.Second); err != nil {
		t.Fatal(err)
	}

	caller, err := call.Caller()
	if err != nil {
		t.Fatal(err)
	}

	if err := call.SetRating(caller, 5); err != nil {
		t.Fatal(err)
	}
}

func TestCallSetConnQuality(t *testing.T) {
	call := testCreateCall(t, CardMinRate)

	if err := call.Hangup(time.Duration(CallTimeFree+600) * time.Second); err != nil {
		t.Fatal(err)
	}

	caller, err := call.Caller()
	if err != nil {
		t.Fatal(err)
	}

	callee, err := call.Caller()
	if err != nil {
		t.Fatal(err)
	}

	if err := call.SetConnQuality(caller, 4); err != nil {
		t.Fatal(err)
	}
	if err := call.SetConnQuality(callee, 4); err != nil {
		t.Fatal(err)
	}
}

func TestCallEndorseCaller(t *testing.T) {
	call := testCreateCall(t, CardMinRate)

	if err := call.Hangup(time.Duration(CallTimeFree+600) * time.Second); err != nil {
		t.Fatal(err)
	}

	if err := call.EndorseCaller(true); err != nil {
		t.Fatal(err)
	}
}

func TestCallTopicsScoreRebuild(t *testing.T) {
	call := testCreateCall(t, CardMinRate)

	if err := call.Hangup(time.Duration(CallTimeFree+600) * time.Second); err != nil {
		t.Fatal(err)
	}

	if err := call.EndorseCaller(true); err != nil {
		t.Fatal(err)
	}

	call.SetRating(testGetUser(t, call.CalleeID), 5)

	if err := call.TopicsScoreRebuild(); err != nil {
		t.Fatal(err)
	}

	if err := call.EndorseCaller(false); err != nil {
		t.Fatal(err)
	}
	call.SetRating(testGetUser(t, call.CalleeID), 1)
	if err := call.TopicsScoreRebuild(); err != nil {
		t.Fatal(err)
	}
}
