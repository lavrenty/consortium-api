package main

import (
	"database/sql"

	"github.com/jmoiron/sqlx"
)

// GetEndorsementsBy ...
func (u *User) GetEndorsementsBy(by *User) ([]Endorsement, error) {
	if by == nil {
		panic("invalid argument")
	}
	res := []Endorsement{}
	err := db.Select(&res, "SELECT * FROM endorsements WHERE user_id = $1 AND endorser_id = $2", u.ID, by.ID)

	return res, err
}

// GetPendingEndorsement ...
func (u *User) GetPendingEndorsement() (*Endorsement, error) {
	res := &Endorsement{}
	err := db.Get(res, "SELECT * FROM endorsements WHERE endorser_id = $1 AND is_endorsed IS NULL AND (card_seen_at IS NULL OR (card_seen_at + interval '1 month') < now()) ORDER BY priority DESC, random() LIMIT 1", u.ID)
	switch {
	case err == nil:
		return res, nil

	case err == sql.ErrNoRows:
		return nil, nil

	default:
		return nil, err
	}
}

// Endorse user :endorsee by :u on topic :topic
func (u *User) Endorse(endorsed *User, topic *Topic, isEndorsed *bool) error {
	endorsement := Endorsement{UserID: endorsed.ID, TopicID: topic.ID, EndorserID: u.ID}
	if isEndorsed != nil {
		endorsement.IsEndorsed.Scan(*isEndorsed)
	}

	err := transaction(func(tx *sqlx.Tx) (err error) {
		// if isendorsed is nil, make sure we're not overwriting whatever is there
		if !endorsement.IsEndorsed.Valid {
			e := Endorsement{}
			err = tx.Get(&e, "SELECT * FROM endorsements WHERE user_id = $1 AND topic_id = $2 AND endorser_id = $3", endorsement.UserID, endorsement.TopicID, endorsement.EndorserID)
			switch {
			case err == nil:
				// endorsement is already there, must not change it with nil
				return
			case err == sql.ErrNoRows:
				// only if there is no result - continue
			default:
				return
			}
		}
		res, err := tx.NamedExec("UPDATE endorsements SET is_endorsed = :is_endorsed, updated_at = now() WHERE user_id = :user_id AND topic_id = :topic_id AND endorser_id = :endorser_id", endorsement)
		if err != nil {
			return
		}

		ra, err := res.RowsAffected()
		if err != nil {
			return
		}

		// nothing changed or affected, do insert
		if ra == 0 {
			if _, err = tx.NamedExec("INSERT INTO endorsements (user_id, topic_id, endorser_id, is_endorsed) VALUES (:user_id, :topic_id, :endorser_id, :is_endorsed)", endorsement); err != nil {
				return
			}
		}
		return
	})

	return err
}

// RebuildEndorsements ...
func (u *User) RebuildEndorsements() error {
	rows, err := db.NamedQuery("SELECT u.* FROM users AS u LEFT JOIN friends AS f ON u.id = f.friend_id WHERE f.user_id = :id", u)
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		friend := new(User)
		if err := rows.StructScan(&friend); err != nil {
			return err
		}

		topics, err := friend.Topics()
		if err != nil {
			return err
		}
		for _, topic := range topics {
			u.Endorse(friend, &topic, nil)
		}
	}
	if err := rows.Err(); err != nil {
		return err
	}

	return nil
}
