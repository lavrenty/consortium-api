package main

import (
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/Sirupsen/logrus"
	"github.com/gin-gonic/gin"
	"github.com/stripe/stripe-go"
	"github.com/stripe/stripe-go/account"
	"github.com/stripe/stripe-go/card"
)

// GET /v1/billing/stripe
func handleStripeAccount(c *gin.Context) {
	apiStripeSuccess(c)
}

// POST v1/billing/stripe - create new stripe account for user
func handleStripeAccountCreate(c *gin.Context) {
	user := c.MustGet("current").(*User)
	// logger := c.MustGet("logger").(*logrus.Entry)

	var args struct {
		Country string `binding:"required"`
	}
	if err := c.BindJSON(&args); err != nil {
		apiError(c, 400, "invalid arguments", err)
		return
	}

	a, err := user.GetStripeAccount()
	if err != nil {
		apiError(c, 500, "internal error", err)
		return
	}
	if a != nil {
		apiError(c, 400, "account already created", err)
		return
	}

	clientIP := strings.Split(c.ClientIP(), ":")[0]

	params := &stripe.AccountParams{
		Managed: true,
		Country: args.Country,
		TransferSchedule: &stripe.TransferScheduleParams{
			Delay:      7,
			Interval:   stripe.Week,
			WeekAnchor: "tuesday",
		},
		TOSAcceptance: &stripe.TOSAcceptanceParams{
			Date:      time.Now().Unix(),
			IP:        clientIP,
			UserAgent: c.Request.UserAgent(),
		},
	}
	params.AddMeta("user_id", fmt.Sprintf("%d", user.ID))
	params.AddMeta("remote_ip", clientIP)

	sacc, err := account.New(params)
	if err != nil {
		apiStripeError(c, err)
		return
	}

	// acc := &stripe.Account{}
	a = &StripeAccount{
		UserID:          user.ID,
		StripeAccountID: sacc.ID,
		Country:         sacc.Country,
		DefaultCurrency: sacc.DefaultCurrency,
		Secret:          sacc.Keys.Secret,
		Publish:         sacc.Keys.Publish,
	}
	if err := a.Save(); err != nil {
		apiErrorInternal(c, err)
		return
	}

	apiStripeSuccess(c)
}

// PUT v1/billing/stripe - update stripe account details, including verification information
func handleStripeAccountUpdate(c *gin.Context) {
	user := c.MustGet("current").(*User)

	a, err := user.GetStripeAccount()
	if err != nil {
		apiError(c, 500, "internal error", err)
		return
	}
	if a == nil {
		apiError(c, 404, "account not found", nil)
		return
	}

	sc := a.GetStripeClient()

	sacc, err := sc.Account.Get()
	if err != nil {
		apiErrorInternal(c, err)
		return
	}

	args := sacc.LegalEntity

	if err = c.BindJSON(args); err != nil {
		apiError(c, 400, "invalid arguments", err)
		return
	}

	params := &stripe.AccountParams{LegalEntity: args}

	_, err = sc.Account.Update(a.StripeAccountID, params)
	if err != nil {
		apiStripeError(c, err)
		return
	}

	apiStripeSuccess(c)
}

// DELETE v1/billing/stripe - delete user stripe account details
// TODO: not supported
func handleStripeAccountDelete(c *gin.Context) {
	user := c.MustGet("current").(*User)

	a, err := user.GetStripeAccount()
	if err != nil {
		apiError(c, 500, "internal error", err)
		return
	}
	if a == nil {
		apiError(c, 404, "account not found", nil)
		return
	}

	// if _, err := account.Del(a.StripeAccountID); err != nil {
	// 	apiError(c, 500, "internal error deleting account", err)
	// 	return
	// }
	//
	// if err := user.DeleteStripeAccount(); err != nil {
	// 	apiError(c, 500, "internal error deleting account", err)
	// 	return
	// }

	apiSuccess(c)
}

// POST v1/billing/stripe/bank - attach bank details into account
func handleStripeAccountBankCreate(c *gin.Context) {
	user := c.MustGet("current").(*User)

	sa, err := user.GetStripeAccount()
	if err != nil {
		apiError(c, 500, "internal error", err)
		return
	}
	if sa == nil {
		apiError(c, 404, "account not found", nil)
		return
	}

	var args struct {
		Token string `binding:"required"`
	}
	if err := c.BindJSON(&args); err != nil {
		apiError(c, 400, "invalid arguments", err)
		return
	}

	params := &stripe.BankAccountParams{
		Token: args.Token,
	}
	params.SetAccount(sa.StripeAccountID)
	params.AddMeta("user_id", fmt.Sprintf("%d", user.ID))
	params.AddMeta("remote_ip", strings.Split(c.ClientIP(), ":")[0])

	sc := sa.GetStripeClient()
	if _, err := sc.BankAccounts.New(params); err != nil {
		apiStripeError(c, err)
		return
	}

	apiStripeSuccess(c)
}

// POST v1/billing/stripe/card - attach card details into account
func handleStripeAccountCardCreate(c *gin.Context) {
	user := c.MustGet("current").(*User)

	sa, err := user.GetStripeAccount()
	if err != nil {
		apiErrorInternal(c, err)
		return
	}
	if sa == nil {
		apiError(c, 404, "account not found", nil)
		return
	}

	var args struct {
		Token string `binding:"required"`
	}
	if err := c.BindJSON(&args); err != nil {
		apiError(c, 400, "invalid arguments", err)
		return
	}

	params := &stripe.CardParams{
		Account: sa.StripeAccountID,
		Token:   args.Token,
		Default: true,
	}
	params.AddMeta("user_id", fmt.Sprintf("%d", user.ID))
	params.AddMeta("remote_ip", strings.Split(c.ClientIP(), ":")[0])

	if _, err := card.New(params); err != nil {
		apiStripeError(c, err)
		return
	}

	apiStripeSuccess(c)
}

func apiStripeError(c *gin.Context, err error) {
	logger := c.MustGet("logger").(*logrus.Entry)

	if stripeErr, ok := err.(*stripe.Error); ok {
		switch stripeErr.Type {
		case stripe.ErrorTypeInvalidRequest:
			c.JSON(http.StatusOK, gin.H{"success": false, "error": gin.H{"code": 400, "type": "stripe_error", "message": "error with stripe parameters"}, "stripe_error": stripeErr})
		default:
			logger.Debugf("Unhandled stripe error %s (%+v)", stripeErr.Type, stripeErr)
			apiErrorInternal(c, err)
		}
	} else {
		apiErrorInternal(c, err)
	}
}

func apiStripeSuccess(c *gin.Context) {
	user := c.MustGet("current").(*User)

	a, err := user.GetStripeAccount()
	if err != nil {
		apiError(c, 404, "not found", err)
		return
	}
	if a == nil {
		apiErrorWithType(c, ErrAPINotFound, "stripe account needs setup")
		return
	}

	params := &stripe.AccountParams{}
	params.Expand("external_accounts")

	sc := a.GetStripeClient()
	sacc, err := sc.Account.GetByID(a.StripeAccountID, params)
	if err != nil {
		apiErrorInternal(c, err)
		return
	}

	spec, err := sc.CountrySpec.Get(a.Country)
	if err != nil {
		apiErrorInternal(c, err)
		return
	}

	c.JSON(200, gin.H{"success": true, "account": a.ToJSON(), "countryspec": spec, "stripe": sacc})
}
