package main

import "time"

// SystemCardFacebook ...
type SystemCardFacebook struct {
	UserID uint
}

// NewSystemCardFacebook ...
func NewSystemCardFacebook(u *User) SystemCardFacebook {
	return SystemCardFacebook{UserID: u.ID}
}

// ToJSON ...
func (c SystemCardFacebook) ToJSON() JSON {
	user, err := MustGetUser(c.UserID)
	if err != nil {
		panic(err)
	}

	return JSON{"permissions": user.FbPermissions}
}

// Type ...
func (c SystemCardFacebook) Type() string {
	return "facebook"
}

// Close ...
// TODO: validate that data is not updated yet
func (c SystemCardFacebook) Close() error {
	user, err := MustGetUser(c.UserID)
	if err != nil {
		return err
	}
	user.FbCardCount++
	user.FbCardSeenAt.Scan(time.Now())

	_, err = db.NamedExec("UPDATE users SET fb_card_count = :fb_card_count, fb_card_seen_at = :fb_card_seen_at, updated_at = now() WHERE id = :id", user)
	return err
}
