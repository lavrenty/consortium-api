package main

import (
	"database/sql"
	"fmt"
	"strconv"
	"time"

	"github.com/jmoiron/sqlx"
)

// Card is DB reflection
type Card struct {
	ID     uint
	UserID uint

	Text     string
	Rate     uint
	Language string

	IsLive     bool
	ViewsCount uint
	CallsCount uint

	UpdatedAt time.Time
	CreatedAt time.Time
}

// ToJSON with optional meta
func (c Card) ToJSON(m *Meta) JSON {
	j := JSON{
		"id":       c.ID,
		"type":     "card",
		"user_id":  c.UserID,
		"text":     c.Text,
		"rate":     c.Rate,
		"currency": "usd",
		"language": c.Language,
		"created":  c.CreatedAt.Unix(),
	}

	if m != nil {
		j["views"] = m.Views
		j["topic_ids"] = m.TopicIds
		j["live"] = m.Live
		// j["busy"] = m.Busy
	}

	return j
}

// ToFullJSON ...
func (c *Card) ToFullJSON() (JSON, error) {
	var meta *Meta

	if dashboard != nil {
		// suck in real meta
		meta = dashboard.GetMeta(c.ID)
	}

	if meta == nil {
		topicIds, err := c.TopicIds()
		if err != nil {
			return nil, err
		}

		meta = &Meta{
			Views:    c.ViewsCount,
			TopicIds: topicIds,
		}
	}

	user, err := MustGetUser(c.UserID)
	if err != nil {
		return nil, err
	}
	users := JSON{strconv.Itoa(int(user.ID)): user.ToCondensedJSON()}

	_topics, err := c.Topics()
	if err != nil {
		return nil, err
	}

	topics := JSON{}
	for _, topic := range _topics {
		topics[strconv.Itoa(int(topic.ID))] = topic.ToJSON()
	}

	j := JSON{
		"card":   c.ToJSON(meta),
		"users":  users,
		"topics": topics,
	}

	return j, nil
}

// GetCard returns card by id which can be represented as string or int
func GetCard(ID interface{}) (*Card, error) {
	card := new(Card)

	if err := db.Get(card, "SELECT * FROM cards WHERE id = $1", ID); err != nil {
		switch {
		case err == sql.ErrNoRows:
			return nil, nil
		default:
			return nil, err
		}
	}
	return card, nil
}

// MustGetCard ...
func MustGetCard(ID interface{}) (*Card, error) {
	card, err := GetCard(ID)
	if err != nil {
		return nil, err
	}
	if card == nil {
		return nil, fmt.Errorf("card %#v not found", ID)
	}

	return card, nil
}

// GetCards fetches multiple users by their id and returns an array of results
func GetCards(ids []uint) ([]Card, error) {
	cards := []Card{}

	if len(ids) <= 0 {
		return cards, nil
	}

	query, args, err := sqlx.In("SELECT DISTINCT * FROM cards WHERE id IN (?)", ids)
	if err != nil {
		return cards, err
	}
	if err := db.Select(&cards, db.Rebind(query), args...); err != nil {
		switch {
		case err == sql.ErrNoRows:
			return cards, nil
		default:
			return cards, err
		}
	}
	return cards, nil
}

// Create card with specified arguments
// Before calling, make sure you've set user_id, text and rate fields
func (c *Card) Create() error {
	if c.UserID == 0 {
		return fmt.Errorf("Sorry, internal error, user id not set")
	}
	if len(c.Text) < CardMinTextLength {
		return fmt.Errorf("Sorry, text is too short")
	}
	if c.Rate > 0 && c.Rate < CardMinRate {
		return fmt.Errorf("Sorry, rates lower than %d are not allowed", CardMinRate)
	}

	// detect missing language
	if len(c.Language) == 0 {
		lang, err := languages.Detect(c.Text)
		if err != nil {
			return err
		}
		c.Language = lang
	}

	rows, err := db.NamedQuery("INSERT INTO cards (user_id, text, rate, language) VALUES (:user_id, :text, :rate, :language) RETURNING *", c)
	if err != nil {
		return err
	}

	defer rows.Close()

	for rows.Next() {
		if err := rows.StructScan(c); err != nil {
			return err
		}
	}
	return nil
}

// MarkAsLive toggles IsLive flag and sets it to true
func (c *Card) MarkAsLive() error {
	if _, err := db.Exec("UPDATE cards SET is_live = true, updated_at = now() WHERE id = $1", c.ID); err != nil {
		return err
	}

	c.IsLive = true
	return nil
}

// MarkAsDead toggles IsLive flag and sets it to false
func (c *Card) MarkAsDead() error {
	if _, err := db.Exec("UPDATE cards SET is_live = false, updated_at = now() WHERE id = $1", c.ID); err != nil {
		return err
	}

	c.IsLive = false
	return nil
}

// Join topic by specified card. This is DB reflection only
func (c *Card) Join(t Topic) error {
	if _, err := db.Exec("INSERT INTO cards_topics (card_id, topic_id) VALUES ($1, $2)", c.ID, t.ID); err != nil {
		return err
	}

	return nil
}

// Topics this card is added to
// TODO: enforce limit on max topics number
func (c *Card) Topics() ([]Topic, error) {
	topics := []Topic{}
	if err := db.Select(&topics, "SELECT t.* FROM topics AS t LEFT JOIN cards_topics AS c ON t.id = c.topic_id WHERE c.card_id = $1", c.ID); err != nil {
		return nil, err
	}

	return topics, nil
}

// TopicIds returns card topic ids
func (c *Card) TopicIds() ([]uint, error) {
	ids := []uint{}
	if err := db.Select(&ids, "SELECT topic_id FROM cards_topics WHERE card_id = $1", c.ID); err != nil {
		return nil, err
	}

	return ids, nil
}

// Save card
func (c *Card) Save() error {
	return save("cards", c)
}

// IncCallsCount ...
func (c *Card) IncCallsCount() error {
	return db.Get(&c.CallsCount, "UPDATE cards SET calls_count = calls_count + 1, updated_at = now() WHERE id = $1 RETURNING calls_count", c.ID)
}

// IncViewsCount ...
func (c *Card) IncViewsCount(count uint) (err error) {
	if count > 0 {
		err = db.Get(&c.ViewsCount, "UPDATE cards SET views_count = views_count + $1, updated_at = now() WHERE id = $2 RETURNING views_count", count, c.ID)
	}

	return
}

// Endorse :user on all topics in the card by :endorser
func (c *Card) Endorse(endorser *User, endorsee *User, endorsed bool) error {
	topics, err := c.Topics()
	if err != nil {
		return err
	}

	for _, topic := range topics {
		if err := endorser.Endorse(endorsee, &topic, &endorsed); err != nil {
			return err
		}
	}

	return nil
}
