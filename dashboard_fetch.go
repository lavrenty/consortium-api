package main

import "sort"

// ByRate Sort metas by rate
type ByRate []*Meta

func (r ByRate) Len() int {
	return len(r)
}

func (r ByRate) Swap(i, j int) {
	r[i], r[j] = r[j], r[i]
}

func (r ByRate) Less(i, j int) bool {
	return r[i].Rate > r[j].Rate
}

// FetchOptions ...
type FetchOptions struct {
	User   *User
	Ignore map[uint]bool
	Limit  int
	Topics []Topic
}

// Fetch searches for metas that most satisfy request
func (d *Dashboard) Fetch(opts *FetchOptions) ([]*Meta, error) {
	topics, err := opts.User.ActiveTopics()
	if err != nil {
		return nil, err
	}
	opts.Topics = topics
	metas := d.fetchMetas(opts)

	for _, m := range metas {
		// d.incViews(m.CardID, 1)
		if err := d.notifyUpdateViews(m.CardID, 1, opts.User.ID); err != nil {
			return nil, err
		}
	}

	return metas, nil
}

func (d *Dashboard) fetchMetas(opts *FetchOptions) []*Meta {
	metas := []*Meta{}
	count := 0

	// locked prebuild array (this copies the whole thing)
	d.mmu.RLock()
	sorted := []*Meta{}
	for _, meta := range d.metas {
		m := *meta
		sorted = append(sorted, &m)
	}
	d.mmu.RUnlock()

	sort.Sort(ByRate(sorted))

	// match by topic
	for _, m := range sorted {
		got := false

		// skip if ignored
		if _, ok := opts.Ignore[m.CardID]; ok {
			continue
		}

		// skip if language unknown
		if !opts.User.SpeaksLanguage(m.Language) {
			continue
		}

		for _, t := range opts.Topics {
			for _, tid := range m.TopicIds {
				if tid == t.ID {
					got = true
					break
				}
			}

			if got {
				break
			}
		}

		if got {
			m.Views++
			metas = append(metas, m)
			count++
		}

		if count >= opts.Limit {
			break
		}
	}

	// append up to limit (what's left)
	if count < opts.Limit {
		for _, m := range sorted {
			skip := false

			if _, ok := opts.Ignore[m.CardID]; ok {
				skip = true
			} else if opts.User.ID == m.UserID {
				// skip your own cards, always
				skip = true
			} else if !opts.User.SpeaksLanguage(m.Language) {
				skip = true
			} else if m.UserID == opts.User.ID {
				skip = true
			} else {
				for _, mm := range metas {
					if mm.CardID == m.CardID {
						skip = true
						break
					}
				}
			}

			if !skip {
				m.Views++
				metas = append(metas, m)
				count++
			}

			if count >= opts.Limit {
				break
			}
		}
	}

	return metas
}
