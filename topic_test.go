package main

import (
	"strings"
	"testing"
)

func TestGetTopic(t *testing.T) {
	topic, err := GetTopic(666666)
	if err != nil || topic != nil {
		t.Errorf("Error fetching unexisting topic (err %#v, topic %#v)", err, topic)
	}

	names := []string{"test1", "test2", "another test1", "another test2", "test2", "test3", "whatever"}
	topics := []*Topic{}

	for _, name := range names {
		topic, err := FindOrCreateTopicByName(name)
		if err != nil {
			t.Errorf("Failed, %#v", err)
		}

		topics = append(topics, topic)
	}

	for _, topic := range topics {
		same, err := GetTopic(topic.ID)
		if err != nil {
			t.Errorf("Failed, %#v", err)
		}

		if same.ID != topic.ID || same.Name != topic.Name {
			t.Errorf("Failed, %#v, %#v", same, topic)
		}
	}
}

func TestGetTopics(t *testing.T) {
	_, err := GetTopics([]uint{1, 2, 3, 4, 5, 6, 6666, 77777, 8888888})
	if err != nil {
		t.Fatal(err)
	}
}

func TestTopicsAutocomplete(t *testing.T) {
	q := "te"

	topics, err := TopicsAutocomplete(q, 50)
	if err != nil {
		t.Errorf("Error from topics autocomplete: %#v", err)
	}

	for _, topic := range topics {
		if !strings.HasPrefix(strings.ToLower(topic.Name), q) {
			t.Errorf("Invalid result")
		}
	}
}

func TestTopicRebuildScore(t *testing.T) {
	user := testCreateUser(t)

	names := []string{"Investing", "Movies", "Mathematics"}
	for _, name := range names {
		topic, err := FindOrCreateTopicByName(name)
		if err != nil {
			t.Fatal(err)
		}

		if err := user.Subscribe(topic, false); err != nil {
			t.Fatal(err)
		}

		if err := topic.RebuildScore(user); err != nil {
			t.Fatal(err)
		}
	}

}
