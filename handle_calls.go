package main

import (
	"net/http"
	"strings"

	"github.com/Sirupsen/logrus"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
)

// WSS /call/:call_id
func handleCallWebsocket(c *gin.Context) {
	user := c.MustGet("current").(*User)
	call := c.MustGet("call").(*Call)
	logger := c.MustGet("logger").(*logrus.Entry)

	if user.ID != call.CallerID && user.ID != call.CalleeID {
		apiError(c, 403, "permission denied", nil)
		return
	}

	var wsupgrader = websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
		CheckOrigin:     wsCheckOrigin,
	}

	ws, err := wsupgrader.Upgrade(c.Writer, c.Request, nil)
	if err != nil {
		logger.WithError(err).Error("Failed to set websocket upgrade")
		return
	}
	defer ws.Close()

	wsCall(&wsConn{ws, c.ClientIP(), logger}, user, call)
}

// POST /call/:call_id/rate
// JSON{
// 	rating: int,
// 	conn_quality: int,
// }
// TODO: forbid call rate changes after certain timeout (1-2 days)
func handleCallRate(c *gin.Context) {
	user := c.MustGet("current").(*User)
	call := c.MustGet("call").(*Call)

	// 1. must be one of the peers
	if !call.IsValidPeerID(user.ID) {
		apiError(c, 403, "permission denied", nil)
		return
	}

	// 2. call must not be terminated already
	// call MIGHT not be terminated, as one of clients might still be holding socket while other disconnected already
	// if !call.IsHangup {
	// 	apiError(c, 403, "permission denied: call is not terminated yet", nil)
	// 	return
	// }

	// neither value is required, because 0 means user not voted
	var args struct {
		Rating      uint `json:"rating"`
		ConnQuality uint `json:"conn_quality"`
	}

	if err := c.BindJSON(&args); err != nil {
		apiError(c, 400, "invalid arguments", err)
		return
	}

	// sanity check
	if args.Rating > 5 || args.ConnQuality > 5 || args.Rating < 0 || args.ConnQuality < 0 {
		apiError(c, 400, "invalid arguments", nil)
		return
	}

	// set rating & quality
	if err := call.SetRating(user, args.Rating); err != nil {
		apiError(c, 500, "internal error", err)
		return
	}
	if err := call.SetConnQuality(user, args.ConnQuality); err != nil {
		apiError(c, 500, "internal error", err)
		return
	}

	// If callee, then endorse caller
	if call.IsCalleeID(user.ID) {
		// endorse for topics
		endorse := false
		if args.Rating > 2 {
			endorse = true
		}

		if err := call.EndorseCaller(endorse); err != nil {
			apiError(c, 500, "internal error", err)
			return
		}
	}

	apiSuccess(c)
}

// GET /call/:call_id/report
func handleCallReport(c *gin.Context) {
	user := c.MustGet("current").(*User)
	call := c.MustGet("call").(*Call)

	peer, err := call.Other(user)
	if err != nil {
		c.HTML(http.StatusInternalServerError, "500.html", gin.H{"message": "Error fetching peer"})
		return
	}

	token, err := createToken(user, "call-report")
	if err != nil {
		c.HTML(http.StatusInternalServerError, "500.html", gin.H{"message": "Error creating token"})
		return
	}

	c.HTML(http.StatusOK, "call_report.html", gin.H{
		"url":   config.Origin,
		"token": token,
		"call":  call.ID,
		"name":  peer.CondensedName(),
	})
}

// POST /call/:call_id/report
// Report abuse / miscondunct for a call
func handleCallReportSubmit(c *gin.Context) {
	user := c.MustGet("current").(*User)
	call := c.MustGet("call").(*Call)

	// 1. must be one of the peers
	if !call.IsValidPeerID(user.ID) {
		apiError(c, 403, "permission denied", nil)
		return
	}

	var args struct {
		Type string `binding:"required"`
	}

	if err := c.BindJSON(&args); err != nil {
		apiError(c, 400, "invalid arguments", err)
		return
	}

	report := Report{
		UserID:     user.ID,
		ReportType: args.Type,
		ReportIP:   strings.Split(c.ClientIP(), ":")[0],
	}

	report.CallID.Scan(call.ID)
	report.Obj = "call"

	if err := report.Save(); err != nil {
		apiErrorInternal(c, err)
		return
	}

	apiSuccess(c)
}
