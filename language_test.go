package main

import "testing"

func TestLanguageDetect(t *testing.T) {
	code, err := languages.Detect("This is, obviously, english")
	if err != nil {
		t.Fatal(err)
	}
	if code != "en" {
		t.Fatalf("expected english, got %s", code)
	}
}
