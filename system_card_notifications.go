package main

// SystemCardNotifications ...
type SystemCardNotifications struct {
	UserID uint
}

// NewSystemCardNotifications ...
func NewSystemCardNotifications(user *User) SystemCardNotifications {
	return SystemCardNotifications{UserID: user.ID}
}

// Type ...
func (c SystemCardNotifications) Type() string {
	return "notifications"
}

// ToJSON ...
func (c SystemCardNotifications) ToJSON() JSON {
	return JSON{}
}

// Close ...
// TODO: move out sql into user.xxx method
func (c SystemCardNotifications) Close() error {
	if _, err := db.Exec("UPDATE users SET notifications_card_seen_at = now(), notifications_card_count = notifications_card_count + 1 WHERE id = $1", c.UserID); err != nil {
		return err
	}

	return nil
}
