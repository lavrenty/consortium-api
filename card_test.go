package main

import (
	"testing"
)

func TestMustGetCard(t *testing.T) {
	_, err := MustGetCard(13241234)
	if err == nil {
		t.Fatal("expected error")
	}
}

func TestCardEndorse(t *testing.T) {
	user := testGetUser(t, 1)
	card := testCreateCard(t, user, []string{"Technology", "Web Development"})

	other := testGetUser(t, 2)
	if err := card.Endorse(user, other, true); err != nil {
		t.Fatal(err)
	}
}
