package main

import "testing"

func TestRadar(t *testing.T) {
	user := testGetUser(t, 1)
	user2 := testGetUser(t, 2)
	card := testCreateCard(t, user, []string{"Bitcoin"})

	d, err := NewDashboard(R)
	if err != nil {
		t.Fatal(err)
	}

	t.Logf("radar join card %d\n", card.ID)
	r, err := d.NewRadar(card.ID)
	if err != nil {
		t.Fatal(err)
	}

	o, err := d.NewObserver()
	if err != nil {
		t.Fatal(err)
	}
	o.Add(card.ID)

	t.Logf("fetch metas %+v\n", d.metas)
	metas, err := d.Fetch(&FetchOptions{
		Ignore: make(map[uint]bool, 0),
		Limit:  5,
		User:   user2,
		Topics: []Topic{},
	})
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("got metas: %v\n", metas)

	meta := <-o.M
	t.Logf("got meta %v\n", meta)

	t.Logf("radar stop card %d\n", card.ID)
	if err := r.Stop(); err != nil {
		t.Fatal(err)
	}

	o.Stop()
}
