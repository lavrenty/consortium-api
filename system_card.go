package main

import "time"
import "math/rand"

// SystemCard ...
type SystemCard interface {
	ToJSON() JSON
	Type() string
	Close() error
}

// GetSystemCard ...
// 1 facebook connect + retries
// 2 notifications + retries
// 3 topic suggestions
//   from cards already answered
//   related topic to what user already has
// 4 endorsements
//   from cards if not rated afterwards
//   from friends who also use the service
// 5 .. ?
func GetSystemCard(userID uint) (SystemCard, error) {
	user, err := MustGetUser(userID)
	if err != nil {
		return nil, err
	}

	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	list := r.Perm(4)

	for _, i := range list {
		switch i {
		case 0:
			// 1 facebook connect + retries
			if !user.FbID.Valid {
				// do not show it too often, and not more than 3 times
				if (!user.FbCardSeenAt.Valid || user.FbCardSeenAt.Time.AddDate(0, 0, user.FbCardCount).Before(time.Now())) && user.FbCardCount < 3 {
					return NewSystemCardFacebook(user), nil
				}
			}

		case 1:
			// TODO: must be device specific notifications
			if !user.Notifications {
				show := false

				if !user.NotificationsCardSeenAt.Valid && user.CreatedAt.AddDate(0, 0, 1).Before(time.Now()) {
					// 1. card not seen yet, show it a few days after login
					show = true
				} else if user.NotificationsCardSeenAt.Valid && user.NotificationsCardCount < 3 && user.NotificationsCardSeenAt.Time.AddDate(0, 0, user.NotificationsCardCount*2).Before(time.Now()) {
					// 2. already shown, show again after incremental delay
					show = true
				}

				if show {
					return NewSystemCardNotifications(user), nil
				}
			}

		case 2:
			// 4 topic suggestions (all must be shadow topics)
			topic, err := user.GetSuggestedTopic()
			if err != nil {
				return nil, err
			}
			if topic != nil {
				return NewSystemCardTopic(user, topic), nil
			}

		case 3:
			// 5 endorsements
			//   from cards if not rated afterwards (ignored for now)
			//   from friends who also use the service
			endorsement, err := user.GetPendingEndorsement()
			if err != nil {
				return nil, err
			}
			if endorsement != nil {
				return NewSystemCardEndorsement(endorsement), nil
			}

		default:
			panic("should not be here")
		}
	}

	return nil, nil
}
