package main

import (
	"bytes"
	"errors"
	"fmt"
	"log"
	"reflect"
	"unicode"
	"unicode/utf8"

	"github.com/jmoiron/sqlx"
)

const (
	forInvalid uint = iota

	// this returns a comma-separated list with assigments: user_id = :user_id, :password = :password, ...
	forUpdate

	// this returns comma-separated list with prefixed values: :user_id, :password, :email ...
	forInsertValues

	// this returns comma-separated without any prefix: user_id, password, ...
	forInsert
	// this returns comma-separated without any prefix: user_id, password, ...
	forSelect
)

// transaction
func transaction(txFunc func(*sqlx.Tx) error) (err error) {
	tx, err := db.Beginx()
	if err != nil {
		return err
	}
	defer func() {
		if r := recover(); r != nil {
			if tx != nil {
				tx.Rollback()
			}
			panic(r)
		}
		if err != nil {
			tx.Rollback()
			return
		}
		err = tx.Commit()
	}()
	return txFunc(tx)
}

func updateFields(tableName string, s interface{}, include []string) error {
	t := reflect.ValueOf(s)
	if t.Kind() == reflect.Ptr {
		t = t.Elem()
	}

	if t.Kind() != reflect.Struct {
		log.Fatalf("updateFields: invalid type of interface, expected Struct, got %#v", s)
	}

	f := t.FieldByName("ID")
	if !f.IsValid() {
		log.Fatalf("updateFields: no pk key named `ID` found")
	}
	if f.Kind() != reflect.Uint {
		log.Fatalf("updateFields: invalid type of primary key (expected Uint got %v)", f.Kind())
	}

	pk := f.Uint()
	if pk == 0 {
		return errors.New("updateFields: primary key missing")
	}

	fields := dbFieldsFor(forUpdate, s, include)
	if _, err := db.NamedExec(fmt.Sprintf("UPDATE %s SET %s, updated_at = now() WHERE id = :id", tableName, fields), s); err != nil {
		return err
	}

	return nil
}

// save structure into corresponding database
func save(tableName string, s interface{}) error {
	t := reflect.ValueOf(s)
	if t.Kind() == reflect.Ptr {
		t = t.Elem()
	}

	if t.Kind() != reflect.Struct {
		log.Fatalf("save: invalid type of interface, expected Struct, got %#v", s)
	}

	f := t.FieldByName("ID")
	if !f.IsValid() {
		log.Fatalf("save: no pk key named `ID` found")
	}
	if f.Kind() != reflect.Uint {
		log.Fatalf("save: invalid type of primary key (expected Uint got %v)", f.Kind())
	}

	pk := f.Uint()

	return transaction(func(tx *sqlx.Tx) error {
		if pk != 0 {
			fields := dbFieldsFor(forUpdate, s, nil)
			if _, err := tx.NamedExec(fmt.Sprintf("UPDATE %s SET %s, updated_at = now() WHERE id = :id", tableName, fields), s); err != nil {
				return err
			}
		} else {
			// TODO: this wont work because ID field is present
			in := dbFieldsFor(forInsert, s, nil)
			inVals := dbFieldsFor(forInsertValues, s, nil)

			rows, err := tx.NamedQuery(fmt.Sprintf("INSERT INTO %s (%s) VALUES (%s) RETURNING *", tableName, in, inVals), s)
			if err != nil {
				return err
			}
			defer rows.Close()
			for rows.Next() {
				if err := rows.StructScan(s); err != nil {
					return err
				}
				break
			}
			if err := rows.Err(); err != nil {
				return err
			}
		}

		return nil
	})
}

// s must be a type struct or *struct with all required fields tagged with `db` tag
func dbFields(s interface{}) []string {
	result := []string{}

	t := reflect.TypeOf(s)
	if t.Kind() == reflect.Ptr {
		t = t.Elem()
	}

	if t.Kind() != reflect.Struct {
		log.Fatalf("dbFields: value not struct %#v", s)
	}
	for i := 0; i < t.NumField(); i++ {
		f := t.Field(i)

		// ignore lower-case fields (they are invisible)
		fs, _ := utf8.DecodeRuneInString(f.Name)
		if unicode.IsLower(fs) {
			continue
		}

		// ignore fields tagged with `db:"-"`
		name := f.Tag.Get("db")
		if name == "-" {
			continue
		}

		// if there is no name from tag, take it from field name
		if len(name) == 0 {
			name = dbUnderscore(f.Name)
		}
		result = append(result, name)
	}

	return result
}

// dbFieldsJoin automatically creates select/insert/update statement arguments from struct
// dbFieldsJoin(&User{}, forSelect, []string{})
// outputs:
//   "id, name, first_name, last_name, middle_name, ..."
// TODO: never update primary key (id)

func dbFieldsFor(forMethod uint, s interface{}, include []string) string {
	var out bytes.Buffer
	i := 0
	defaultSkip := true

	if include == nil {
		defaultSkip = false
	}

	for _, field := range dbFields(s) {
		if forMethod != forSelect && (field == "created_at" || field == "updated_at" || field == "id") {
			continue
		}
		skip := defaultSkip
		for _, x := range include {
			if x == field {
				skip = false
				break
			}
		}
		if skip {
			continue
		}

		switch forMethod {
		case forUpdate:
			// user_id = :user_id
			if i != 0 {
				out.WriteString(", ")
			}
			out.WriteString(field)
			out.WriteString(" = :")
			out.WriteString(field)

		case forInsertValues:
			// :user_id
			if i != 0 {
				out.WriteString(", ")
			}
			out.WriteByte(':')
			out.WriteString(field)

		case forSelect, forInsert:
			// user_id
			if i != 0 {
				out.WriteString(", ")
			}
			out.WriteString(field)
		default:
			log.Fatalf("invalid for method %d", forMethod)
		}
		i++
	}

	return out.String()
}

// ID -> id, UserID -> user_id, CreatedAt -> created_at, WhateverIsThisURL -> whatever_is_this_url
func dbUnderscore(in string) string {
	out := make([]byte, len(in)*2*utf8.UTFMax)
	n := 0
	wu := false
	for i, c := range in {
		iu := unicode.IsUpper(c)
		if i != 0 && iu && !wu {
			out[n] = '_'
			n++
		}
		n += utf8.EncodeRune(out[n:n+utf8.UTFMax], unicode.ToLower(c))
		wu = iu
	}

	return string(out[0:n])
}

// type ArrayString struct {
// 	Items []string
// }
//
// func (as *ArrayString) Append(item string) {
//
// }
//
// func (as ArrayString) String() string {
// 	return strings.Join(as.Items, ",")
// }
//
// func (as *ArrayString) Scan(items interface{}) (err error) {
// 	switch i := items.(type) {
// 	case string:
// 		as.Items = strings.Split(i, ",")
// 	case []string:
// 		as.Items = i
// 	default:
// 		as.Items = []string{}
// 		err = fmt.Errorf("cq: invalid Scan value for %T: %T", as, items)
// 	}
// 	return
// }
//
// func (as ArrayString) Value() (driver.Value, error) {
// 	return as.String(), nil
// }
