package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"os/signal"

	"github.com/Sirupsen/logrus"
	"github.com/nsqio/go-nsq"
)

// WorkerDefaultTopic ...
const (
	WorkerDefaultTopic                   = "workers"
	WorkerDefaultChannel                 = "main"
	taskUpdateOtherProfileFacebookString = "update_other_profile_fb" // uid attached
	taskScoreRebuildString               = "score_rebuild"
	taskCallTopicsScoreRebuildString     = "call_topics_score_rebuild"
)

// WorkerFunc ...
type WorkerFunc func(uint, JSON) error

// Worker ..
type Worker struct {
	inline   bool
	count    int
	config   *nsq.Config
	producer *nsq.Producer
	topic    string
	handlers map[string]WorkerFunc
}

var worker *Worker

// NewWorker ...
func NewWorker(topic string, count int) (*Worker, error) {
	wcfg := nsq.NewConfig()
	wcfg.MaxInFlight = 10
	wcfg.UserAgent = fmt.Sprintf("conspiracy/1.0 nsq/%s ns/%s", nsq.VERSION, config.NSQ.Namespace)

	w := &Worker{
		inline:   false,
		topic:    fmt.Sprintf("%s-%s", config.NSQ.Namespace, topic),
		handlers: make(map[string]WorkerFunc),
		count:    count,
		config:   wcfg,
	}

	p, err := nsq.NewProducer(config.NSQ.NsqdTCPAddress, w.config)
	if err != nil {
		return nil, err
	}
	w.producer = p
	return w, w.producer.Ping()
}

func workerRegisterHandlers(w *Worker) {
	w.Register(taskUpdateOtherProfileFacebookString, taskUpdateOtherProfileFacebook)
	w.Register(taskScoreRebuildString, taskScoreRebuild)
	w.Register(taskCallTopicsScoreRebuildString, taskCallTopicsScoreRebuild)
}

// TODO: should not require explicit external call, move to init() ?
func workerSetup() {
	w, err := NewWorker(WorkerDefaultTopic, 50)
	if err != nil {
		panic(err)
	}

	workerRegisterHandlers(w)

	worker = w
}

func workerInlineSetup() {
	w := &Worker{
		inline:   true,
		handlers: make(map[string]WorkerFunc),
	}

	workerRegisterHandlers(w)

	worker = w
}

// Register ...
func (w *Worker) Register(name string, handler WorkerFunc) {
	if _, ok := w.handlers[name]; ok {
		log.Panicf("task %s already present", name)
	}

	w.handlers[name] = handler
}

// PerformAsync ...
func (w *Worker) PerformAsync(name string, id uint, args JSON) error {
	if w == nil {
		panic("Dude, seriosly? Set up workers!")
	}

	if _, ok := w.handlers[name]; !ok {
		return fmt.Errorf("unknown task %s", name)
	}

	if w.inline {
		f := w.handlers[name]
		if err := f(id, args); err != nil {
			logrus.Debugf("%s(%d) error: %#v", name, id, err)
		}
		return nil
	}

	msg, err := json.Marshal(JSON{"name": name, "id": id, "args": args})
	if err != nil {
		return err
	}
	return w.producer.Publish(w.topic, msg)
}

// Work handles async requests and processes them (see task.go)
func (w *Worker) Work() {
	consumer, err := nsq.NewConsumer(w.topic, WorkerDefaultChannel, w.config)
	if err != nil {
		panic(err)
	}

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt, os.Kill)

	consumer.AddConcurrentHandlers(nsq.HandlerFunc(func(m *nsq.Message) error {
		var msg struct {
			Name string
			ID   uint
			Args JSON
		}
		if err := json.Unmarshal(m.Body, &msg); err != nil {
			return err
		}

		if h, ok := w.handlers[msg.Name]; ok {
			logrus.Debugf("worker #%s %s(%d, args %#v)", w.topic, msg.Name, msg.ID, msg.Args)
			if err := h(msg.ID, msg.Args); err != nil {
				sentry.CaptureError(err, map[string]string{
					"type":    "worker",
					"handler": msg.Name,
					"arg_id":  fmt.Sprintf("%v", msg.ID),
					"args":    string(m.Body),
				})
				return err
			}

			return nil
		}

		return fmt.Errorf("unsupported task name %s in #%s", msg.Name, w.topic)
	}), 50)

	if len(config.NSQ.LookupdHTTPAddresses) > 0 {
		consumer.ConnectToNSQLookupds(config.NSQ.LookupdHTTPAddresses)
	} else {
		consumer.ConnectToNSQD(config.NSQ.NsqdTCPAddress)
	}

	go func() {
		<-stop
		consumer.Stop()
	}()
	<-consumer.StopChan
	return
}
