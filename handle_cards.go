package main

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/Sirupsen/logrus"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
)

// GET /card/:id - fetch card data (used by notifications system)
func handleCard(c *gin.Context) {
	card := c.MustGet("card").(*Card)

	j, err := card.ToFullJSON()
	if err != nil {
		apiErrorInternal(c, err)
		return
	}

	j["success"] = true
	c.JSON(http.StatusOK, j)
}

// POST /cards/rates
// topics=[]ids
// language=
// TODO: validate topic_ids & language
func handleCardsRates(c *gin.Context) {
	user := c.MustGet("current").(*User)

	var args struct {
		TopicIds []uint `json:"topic_ids" binding:"required"`
		Language string `json:"language"`
		Text     string `json:"text"`
	}

	err := c.BindJSON(&args)
	if err != nil {
		apiError(c, 400, "error parsing arguments", err)
		return
	}

	lang := DefaultLanguage
	if len(args.Text) >= CardMinTextLength {
		// detect language if not specified
		lang, err = languages.Detect(args.Text)
		if err != nil {
			apiErrorWithType(c, ErrAPITextInvalid, "could not determine language")
			return
		}
	} else if args.Language != "" {
		if !languages.IsSupported(args.Language) {
			apiErrorWithType(c, ErrAPITextInvalid, "language not supported")
			return
		}

		lang = args.Language
	} else {
		lang = strings.Split(user.Languages, ",")[0]
	}

	if !user.SpeaksLanguage(lang) {
		lang = strings.Split(user.Languages, ",")[0]
	}

	// validate, just in case

	estimates, err := TopicsRates(args.TopicIds, args.Language)
	if err != nil {
		apiErrorInternal(c, err)
		return
	}

	rates := JSON{}
	for _, estimate := range estimates {
		rates[strconv.Itoa(int(estimate.Rate))] = estimate.ToJSON()
	}

	c.JSON(http.StatusOK, gin.H{"success": true, "rates": rates})
}

// POST /cards
// Create card from user arguments
// TODO: wrap in transaction
func handleCardsCreate(c *gin.Context) {
	user := c.MustGet("current").(*User)

	// parse arguments
	var cardMsg struct {
		TopicIds []uint `json:"topic_ids" binding:"required"`
		Text     string `json:"text" binding:"required"`
		Rate     uint   `json:"rate"`
		Language string `json:"language"`
	}

	if err := c.BindJSON(&cardMsg); err != nil {
		apiError(c, 400, "error parsing arguments", err)
		return
	}

	// validate args
	if len(cardMsg.Text) < CardMinTextLength {
		apiErrorWithType(c, ErrAPITextInvalid, "text is too short")
		return
	}
	if len(cardMsg.Text) > CardMaxTextLength {
		apiErrorWithType(c, ErrAPITextInvalid, "text is too long")
		return
	}
	if cardMsg.Rate > 0 && cardMsg.Rate < CardMinRate {
		apiErrorWithType(c, ErrAPIRateInvalid, "invalid rate")
		return
	}

	// preload topics
	topics, err := GetTopics(cardMsg.TopicIds)
	if err != nil {
		apiError(c, 500, "internal error fetching topics", err)
		return
	}
	if len(topics) <= 0 {
		apiErrorWithType(c, ErrAPITopicInvalid, "no valid topics specified")
		return
	}

	lang := cardMsg.Language
	if lang == "" {
		// detect language if not specified
		lang, err = languages.Detect(cardMsg.Text)
		if err != nil {
			apiErrorWithType(c, ErrAPITextInvalid, "could not determine language")
			return
		}
	} else if !languages.IsSupported(lang) {
		apiErrorWithType(c, ErrAPITextInvalid, "language not supported")
		return
	}

	if !user.SpeaksLanguage(lang) {
		apiErrorWithType(c, ErrAPITextInvalid, fmt.Sprintf("language '%s' is not in list of languages you speak. Please update your profile.", lang))
		return
	}

	// create card
	card := &Card{UserID: user.ID, Text: cardMsg.Text, Rate: cardMsg.Rate}
	card.Language = lang

	// check permissions
	if ok, reason := user.CanCreate(card); !ok {
		apiError(c, 403, "permission denied", fmt.Errorf("permission denied: %s", reason))
		return
	}

	if err := card.Create(); err != nil {
		apiError(c, 500, "internal error", err)
		return
	}

	// join topics
	for _, topic := range topics {
		if err := card.Join(topic); err != nil {
			apiError(c, 500, "internal error", err)
			return
		}
	}

	c.JSON(http.StatusOK, gin.H{"success": true, "card_id": card.ID})
}

// POST /card/:card/dial
// Creates a call session
// TODO: user must be able to dial only sessions he has access to
func handleCardDial(c *gin.Context) {
	user := c.MustGet("current").(*User)
	card := c.MustGet("card").(*Card)

	// check permissions
	if !user.CanAnswer(card) {
		apiError(c, 403, "permission denied", nil)
		return
	}

	if card.UserID == user.ID {
		apiError(c, 404, "oops, schizophrenia detected", nil)
		return
	}

	call, err := dashboard.Call(user, card)
	if err != nil {
		switch {
		case err == ErrDashboardCardBusy:
			apiErrorWithType(c, ErrAPILineBusy, "line busy")

		case err == ErrDashboardCardNoResponse:
			apiErrorWithType(c, ErrAPINoReply, "no reply")

		default:
			apiErrorInternal(c, err)
		}

		return
	}

	opentok := NewOpenTokFromString(call.SessionID)
	token := opentok.Token(user.ID, 10*time.Minute)

	c.JSON(http.StatusOK, gin.H{
		"success": true,
		"call":    call.ToJSON(),
		"session": gin.H{
			"sid":   opentok.SessionID,
			"token": token,
		},
	})
}

// WSS /card/:card/radar
func handleCardRadarWebsocket(c *gin.Context) {
	user := c.MustGet("current").(*User)
	card := c.MustGet("card").(*Card)
	logger := c.MustGet("logger").(*logrus.Entry)

	if card.UserID != user.ID {
		apiError(c, 404, "card not found or permissions denied", nil)
		return
	}

	var wsupgrader = websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
		CheckOrigin:     wsCheckOrigin,
	}

	ws, err := wsupgrader.Upgrade(c.Writer, c.Request, nil)
	if err != nil {
		logger.WithError(err).Error("Failed to set websocket upgrade")
		return
	}
	defer ws.Close()

	wsRadar(&wsConn{ws, c.ClientIP(), logger}, user, card)
}

// WSS /cards/dashboard
func handleCardsDashboardWebsocket(c *gin.Context) {
	user := c.MustGet("current").(*User)
	logger := c.MustGet("logger").(*logrus.Entry)

	var wsupgrader = websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
		CheckOrigin:     wsCheckOrigin,
	}

	ws, err := wsupgrader.Upgrade(c.Writer, c.Request, nil)
	if err != nil {
		logger.WithError(err).Error("Failed to set websocket upgrade")
		return
	}
	defer ws.Close()

	wsDashboard(&wsConn{ws, c.ClientIP(), logger}, user)
}

// GET /card/:card_id/report
func handleCardReport(c *gin.Context) {
	user := c.MustGet("current").(*User)
	card := c.MustGet("card").(*Card)

	peer, err := MustGetUser(card.UserID)
	if err != nil {
		c.HTML(http.StatusInternalServerError, "500.html", gin.H{"message": "Error fetching card owner"})
		return
	}

	token, err := createToken(user, "card-report")
	if err != nil {
		c.HTML(http.StatusInternalServerError, "500.html", gin.H{"message": "Error creating token"})
		return
	}

	c.HTML(http.StatusOK, "card_report.html", gin.H{
		"url":   config.Origin,
		"token": token,
		"card":  card.ID,
		"name":  peer.CondensedName(),
	})
}

// POST /card/:card_id/report
// Report abuse / miscondunct for a card
func handleCardReportSubmit(c *gin.Context) {
	user := c.MustGet("current").(*User)
	card := c.MustGet("card").(*Card)

	var args struct {
		Type string `binding:"required"`
	}

	if err := c.BindJSON(&args); err != nil {
		apiError(c, 400, "invalid arguments", err)
		return
	}

	report := Report{
		UserID:     user.ID,
		ReportType: args.Type,
		ReportIP:   strings.Split(c.ClientIP(), ":")[0],
	}

	report.CardID.Scan(card.ID)
	report.Obj = "card"

	if err := report.Save(); err != nil {
		apiErrorInternal(c, err)
		return
	}

	apiSuccess(c)
}
