package main

import (
	"database/sql"
	"time"

	"github.com/jmoiron/sqlx"
)

// CanNotify returns wether we can send notification to this user right now
func (u *User) CanNotify() bool {
	if !u.Notifications {
		return false
	}

	if u.NextNotificationAt.After(time.Now()) {
		return false
	}

	return true
}

// MarkAsNotified updates next notification at stamp
func (u *User) MarkAsNotified() error {
	switch u.NotificationsFrequency {
	case NotificationsAlways:
		u.NextNotificationAt = time.Now()

	case NotificationsHourly:
		u.NextNotificationAt = time.Now().Add(time.Hour).Truncate(time.Hour)

	case NotificationsWeekly:
		u.NextNotificationAt = time.Now().AddDate(0, 0, 7)

		// NotificationsDaily
	default:
		// daily is the default, notify at 7am
		u.NextNotificationAt = time.Now().AddDate(0, 0, 1).Truncate(24 * time.Hour).Add(7 * time.Hour)
	}

	return db.Get(&u.NextNotificationAt, "UPDATE users SET next_notification_at = $1 WHERE id = $2 RETURNING next_notification_at", u.NextNotificationAt, u.ID)
}

// EnableNotifications ...
func (u *User) EnableNotifications(token string) error {
	return transaction(func(tx *sqlx.Tx) error {
		device := &Device{}

		if err := tx.Get(device, "SELECT * FROM devices WHERE user_id = $1 AND token = $2 LIMIT 1", u.ID, token); err != nil {
			switch {
			case err == sql.ErrNoRows:
				if err = tx.Get(device, "INSERT INTO devices (user_id, token, is_enabled) VALUES ($1, $2, true) RETURNING *", u.ID, token); err != nil {
					return err
				}

			default:
				return err
			}
		}

		// enable in case if it was disabled
		if !device.IsEnabled {
			if _, err := tx.NamedExec("UPDATE devices SET is_enabled = true WHERE id = :id", device); err != nil {
				return err
			}
		}

		if _, err := tx.Exec("UPDATE users SET notifications = true WHERE id = $1", u.ID); err != nil {
			return err
		}

		return nil
	})
}

// DisableNotifications ...
// TODO: log if there are no upadtes done by this reuqest
func (u *User) DisableNotifications(token string) error {
	if _, err := db.Exec("UPDATE devices SET is_enabled = false WHERE user_id = $1 AND token = $2", u.ID, token); err != nil {
		return err
	}

	devicesCount := 0
	if err := db.Get(&devicesCount, "SELECT count(*) FROM devices WHERE user_id = $1 AND is_enabled = true", u.ID); err != nil {
		return err
	}

	// no devices left, mark users as someone who has zero devices enabled and not eligible for notifications
	if devicesCount == 0 {
		if _, err := db.Exec("UPDATE users SET notifications = false WHERE id = $1", u.ID); err != nil {
			return err
		}
	}

	return nil
}
