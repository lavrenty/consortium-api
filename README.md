# Consortium API

## API Docs

* [Models](docs/models.md)
* [HTTP endpoints](docs/http-endpoints.md)
* [Billing HTTP endpoints](docs/billing-http-endpoints.md)
* [Websocket endpoints](docs/websocket-endpoints.md)
* [Push Notifications](docs/push-notifications.md)
* [Command line options](docs/command-line.md)


## TODO

Please see issues

### Big stuff

- [ ] restructure code to meet golang best practices, e.g. move out commands
- [ ] get rid of global var madness
- [ ] metrics subsystem
- [ ] store user pictures on the server with aws s3
- [ ] gin graceful restarts, including ws* modules (rest api, ws api < special api)

### Smaller stuff

- [ ] add timezone support for notifications
- [ ] only try sending notifications for offline users (implement online/offline check w/redis)
- [ ] feature: buffer local dashboard "view" messages, send them out in batches
- [ ] bug: fix worker issues with re-scheduling tasks over nsq
- [ ] for the sake of our sanity, rename `caller` to consultant/merchant and `callee` to customer
- [ ] MustGet... should panic?
- [ ] obscene / spam filter for cards (emails, phone numbers, websites, obscene words, sex terms)
- [ ] expired user facebook token ?
- [ ] get rid of "select * ..." so that when we alter table in live database, processed don't crash with unknown fields

## API styleguide and assumptions

* HTTP Endpoints [https://api.notconspiracy.io/v1](https://api.notconspiracy.io/v1)
* [SOON] Websoket endpoints [https://rt.notconspiracy.io/v1](https://rt.notconspiracy.io/v1)
* HTTPS only
* JSON only (in & out), e.g. send `Content-Type: application/json`
* Success: 200 OK application/json in form `{"success": true, ...}`
* Errors: 200 OK application/json `{"success": false, "error": {"code": int, type: string, "message": string}}`
* Errors: non-200 OK replies MUST be considered temporary and use [Exponential backoff](https://en.wikipedia.org/wiki/Exponential_backoff) algorithm with 5 retries before failing
* Authorization with BEARER access token, e.g. http header: `Authorization: BEARER <your-access-token>`

#### Error codes

* 400 - client error, no need to retry (same data will lead to same result)
* 401 - auth error (drop access token, logout user)
* 403 - permission denied
* 404 - object not found
* 500 - internal error, retry (same data will lead to diff result)

`type` defaults to "error" meaning generic error (one which can be displayed with default alert). Other values are speicified with their request and their meaning depends on context.

#### Error types

See api.go for details

* `line_busy` - on dialing; when someone else started calling that person
* `no_reply` - on dialing; when other party did not respond to our call
* `text_invalid` - on card creation; text is invalid or was not able to detect language
* `rate_invalid` - on card creation; when rate is invalid; highlight rate rubber
* `topic_invalid` - on card creation one of the provided topics is invalid; react by highlighiting topic field
* `not_found` - used in billing/stripe when account is not yet set up
* `contact_support` - user is banned/disabled; react by displaying message stating that there is internal error and user needs to reach out to support

## Server

#### Deps

- Go1.7
- PostgreSQL >= 9.5
- Redis >= 3
- NSQd >= 0.3.x (http://nsq.io/)

#### Building

  (install bindata if required: go get -u github.com/jteeuwen/go-bindata/...)
  $ go generate -x
  go-bindata -o bindata.go languages.json

  $ go build
  $

To start:
  copy consortium.example.conf into consortium.conf and start server:

  $ ./consortium-api



#### Command line

Please see [Command line options](docs/command-line.md) document


#### Deploy

To deploy code to production server run following cmd:

```shell
$ ansible-playbook -i ansible/hosts ansible/production.yml
```

Same about staging:

```shell
$ ansible-playbook -i ansible/hosts ansible/staging.yml
```

[Supervisord](http://supervisord.org/) is used to control app processes (server, worker, pusher etc).
