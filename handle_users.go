package main

import (
	"net/http"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
)

// GET /user/:user/profile
// Return current user profile
func handleUserProfile(c *gin.Context) {
	user := c.MustGet("current").(*User)
	other := c.MustGet("user").(*User)

	if other.ID != user.ID {
		apiError(c, 404, "not found", nil)
		return
	}

	userJSON := user.ToFullJSON()
	topicIds := []uint{}
	topics, err := user.ActiveTopics()
	if err != nil {
		apiError(c, 500, "internal error fetching topics", err)
		return
	}
	topicsJSON := map[string]JSON{}
	for _, topic := range topics {
		topicIds = append(topicIds, topic.ID)
		topicsJSON[strconv.Itoa(int(topic.ID))] = topic.ToStatsJSON()
	}
	userJSON["topic_ids"] = topicIds

	c.JSON(http.StatusOK, gin.H{"success": true, "user": userJSON, "topics": topicsJSON})
}

// POST /user/:user/endorse/:topic
// Endorse user on certain topic
// TODO: validate that users are related in some way (friends, pending system endorsement, etc)
// TODO: check payload to update endorsement value for card
func handleUserEndorse(c *gin.Context) {
	user := c.MustGet("current").(*User)
	other := c.MustGet("user").(*User)
	topic := c.MustGet("topic").(*Topic)

	val := true
	if err := user.Endorse(other, topic, &val); err != nil {
		apiError(c, 500, "endorsement failed", err)
		return
	}

	apiSuccess(c)
}

// POST /user/:user/language/:lang
// Add `lang` to users language list
func handleUserLanguageUpdate(c *gin.Context) {
	user := c.MustGet("current").(*User)
	lang := c.MustGet("lang").(string)

	if err := user.AddLanguage(lang); err != nil {
		apiError(c, 500, "error updating lang", nil)
		return
	}

	c.JSON(200, gin.H{"success": true, "languages": strings.Split(user.Languages, ",")})
}

// POST /user/:user/language/:lang
// Remove `lang` from users language list
func handleUserLanguageDestroy(c *gin.Context) {
	user := c.MustGet("current").(*User)
	lang := c.MustGet("lang").(string)

	if err := user.DeleteLanguage(lang); err != nil {
		apiError(c, 500, "error updating lang", nil)
		return
	}

	c.JSON(200, gin.H{"success": true, "languages": strings.Split(user.Languages, ",")})
}

// POST /user/:user/languages
// Set user languages to specified list
//
// Params:
//   languages: [string, ...] // 2-letter codes, e.g. ["en","fr","es",...]
//
func handleUserLanguagesUpdate(c *gin.Context) {
	user := c.MustGet("current").(*User)

	var args struct {
		Languages []string `binding:"required"`
	}
	if err := c.BindJSON(&args); err != nil {
		apiError(c, 400, "error parsing arguments", err)
		return
	}

	if err := user.SetLanguages(args.Languages); err != nil {
		apiErrorInternal(c, err)
		return
	}

	c.JSON(200, gin.H{"success": true, "languages": strings.Split(user.Languages, ",")})
}

// DELETE /user/:user
// Deletes related user account (marks as deleted, stops all acitvity)
func handleUserDelete(c *gin.Context) {
	user := c.MustGet("current").(*User)

	if err := user.Disable("disabled by user", user); err != nil {
		apiErrorInternal(c, err)
		return
	}

	apiSuccess(c)
}
