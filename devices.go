package main

import (
	"database/sql"
	"fmt"
	"time"

	"github.com/lib/pq"
)

// Device ...
type Device struct {
	ID     uint
	UserID uint

	Token     string
	IsEnabled bool
	IsSandbox bool

	LastNotifiedAt pq.NullTime
	CreatedAt      time.Time
}

// GetDevice by ID
// Error will be nil if not found
func GetDevice(ID interface{}) (*Device, error) {
	d := new(Device)
	if err := db.Get(d, "SELECT * FROM devices WHERE id = $1", ID); err != nil {
		switch {
		case err == sql.ErrNoRows:
			return nil, nil
		default:
			return nil, err
		}
	}

	return d, nil
}

// MustGetDevice ...
func MustGetDevice(ID interface{}) (*Device, error) {
	d, err := GetDevice(ID)
	if err != nil {
		return nil, err
	}
	if d == nil {
		return nil, fmt.Errorf("device %#v not found", ID)
	}
	return d, nil
}

// Devices returns list of current user devices (enabled or not)
func (u *User) Devices() ([]Device, error) {
	devices := []Device{}

	if err := db.Select(&devices, "SELECT * FROM devices WHERE user_id = $1 AND is_enabled = true", u.ID); err != nil {
		return nil, err
	}

	return devices, nil
}

// CanNotify checks against per-device rate limtis
func (d *Device) CanNotify() bool {
	if !d.LastNotifiedAt.Valid {
		return true
	}

	return time.Now().After(d.LastNotifiedAt.Time.Add(time.Minute))
}

// MarkAsNotified ...
func (d *Device) MarkAsNotified() error {
	return db.Get(&d.LastNotifiedAt, "UPDATE devices SET last_notified_at = now() WHERE id = $1 RETURNING last_notified_at", d.ID)
}
