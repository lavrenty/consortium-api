package main

// DISABLED
//
// import (
// 	"net/http"
//
// 	"github.com/gin-gonic/gin"
// )
//
// func handleAccountKitConnect(c *gin.Context) {
// 	var msg struct {
// 		AuthorizationCode string   `json:"authorization_code" binding:"required"`
// 		Locale            string   `json:"locale" binding:"required"`
// 		Languages         []string `json:"languages" binding:"required"`
// 	}
//
// 	if err := c.BindJSON(&msg); err != nil {
// 		apiError(c, 400, "invalid arguments", err)
// 		return
// 	}
//
// 	k, err := NewAccountKitWithCode(msg.AuthorizationCode)
// 	if err != nil {
// 		apiErrorInternal(c, err)
// 		return
// 	}
//
// 	user, err := FindOrCreateUserFromAccountKit(k.AccountID)
// 	if err != nil {
// 		apiErrorInternal(c, err)
// 		return
// 	}
//
// 	user.AccountKitAccessToken.Scan(k.AccessToken)
// 	if err := save("users", user); err != nil {
// 		apiErrorInternal(c, err)
// 		return
// 	}
//
// 	// process languages
// 	if err := user.SetLanguages(msg.Languages); err != nil {
// 		apiErrorInternal(c, err)
// 		return
// 	}
//
// 	// issue access token
// 	accessToken, err := user.AccessToken("ak")
// 	if err != nil {
// 		apiErrorInternal(c, err)
// 		return
// 	}
//
// 	c.JSON(http.StatusOK, gin.H{"success": true, "access_token": accessToken, "user": user.ToFullJSON()})
// }
