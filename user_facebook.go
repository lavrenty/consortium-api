package main

import (
	"database/sql"
	"fmt"
	"math/rand"
)

// FacebookDisconnect detaches user from his facebook session
// func (u *User) FacebookDisconnect() error {
// 	fields := []string{"fb_id", "fb_access_token", "fb_card_count", "fb_card_seen_at", "fb_permissions"}
// 	u.FbID.Scan(nil)
// 	u.FbAccessToken.Scan(nil)
// 	u.FbCardCount = 0
// 	u.FbCardSeenAt.Scan(nil)
// 	u.FbPermissions = DefaultFbPermissions
//
// 	query := fmt.Sprintf("UPDATE users SET %s, updated_at = now() WHERE id = :id", dbFieldsFor(forUpdate, u, fields))
//
// 	if _, err := db.NamedExec(query, u); err != nil {
// 		return err
// 	}
//
// 	return nil
// }

// GetUserByFbID tries to find user with specified facebook id
func GetUserByFbID(fbid string) (*User, error) {
	user := new(User)
	if err := db.Get(user, "SELECT * FROM users WHERE fb_id = $1", fbid); err != nil {
		switch {
		case err == sql.ErrNoRows:
			return nil, nil
		default:
			return nil, err
		}
	}

	return user, nil
}

// FakeFacebookVerification for simulator mainly
func (u *User) FakeFacebookVerification() error {
	u.FbID.Scan(fmt.Sprintf("%15d", rand.Int63()))
	u.FbAccessToken.Scan(fmt.Sprintf("%16x%16x%16x%16x", rand.Int63(), rand.Int63(), rand.Int63(), rand.Int63()))

	return save("users", u)
}
