package main

import (
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strconv"

	"github.com/Sirupsen/logrus"
	"github.com/gorilla/websocket"
)

func clientConnect(userID uint, path string) *wsConn {
	user, err := GetUser(userID)
	if err != nil || user == nil {
		log.Fatal(err, user)
	}

	t, err := createToken(user, "client")
	if err != nil {
		log.Fatal(err)
	}

	u := url.URL{Scheme: "ws", Host: config.Listen, Path: path}
	h := http.Header{}
	h.Add("Authorization", fmt.Sprintf("BEARER %s", t))

	ws, _, err := websocket.DefaultDialer.Dial(u.String(), h)
	if err != nil {
		log.Fatal(err)
	}

	return &wsConn{Conn: ws}
}

func clientRadar(cardID int) {
	card, err := GetCard(cardID)
	if err != nil || card == nil {
		log.Fatal(err)
	}

	ws := clientConnect(card.UserID, fmt.Sprintf("/v1/card/%d/radar", card.ID))
	defer ws.Close()

	client := wsClient(ws, false)

	// dead client emulation
	// ws.SetPingHandler(func(_ string) error {
	// 	return nil
	// })
loop:

	for {
		select {
		// recieve client packets
		case msg, ok := <-client:
			if !ok {
				logrus.Debugf("r card%d client closed", cardID)
				break loop
			}
			switch msg.Method {
			case "error":
				message, ok := msg.Raw["message"].(string)
				if !ok {
					message = string(msg.Bytes)
				}
				logrus.Debugf("r card%d error %s", cardID, message)

			case "view":
				logrus.Debugf("r card%d view %v", cardID, msg.Raw)

			case "update":
				logrus.Debugf("r card%d update %v", cardID, msg.Raw)

			case "call":
				logrus.Debugf("r card%d call %v", cardID, msg.Raw)
				break loop

			case "failure":
				logrus.Debugf("r card%d failure %v", cardID, msg.Raw)
				break loop

			default:
				logrus.Debugf("r card%d unknown command `%s` (%s)", cardID, msg.Method, string(msg.Bytes))
			}
		}
	}
	logrus.Debugf("r card%d off", cardID)
}

func clientDashboard(userID int) {
	ws := clientConnect(uint(userID), "/v1/cards/dashboard")
	defer ws.Close()

	console := wsConsole()

	// recieve client commands
	client := wsClient(ws, false)

loop:
	for {
		select {
		case msg, ok := <-console:
			if !ok {
				break loop
			}

			switch msg[0] {
			case "debug":
				if err := ws.WriteJSON(JSON{"method": "debug"}); err != nil {
					logrus.Debugf("d user%d cmd > debug failed: %v", userID, err)
					break loop
				}

			case "close":
				if len(msg) == 2 {
					if msg[1] == "system" {
						ws.WriteJSON(JSON{"method": "close", "system_id": 1})
					} else {
						cardID, _ := strconv.Atoi(msg[1])
						ws.WriteJSON(JSON{"method": "close", "card_id": cardID})
					}
				} else {
					logrus.Debugf("usage: close <card-id>")
				}

			default:
				logrus.Debugf("Unknown command. Supported commands are: close system|<card-id>/debug")
			}

		// recieve client packets
		case msg, ok := <-client:
			if !ok {
				logrus.Debugf("d user%d client closed", userID)
				break loop
			}
			switch msg.Method {
			case "debug":
				logrus.Debugf("d user%d debug %s", userID, string(msg.Bytes))

			case "system":
				logrus.Debugf("d user%d system %v", userID, msg.Raw)

			case "error":
				message, ok := msg.Raw["message"].(string)
				if !ok {
					message = string(msg.Bytes)
				}
				logrus.Debugf("d user%d error %s", userID, message)
			case "close":
				logrus.Debugf("d user%d close %d", userID, int(msg.Raw["card_id"].(float64)))
			case "show":
				logrus.Debugf("d user%d show %v", userID, msg.Raw)
			case "update":
				logrus.Debugf("d user%d update %v", userID, msg.Raw)
			default:
				logrus.Debugf("d user%d unknown command `%s` (%s)", userID, msg.Method, string(msg.Bytes))
			}
		}
	}
	logrus.Debugf("d user%d off", userID)
}

func clientDial(cardID int, userID int) {
	user, err := MustGetUser(userID)
	if err != nil {
		panic(err)
	}

	body := clientSimHTTP(user, fmt.Sprintf("card/%d/dial", cardID), JSON{})

	logrus.Debugf("%s", body)
}

func clientCall(callID int, userID int) {
	ws := clientConnect(uint(userID), fmt.Sprintf("/v1/call/%d", callID))
	defer ws.Close()

	// recieve client commands
	client := wsClient(ws, false)

	// read console
	console := wsConsole()

loop:
	for {
		select {
		case msg, ok := <-client:
			if !ok {
				logrus.Debugf("c call ws close %d %d", callID, userID)
				break loop
			}
			logrus.Debugf("< %s", string(msg.Bytes))

		case msg, ok := <-console:
			if !ok {
				break loop
			}

			switch msg[0] {
			case "live":
				ws.WriteJSON(JSON{"method": "live"})
			case "dead":
				ws.WriteJSON(JSON{"method": "dead"})
			case "hangup":
				ws.WriteJSON(JSON{"method": "hangup"})
			default:
				logrus.Debugf("[-] allowed commands are: live/dead/hangup")
			}
		}
	}
}
