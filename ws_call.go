package main

import (
	"expvar"
	"fmt"
	"time"
)

var (
	cClients = expvar.NewInt("cClients")
)

// TODO: validate client connection by checking opentok token?
func wsCall(ws *wsConn, user *User, call *Call) {
	// callee is basically the guy who's paying for the call
	// isCallee := user.ID == call.CalleeID

	// 0. if call has ended, return the hangup data and bail out
	if call.IsHangup {
		d := time.Duration(call.Duration) * time.Second
		c := call.CostFor(user.ID)

		ws.Logger().Debugf("c call%d(user%d) reconnect live:%t hangup:%t duration:%s cost:$%.02f done", call.ID, user.ID, false, true, d.String(), float64(c)/100)
		ws.WriteJSON(JSON{
			"method":   "update",
			"rate":     call.Rate,
			"cost":     c,
			"duration": call.Duration,
			"live":     false,
			"hangup":   true,
		})

		return
	}

	cClients.Add(1)
	defer cClients.Add(-1)

	// subscribe to call data updates
	p, updates := GetCallState(call, user)
	if p == nil {
		wsError(ws, "line busy", nil)
		return
	}
	defer p.Close(user)

	// recv client messages
	client := wsClient(ws, true)

	// recv ping timeouts
	timeout := wsPinger(ws)

	// ticker
	ticker := time.NewTicker(time.Second)
	defer ticker.Stop()

loop:
	for {
		select {
		case _, ok := <-updates:
			if !ok {
				ws.Logger().Debugf("c call%d(user%d) close (updates)", call.ID, user.ID)
				break loop
			}
			j := p.ToJSON(user)
			j["method"] = "update"
			ws.WriteJSON(j)

			// logrus.Debugf("c call%d(user%d) live:%t hangup:%t duration:%s cost:$%.02f", call.ID, user.ID, p.isLive(), p.isHangup(), p.GetDuration().String(), float64(p.GetCost(user))/100)

			if p.IsHangup() {
				break loop
			}

		// client messages
		case msg, ok := <-client:
			if !ok {
				ws.Logger().Debugf("c call%d(user%d) close (client)", call.ID, user.ID)
				break loop
			}
			switch msg.Method {
			case "live":
				ws.Logger().Debugf("c call%d(user%d) cmd < live", call.ID, user.ID)

				p.Live(user)

			case "dead":
				ws.Logger().Debugf("c call%d(user%d) cmd < dead", call.ID, user.ID)

				p.Dead(user)

			case "hangup":
				ws.Logger().Debugf("c call%d(user%d) cmd < hangup", call.ID, user.ID)

				p.Hangup(user)

				// should not get here
			default:
				wsError(ws, fmt.Sprintf("unknown command `%s`", msg.Method), nil)
			}

			// send call progress
		case <-ticker.C:
			j := p.ToJSON(user)
			j["method"] = "update"
			ws.WriteJSON(j)

			if p.IsHangup() {
				break loop
			}

			// client ping/pong timeout
		case <-timeout:
			break loop
		}
	}

	ws.Logger().Debugf("c call%d(user%d) live:%t hangup:%t duration:%s cost:$%.02f done", call.ID, user.ID, p.IsLive(), p.IsHangup(), p.GetDuration().String(), float64(p.GetCost(user))/100)
}
