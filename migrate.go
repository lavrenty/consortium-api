package main

import (
	"fmt"
	"io/ioutil"
	"path/filepath"
	"sort"
	"strconv"
	"strings"

	"github.com/Sirupsen/logrus"
)

type migration struct {
	Version int
	File    string
}

type upDirection []migration
type downDirection []migration

func (a upDirection) Len() int {
	return len(a)
}

func (a upDirection) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}

func (a upDirection) Less(i, j int) bool {
	return a[i].Version < a[j].Version
}

func (a downDirection) Len() int {
	return len(a)
}

func (a downDirection) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}

func (a downDirection) Less(i, j int) bool {
	return a[i].Version > a[j].Version
}

// Migrate ...
func Migrate(direction string) {
	logger := logrus.WithField("direction", direction)
	_, err := db.Exec(`
CREATE TABLE IF NOT EXISTS schema_migrations (
	version integer NOT NULL
)
`)
	if err != nil {
		logger.WithError(err).Fatal("Unable to create schema_migrations table")
	}

	files, err := filepath.Glob(fmt.Sprintf("db/migrations/**.%s.sql", direction))
	if err != nil {
		logger.WithError(err).Fatal("Unable to find migration files")
	}

	lastMigration := struct{ Version int }{0}
	db.Get(&lastMigration, "SELECT * FROM schema_migrations ORDER BY version DESC LIMIT 1")

	var migrations []migration
	for _, file := range files {
		ver, err := strconv.Atoi(strings.Split(filepath.Base(file), "_")[0])
		if err != nil {
			logger.WithError(err).Warn("Unable to parse version number")
			continue
		}

		if direction == "up" && ver > lastMigration.Version || direction == "down" && ver <= lastMigration.Version {
			migrations = append(migrations, migration{ver, file})
		}
	}

	if len(migrations) == 0 {
		logger.Info("No migrations to apply")
		return
	}

	switch direction {
	case "up":
		sort.Sort(upDirection(migrations))
	case "down":
		sort.Sort(downDirection(migrations))
	}

	for _, migration := range migrations {
		mLogger := logger.WithField("file", migration.File)
		data, err := ioutil.ReadFile(migration.File)
		if err != nil {
			mLogger.WithError(err).Fatal("Unable to read migration file")
		}

		mLogger.Info("Applying migration...")

		tx, err := db.Begin()
		if err != nil {
			mLogger.WithError(err).Fatal("Unable to begin transaction")
		}

		_, err = tx.Exec(string(data))
		if err != nil {
			tx.Rollback()
			mLogger.WithError(err).Fatal("Unable to apply migration")
		}

		var q string
		switch direction {
		case "up":
			q = "INSERT INTO schema_migrations VALUES ($1)"
		case "down":
			q = "DELETE FROM schema_migrations WHERE version = $1"
		}

		_, err = db.Exec(q, migration.Version)
		if err != nil {
			tx.Rollback()
			mLogger.WithError(err).Fatal("Unable to update schema_migrations table")
		}

		err = tx.Commit()
		if err != nil {
			mLogger.WithError(err).Fatal("Unable to commit transaction")
		}

		mLogger.Info("Done!")

		// go down one at a time
		if direction == "down" {
			break
		}
	}
}
