package main

import "testing"

func TestGetCost(t *testing.T) {
	cost := GetCost(CallTimeFree-1, 0, true)
	if cost.CallerCost != 0 || cost.CalleeCost != 0 || cost.ApplicationFee != 0 {
		t.Fatalf("bad cost for free short call: %+v", cost)
	}

	cost = GetCost(CallTimeFree*10, 0, true)
	if cost.CallerCost != 0 || cost.CalleeCost != 0 || cost.ApplicationFee != 0 {
		t.Fatalf("bad cost for free long call: %+v", cost)
	}

	cost = GetCost(CallTimeFree-1, 200, true)
	if cost.CallerCost != 0 || cost.CalleeCost != 0 || cost.ApplicationFee != 0 {
		t.Fatalf("bad cost for paid short call: %+v", cost)
	}

	cost = GetCost(122, 150, true)
	if cost.CallerCost != 275 || cost.CalleeCost != 405 || cost.ApplicationFee != 130 {
		t.Fatalf("bad cost for paid long call: %+v", cost)
	}
	t.Log(cost.CalleeCost, cost.CallerCost, cost.ApplicationFee)
}

// test in-call rate
func TestGetCostPartial(t *testing.T) {
	cost := GetCost(CallTimeFree*10, 20, false)

	if cost.CalleeCost == 0 || cost.CallerCost == 0 || cost.ApplicationFee != 0 {
		t.Fatalf("bad cost %+v", cost)
	}
}
