package main

import (
	"database/sql/driver"
	"errors"
	"time"
)

type jsonTime time.Time

func (t jsonTime) format() string {
	return time.Time(t).Format(time.RFC3339)
}

func (t jsonTime) MarshalText() ([]byte, error) {
	return []byte(t.format()), nil
}

func (t jsonTime) MarshalJSON() ([]byte, error) {
	return []byte(`"` + t.format() + `"`), nil
}

func (t *jsonTime) UnmarshalJSON(data []byte) error {
	result, err := time.Parse(`"`+time.RFC3339+`"`, string(data))
	if err != nil {
		return err
	}
	*t = jsonTime(result)
	return nil
}

func (t jsonTime) Value() (driver.Value, error) {
	return t.format(), nil
}

func (t *jsonTime) Scan(src interface{}) error {
	switch val := src.(type) {
	case time.Time:
		*t = jsonTime(val)
	default:
		return errors.New("Incompatible type for jsonTime")
	}

	return nil
}
