package main

import (
	"testing"

	"github.com/stripe/stripe-go"
)

func TestUserAddCard(t *testing.T) {
	user := testCreateUser(t)

	card := &stripe.CardParams{
		Number: "4242424242424242",
		CVC:    "234",
		Month:  "10",
		Year:   "2020",
		Name:   "Test User",
	}
	if err := user.AddCard(card); err != nil {
		t.Fatal(err)
	}
}

func TestUserDelCard(t *testing.T) {
	user := testCreateUser(t)

	if err := user.DelCard(); err != nil {
		t.Fatal(err)
	}
}
