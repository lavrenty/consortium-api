package main

import (
	"database/sql"
	"time"
)

// Score represents scoring system
type Score struct {
	initialized bool

	ID     uint
	UserID uint

	FbVerified     sql.NullBool
	FbIsVerified   sql.NullBool
	FbFriendsCount sql.NullInt64

	// sum(rating - 3) where rating > 0   +   count(*) where rating = 0;
	CallerScore int
	CalleeScore int

	UpdatedAt time.Time
	CreatedAt time.Time
}

// GetScore returns score object for specified user
func GetScore(userID uint) (*Score, error) {
	score := &Score{initialized: true, UserID: userID}

	if err := db.Get(score, "SELECT * FROM scores WHERE user_id = $1", userID); err != nil {
		switch {
		case err == sql.ErrNoRows:
			return score, nil
		default:
			return nil, err
		}
	}

	return score, nil
}

// Rebuild current score, better run async!
func (s *Score) Rebuild() error {
	if !s.initialized {
		panic("score was not properly intialized")
	}

	// Caller stats
	ratingRated := 0
	ratingUnrated := 0
	if err := db.Get(&ratingRated, "SELECT coalesce(sum(caller_rating - 3), 0) FROM calls WHERE is_hangup = true AND caller_rating > 0 AND caller_id = $1 AND duration > $2", s.UserID, CallTimeFree); err != nil {
		return err
	}
	if err := db.Get(&ratingUnrated, "SELECT count(*) FROM calls WHERE is_hangup = true AND caller_rating = 0 AND caller_id = $1 AND duration > $2", s.UserID, CallTimeFree); err != nil {
		return err
	}
	s.CallerScore = ratingRated + ratingUnrated

	// Callee stats
	ratingRated = 0
	ratingUnrated = 0
	if err := db.Get(&ratingRated, "SELECT coalesce(sum(callee_rating - 3), 0) FROM calls WHERE is_hangup = true AND callee_rating > 0 AND callee_id = $1 AND duration > $2", s.UserID, CallTimeFree); err != nil {
		return err
	}
	if err := db.Get(&ratingUnrated, "SELECT count(*) FROM calls WHERE is_hangup = true AND callee_rating = 0 AND callee_id = $1 AND duration > $2", s.UserID, CallTimeFree); err != nil {
		return err
	}
	s.CalleeScore = ratingRated + ratingUnrated

	return s.Save()
}

// Save all score fields
func (s *Score) Save() error {
	if !s.initialized {
		panic("score was not properly intialized")
	}

	return save("scores", s)
}

func rebuildAllScores() {
	rows, err := db.Queryx("SELECT id FROM users ORDER BY id ASC")
	if err != nil {
		panic(err)
	}
	defer rows.Close()
	for rows.Next() {
		uid := 0
		if err = rows.Scan(&uid); err != nil {
			panic(err)
		}

		// user, err := MustGetUser(uid)
		// if err != nil {
		// 	panic(err)
		// }
		score, errx := GetScore(uint(uid))
		if errx != nil {
			panic(err)
		}
		if err = score.Rebuild(); err != nil {
			panic(err)
		}
	}
	rows.Close()

	rows, err = db.Queryx("SELECT user_id, topic_id FROM users_topics ORDER BY user_id ASC")
	if err != nil {
		panic(err)
	}
	defer rows.Close()
	var user *User
	for rows.Next() {
		var args struct {
			UserID  uint
			TopicID uint
		}
		if err := rows.StructScan(&args); err != nil {
			panic(err)
		}

		topic, err := MustGetTopic(args.TopicID)
		if err != nil {
			panic(err)
		}

		if user == nil || args.UserID != user.ID {
			user, err = MustGetUser(args.UserID)
			if err != nil {
				panic(err)
			}
		}

		if err := topic.RebuildScore(user); err != nil {
			panic(err)
		}
	}
}
