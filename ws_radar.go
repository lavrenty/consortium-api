package main

import (
	"expvar"
	"fmt"
	"time"

	"github.com/Sirupsen/logrus"
	"github.com/jmoiron/sqlx"
)

var (
	rClientsCount = expvar.NewInt("rClients")
)

const wsPusherDelay = 10 * time.Second // timeout after which we start
const wsPusherLimit = 20               // how much users to ping max

// TODO: per-user global notification rate limits
// TODO: exclude online users as they might be able to see that anyways
func wsPusher(user *User, card *Card, done chan bool) {
	logrus.Debugf("p card%d on", card.ID)

	topicIds, err := card.TopicIds()
	if err != nil {
		panic(err)
	}

	qs := fmt.Sprintf("SELECT u.id,ut.score FROM users AS u LEFT JOIN users_topics AS ut ON u.id = ut.user_id WHERE u.notifications = true AND u.next_notification_at < now() AND u.is_disabled = false AND ut.topic_id IN (?) AND ut.score > 0 AND ut.is_deleted = false AND ut.is_suggested = false AND u.languages LIKE '%%%s%%' ORDER BY ut.score DESC LIMIT %d", card.Language, wsPusherLimit)

	query, args, err := sqlx.In(qs, topicIds)
	if err != nil {
		panic(err)
	}

	var users []struct {
		ID    uint
		Score int
	}
	if err := db.Select(&users, db.Rebind(query), args...); err != nil {
		panic(err)
	}
	logrus.Debugf("p card%d users %v, topics %v", card.ID, users, topicIds)

	if len(users) == 0 {
		logrus.Warnf("p card%d no users found available for notifications", card.ID)
	}

	for _, u := range users {
		logrus.Debugf("p card%d push > user%d", card.ID, u.ID)
		if err := taskPushCardNotificationAsync(u.ID, card.ID); err != nil {
			panic(err)
		}

		select {
		case <-done:
			return

			// have a small delay between pushing out updates
		case <-time.NewTimer(100 * time.Millisecond).C:
			// do nothing
		}
	}
}

// TODO: .isLive might will not be unset on unsafe shutdown
func wsRadar(ws *wsConn, user *User, card *Card) {
	var viewsCount uint
	var viewers []uint

	rClientsCount.Add(1)
	defer rClientsCount.Add(-1)

	radar, err := dashboard.NewRadar(card.ID)
	if err != nil {
		wsError(ws, "Could not enqueue your request", err)
		return
	}
	defer radar.Stop()

	// subscribe to hub updates
	observer, err := dashboard.NewObserver()
	if err != nil {
		wsError(ws, "Could not enqueue your request", err)
		return
	}
	defer observer.Stop()
	observer.Add(card.ID)

	// recv client messages
	client := wsClient(ws, true)

	// recv ping timeouts
	timeout := wsPinger(ws)

	// update sender
	ticker := time.NewTicker(time.Second)
	defer ticker.Stop()
	metas := map[uint]Meta{}

	// pusher
	pusherTimer := time.NewTimer(wsPusherDelay)
	pusherDone := make(chan bool, 1)
	defer func() {
		pusherTimer.Stop()
		pusherDone <- true
	}()

	// mark as live
	if err := card.MarkAsLive(); err != nil {
		wsError(ws, "internal error", err)
		return
	}
	defer card.MarkAsDead()
	defer card.IncViewsCount(viewsCount)

loop:
	for {
		select {
		// start active pusher
		case <-pusherTimer.C:
			if card.Rate > 0 {
				go wsPusher(user, card, pusherDone)
			}

			// client messages
			// supported radar commands are: none
		case msg, ok := <-client:
			if !ok {
				ws.Logger().Debugf("r card%d close (client)", card.ID)
				break loop
			}
			wsError(ws, fmt.Sprintf("unknown command `%s`", msg.Method), nil)

		case userID, ok := <-radar.V:
			if !ok {
				ws.Logger().Debugf("r card%d close (wire viewers)", card.ID)
				break loop
			}

			viewers = append(viewers, userID)

			// direct card notifications
			// means someone's calling us
		case callID, ok := <-radar.C:
			if !ok {
				ws.Logger().Debugf("r card%d close (wire call)", card.ID)
				break loop
			}
			call, err := MustGetCall(callID)
			if err != nil {
				wsError(ws, "internal error fetching call details", err)
				break loop
			}
			opentok := NewOpenTokFromString(call.SessionID)

			caller, err := call.Caller()
			if err != nil || caller == nil {
				wsError(ws, "internal error fetching caller", err)
				break loop
			}

			if err := ws.WriteJSON(JSON{
				"method": "call",
				"call":   call.ToJSON(),
				"caller": caller.ToJSON(),
				"session": JSON{
					"sid":   call.SessionID,
					"token": opentok.Token(user.ID, 5*time.Minute),
				},
			}); err != nil {
				ws.Logger().Debugf("r card%d wire call failed (%v)", card.ID, err)
				break loop
			}

			if err := card.IncCallsCount(); err != nil {
				wsError(ws, "internal error", err)
				return
			}

			ws.Logger().Debugf("r card%d wire call%d", card.ID, call.ID)

			// metadata updates, we recieve only current cards
		case meta, ok := <-observer.M:
			if !ok {
				ws.Logger().Debugf("r card%d close (update)", card.ID)
				break loop
			}
			if meta.CardID == card.ID {
				metas[meta.CardID] = meta
				// viewsCount = meta.Views
			} else {
				ws.Logger().Debugf("r card%d got invalid notification for card %d", card.ID, meta.CardID)
			}

			// send update once a ticker max
		case <-ticker.C:
			if len(metas) > 0 {
				for _, meta := range metas {
					metaMsg := JSON{
						"method":  "update",
						"card_id": meta.CardID,
						"views":   meta.Views,
						"rate":    meta.Rate,
						// "busy":    meta.Busy,
						"live": meta.Live,
					}

					// logrus.Debugf("r user%d cmd > update card%d", user.ID, meta.ID)
					if err := ws.WriteJSON(metaMsg); err != nil {
						ws.Logger().Debugf("r card%d write meta failed (%s)", card.ID, err)
						break loop
					}
				}
				metas = map[uint]Meta{}
			}

			// dump users
			if len(viewers) > 0 {
				users, err := GetUsers(viewers)
				if err != nil {
					wsError(ws, "internal error", err)
					return
				}
				for _, u := range users {
					viewMsg := JSON{
						"method":  "view",
						"card_id": card.ID,
					}
					if u.Picture.Valid {
						viewMsg["picture"] = u.Picture.String
					}

					if err := ws.WriteJSON(viewMsg); err != nil {
						ws.Logger().Debugf("r card%d write view failed (%s)", card.ID, err)
						break loop
					}
				}

				viewers = []uint{}
			}

			// client ping/pong timeout
		case <-timeout:
			ws.Logger().Debugf("r card%d close (timeout)", card.ID)
			break loop
		}
	}

	// logrus.Debugf("r card%d off", card.ID)
}
