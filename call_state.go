package main

import (
	"expvar"
	"math"
	"sync"
	"time"
)

var cLive = expvar.NewInt("cLive")

// CallPeer describes state of caller or callee attached to CallProcess
type CallPeer struct {
	UserID uint

	Connected int32

	// indicates wether peer is connected and listening to updates
	Live   int32
	Hangup int32

	// user specific resulting cost of the call
	Cost int

	//
	Wire chan bool
}

// CallState represents state
type CallState struct {
	mu sync.Mutex

	CallID uint
	CardID uint
	Caller CallPeer
	Callee CallPeer

	Duration time.Duration
	Rate     uint

	LiveSince time.Time

	hangupOnce sync.Once
}

// ToJSON returns JSON value
func (s *CallState) ToJSON(user *User) JSON {
	s.mu.Lock()
	defer s.mu.Unlock()

	d := s.Duration
	if !s.LiveSince.IsZero() {
		d = d + time.Since(s.LiveSince)
	}

	cost := GetCost(uint(math.Ceil(d.Seconds())), s.Rate, s.IsHangup())
	c := cost.CalleeCost
	if s.Caller.UserID == user.ID {
		c = cost.CallerCost
	}

	j := JSON{
		"rate":     s.Rate,
		"duration": cost.Duration,
		"live":     s.IsLive(),
		"hangup":   cost.Hangup,
		"cost":     c,
	}

	return j
}

var gcsLock sync.Mutex
var gcs map[uint]*CallState

// GetCallState returns call state associated with current call
func GetCallState(call *Call, user *User) (*CallState, chan bool) {
	gcsLock.Lock()
	defer gcsLock.Unlock()

	if gcs == nil {
		gcs = make(map[uint]*CallState, 0)
	}

	s, ok := gcs[call.ID]
	if !ok {
		s = &CallState{
			CallID: call.ID,
			CardID: call.CardID,
			Caller: CallPeer{UserID: call.CallerID},
			Callee: CallPeer{UserID: call.CalleeID},
			Rate:   call.Rate,
		}
		gcs[call.ID] = s
	}

	// check peer is not connected already
	peer := s.getPeer(user)
	if atomicLoadBool(&peer.Connected) {
		return nil, nil
	}

	return s, s.Connect(user)
}

func (s *CallState) getPeer(user *User) *CallPeer {
	peer := &s.Caller
	if user.ID == s.Callee.UserID {
		peer = &s.Callee
	}

	if user.ID != peer.UserID {
		panic("getPeer: user.ID != peer.UserID")
	}

	return peer
}

func (s *CallState) getOtherPeer(user *User) *CallPeer {
	peer := &s.Caller
	if user.ID != s.Callee.UserID {
		peer = &s.Callee
	}

	return peer
}

// Connect user connects and will notify us
func (s *CallState) Connect(user *User) chan bool {
	s.mu.Lock()
	defer s.mu.Unlock()

	peer := s.getPeer(user)
	atomicStoreBool(&peer.Connected, true)

	peer.Wire = make(chan bool)

	s.notifyOther(user)

	if dashboard != nil {
		dashboard.notifyCaller(s.CardID, s.Caller.UserID)
	}

	return peer.Wire
}

// Close when user socket disconnects, marks connected as false
func (s *CallState) Close(user *User) {
	s.mu.Lock()
	defer s.mu.Unlock()

	// mark dead
	s.dead(user)

	// mark disConnected
	peer := s.getPeer(user)
	atomicStoreBool(&peer.Connected, false)

	// close wire
	// close(peer.Wire)

	// no one connected? hangup once and unlink us
	if s.IsEmpty() {
		if dashboard != nil {
			dashboard.notifyCaller(s.CardID, 0)
		}

		s.hangup()

		gcsLock.Lock()
		delete(gcs, s.CallID)
		gcsLock.Unlock()
	} else {
		// connected? notify about changes
		s.notifyOther(user)
	}
}

// Live marks connection as live
func (s *CallState) Live(user *User) {
	s.mu.Lock()
	defer s.mu.Unlock()

	wasLive := s.IsLive()

	peer := s.getPeer(user)
	atomicStoreBool(&peer.Live, true)
	if s.IsLive() && !wasLive {
		cLive.Add(1)

		if !s.LiveSince.IsZero() {
			panic("live: timer start but already running")
		}
		// start timer
		s.LiveSince = time.Now()

		// notify
		s.notifyOther(user)
	}
}

// Dead connection
func (s *CallState) Dead(user *User) {
	s.mu.Lock()
	defer s.mu.Unlock()

	wasLive := s.IsLive()
	s.dead(user)

	if wasLive {
		// notify
		s.notifyOther(user)
	}
}

func (s *CallState) dead(user *User) {
	peer := s.getPeer(user)
	atomicStoreBool(&peer.Live, false)

	if !s.LiveSince.IsZero() {
		// stop timer
		s.Duration = s.Duration + time.Since(s.LiveSince)
		s.LiveSince = time.Time{}
		cLive.Add(-1)
	}
}

// Hangup current connection
func (s *CallState) Hangup(user *User) {
	s.mu.Lock()
	defer s.mu.Unlock()

	peer := s.getPeer(user)
	atomicStoreBool(&peer.Hangup, true)

	// mark as dead and stop timer
	s.dead(user)

	// hangup
	s.hangup()

	// notify
	s.notifyOther(user)
}

func (s *CallState) hangup() {
	if !s.LiveSince.IsZero() {
		panic("hangup: timer is running")
	}

	s.hangupOnce.Do(func() {
		call, err := GetCall(s.CallID)
		if err != nil {
			panic(err) // TODO ???
		}
		if call == nil {
			panic("call is nil") // TODO ???
		}
		if err := call.Hangup(s.Duration); err != nil {
			panic(err) // TODO ???
		}
	})
}

func (s *CallState) notifyOther(user *User) {
	peer := s.getOtherPeer(user)
	if atomicLoadBool(&peer.Connected) {
		go func(wire chan bool) {
			select {
			case wire <- true:
			case <-time.After(1 * time.Second):
			}
		}(peer.Wire)
	}
}

// IsLive atomic
func (s *CallState) IsLive() bool {
	return s.IsConnected() && atomicLoadBool(&s.Callee.Live) && atomicLoadBool(&s.Caller.Live)
}

// IsConnected atomic
func (s *CallState) IsConnected() bool {
	return atomicLoadBool(&s.Callee.Connected) && atomicLoadBool(&s.Caller.Connected)
}

// IsHangup atomic
func (s *CallState) IsHangup() bool {
	return atomicLoadBool(&s.Callee.Hangup) || atomicLoadBool(&s.Caller.Hangup) || s.IsEmpty()
}

// IsEmpty atomic
func (s *CallState) IsEmpty() bool {
	return !atomicLoadBool(&s.Callee.Connected) && !atomicLoadBool(&s.Caller.Connected)
}

// GetDuration returns duration interval
func (s *CallState) GetDuration() time.Duration {
	s.mu.Lock()
	defer s.mu.Unlock()

	d := s.Duration
	if !s.LiveSince.IsZero() {
		d = d + time.Since(s.LiveSince)
	}
	return d
}

// GetCost returns cost for specified user
func (s *CallState) GetCost(user *User) uint {
	s.mu.Lock()
	defer s.mu.Unlock()

	d := s.Duration
	if !s.LiveSince.IsZero() {
		d = d + time.Since(s.LiveSince)
	}

	caller := s.Caller.UserID == user.ID
	cost := GetCost(uint(math.Ceil(d.Seconds())), s.Rate, s.IsHangup())
	if caller {
		return cost.CallerCost
	}
	return cost.CalleeCost
}
