package main

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"testing"

	"github.com/Pallinder/go-randomdata"
	"github.com/stripe/stripe-go"
)

func TestMain(m *testing.M) {
	loadConfig("conspiracy.test.conf")

	workerInlineSetup()

	// create schema
	cleanup, err := ioutil.ReadFile("db/cleanup.sql")
	if err != nil {
		panic(err)
	}

	schema, err := ioutil.ReadFile("db/schema.sql")
	if err != nil {
		panic(err)
	}

	db.MustExec(string(cleanup))
	db.MustExec(string(schema))

	// populateDb
	populateDb()

	os.Exit(m.Run())
}

func testGetUser(t *testing.T, ID interface{}) *User {
	user, err := GetUser(ID)
	if err != nil || user == nil {
		t.Fatal(ID, err, user)
	}
	return user
}

func testCreateUser(t *testing.T) *User {
	user, err := FindOrCreateUserFromFacebook(fmt.Sprintf("fb%d", rand.Int63()))
	if err != nil {
		t.Fatal(err)
	}
	return user
}

func testGetTopic(t *testing.T, ID interface{}) *Topic {
	topic, err := GetTopic(ID)
	if err != nil || topic == nil {
		t.Fatal(ID, err, topic)
	}
	return topic
}

func testGetCard(t *testing.T, ID interface{}) *Card {
	card, err := GetCard(ID)
	if err != nil || card == nil {
		t.Fatal(ID, err, card)
	}
	return card
}

func testCreateCard(t *testing.T, user *User, names []string) *Card {
	card := &Card{
		UserID: user.ID,
		Text:   randomdata.Paragraph(),
		Rate:   uint(CardMinRate + rand.Intn(500)),
	}
	if err := card.Create(); err != nil {
		t.Fatal(err)
	}

	// check
	testGetCard(t, card.ID)

	// generate topics
	topics := []Topic{}
	for _, name := range names {
		topic, err := FindOrCreateTopicByName(name)
		if err != nil {
			t.Fatal(err)
		}
		topics = append(topics, *topic)
	}

	// join topics
	for _, topic := range topics {
		if err := card.Join(topic); err != nil {
			t.Fatal(err)
		}
	}

	return card
}

func testGetCall(t *testing.T, ID interface{}) *Call {
	call, err := GetCall(ID)
	if err != nil || call == nil {
		t.Fatal(ID, err, call)
	}
	return call
}

func testCreateCall(t *testing.T, rate uint) *Call {
	user := testCreateUser(t)
	user.FakeFacebookVerification()
	if err := user.AddCard(&stripe.CardParams{
		Number: "4242424242424242",
		CVC:    "234",
		Month:  "10",
		Year:   "2020",
		Name:   "Test User",
	}); err != nil {
		t.Fatal(err)
	}

	card := testCreateCard(t, user, []string{"Bitcoin", "Money"})

	other := testCreateUser(t)

	call := &Call{CardID: card.ID, CallerID: other.ID, CalleeID: card.UserID, Rate: card.Rate, SessionID: fmt.Sprintf("%16x%16x", rand.Int63(), rand.Int63())}
	if err := call.Create(); err != nil {
		t.Fatal(err)
	}

	return call
}

func populateDb() {
	// zz
	_, _ = FindOrCreateUserFromFacebook("123456")
	// jovan
	_, _ = FindOrCreateUserFromFacebook("123458")
	// max
	_, _ = FindOrCreateUserFromFacebook("123459")
	// enze
	_, _ = FindOrCreateUserFromFacebook("123460")

	_, _ = FindOrCreateTopicByName("Web Development")
	_, _ = FindOrCreateTopicByName("Investing")
	_, _ = FindOrCreateTopicByName("Technology")
	_, _ = FindOrCreateTopicByName("C (programming language)")
	_, _ = FindOrCreateTopicByName("Entrepreneurship")
	_, _ = FindOrCreateTopicByName("Fashion and Style")
	_, _ = FindOrCreateTopicByName("Health")
	_, _ = FindOrCreateTopicByName("Costa Rica")
	_, _ = FindOrCreateTopicByName("Quora")
	_, _ = FindOrCreateTopicByName("Movies")
	_, _ = FindOrCreateTopicByName("Amazon Web Services")
	_, _ = FindOrCreateTopicByName("Mathematics")
	_, _ = FindOrCreateTopicByName("Startup Advice and Strategy")
	_, _ = FindOrCreateTopicByName("Web Applications")
}
