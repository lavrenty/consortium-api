// tasks to be executed from worker

package main

import "log"

func taskUpdateOtherProfileFacebook(userID uint, _ JSON) error {
	user, err := GetUser(userID)
	if err != nil {
		return err
	}
	if user == nil {
		log.Printf("updateother %d: not found", userID)
		return nil
	}

	if !user.FbAccessToken.Valid {
		log.Printf("updateother %d: empty token", userID)
		return nil
	}

	// what if user disconnected?
	f, err := NewFacebookFromToken(user.FbAccessToken.String)
	if err != nil {
		return err
	}

	return f.UpdateOther(user)
}

func taskUpdateOtherProfileFacebookAsync(userID uint) error {
	return worker.PerformAsync(taskUpdateOtherProfileFacebookString, userID, nil)
}

func taskScoreRebuild(userID uint, _ JSON) error {
	user, err := GetUser(userID)
	if err != nil {
		return err
	}
	if user == nil {
		log.Printf("scorerebuild %d: user not found\n", userID)
		return nil
	}

	score, err := GetScore(user.ID)
	if err != nil {
		return err
	}
	return score.Rebuild()
}

func taskScoreRebuildAsync(userID uint) error {
	return worker.PerformAsync(taskScoreRebuildString, userID, nil)
}

func taskCallTopicsScoreRebuild(callID uint, _ JSON) error {
	call, err := GetCall(callID)
	if err != nil {
		return err
	}
	if call == nil {
		log.Printf("calltopicsscorerebuild %d: card not found\n", callID)
		return nil
	}

	return call.TopicsScoreRebuild()
}

func taskCallTopicsScoreRebuildAsync(callID uint) error {
	return worker.PerformAsync(taskCallTopicsScoreRebuildString, callID, nil)
}
