package main

// SystemCardTopic ...
type SystemCardTopic struct {
	UserID  uint
	TopicID uint
}

// NewSystemCardTopic ...
func NewSystemCardTopic(user *User, topic *Topic) SystemCardTopic {
	if topic == nil || user == nil {
		panic("invalid argument")
	}

	return SystemCardTopic{UserID: user.ID, TopicID: topic.ID}
}

// Type ...
func (c SystemCardTopic) Type() string {
	return "topic"
}

// ToJSON ...
// because it's result of pending endorsements, endorser == current user
func (c SystemCardTopic) ToJSON() JSON {
	topic, err := MustGetTopic(c.TopicID)
	if err != nil {
		panic(err)
	}

	return JSON{
		"topic": topic.ToJSON(),
	}
}

// Close ...
func (c SystemCardTopic) Close() error {
	user, err := MustGetUser(c.UserID)
	if err != nil {
		panic(err)
	}
	topic, err := MustGetTopic(c.TopicID)
	if err != nil {
		panic(err)
	}

	return user.UpdateSuggestedTopicCardAsSeen(topic)
}
