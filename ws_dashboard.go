// NOTE: hate this code, needs global re-think
package main

import (
	"fmt"
	"math/rand"
	"strconv"
	"time"

	"github.com/Sirupsen/logrus"
	"github.com/jmoiron/sqlx"
)

type dashboardState struct {
	ws   *wsConn
	user *User

	// slots is how much space onscreen we have for this specific user excluding system card(s) if shown
	// e.g. it's not an empty space or busy space it's almost constant amount of overall space we're working with
	slots int
	// busy slots on contrary keeps track of how much busy slots there are currently
	busySlots int

	// active / inactive cards
	// active cards is what person sess
	cards map[uint]bool

	// current active cards metadata cache (sent out and cleared on every update)
	buffer map[uint]Meta

	// list of card ids that are about to close and specific time
	closing map[uint]time.Time

	sc SystemCard

	observer               *Observer
	outdatedCards          map[uint]*Meta
	outdatedCardsCache     map[uint]*Meta
	outdatedCardsLastFetch time.Time
}

func (state *dashboardState) freeSlots() int {
	return state.slots - state.busySlots
}

func (state *dashboardState) writeSystemCard() (bool, error) {
	if state.sc != nil {
		panic("writeSystemCard called with sc != nil")
	}

	sc, err := GetSystemCard(state.user.ID)
	if err != nil {
		wsError(state.ws, "internal error fetching system card", err)
		return false, err
	}
	if sc != nil {
		state.sc = sc
		state.slots--
		if state.slots < 0 {
			panic(fmt.Sprintf("state slots reached <0, %v", state))
		}

		if err := state.ws.WriteJSON(JSON{
			"method": "system",
			"id":     1,
			"type":   sc.Type(),
			"data":   sc.ToJSON(),
		}); err != nil {
			logrus.Debugf("d user%d write failed (%s)", state.user.ID, err)
			return false, err
		}

		return true, nil
	}

	return false, nil
}

func (state *dashboardState) closeSystemCard() error {
	if state.sc != nil {
		if err := state.sc.Close(); err != nil {
			wsError(state.ws, "internal error closing system card", err)
			return err
		}
		state.sc = nil
		state.slots++
	}

	return nil
}

func (state *dashboardState) writeClosed() error {
	for cardID, discardAt := range state.closing {
		if state.cards[cardID] && discardAt.Before(time.Now()) {
			// logrus.Debugf("d user%d cmd > close card%d", user.ID, cardID)
			if err := state.ws.WriteJSON(JSON{
				"method":  "close",
				"card_id": cardID,
			}); err != nil {
				logrus.Debugf("d user%d write failed (%s)", state.user.ID, err)
				return err
			}
			delete(state.closing, cardID)
		}

		// if not in active cards, discard that anyways
		if !state.cards[cardID] {
			delete(state.closing, cardID)
		}
	}

	return nil
}

func (state *dashboardState) writeCards() (err error) {
	if state.sc == nil && state.freeSlots() > 0 && rand.Intn(3) == 0 {
		if _, err = state.writeSystemCard(); err != nil {
			return
		}
	}

	if state.freeSlots()+len(state.outdatedCards) > 0 {
		if err = state.writeUserCards(); err != nil {
			return
		}
	}

	return
}

func (state *dashboardState) writeUserCards() error {
	metas, err := dashboard.Fetch(&FetchOptions{
		Ignore: state.cards,
		User:   state.user,
		Limit:  state.freeSlots() + len(state.outdatedCards),
	})

	if len(metas) > 0 {
	loop:
		for id := range state.outdatedCards {
			state.closing[id] = time.Now()
			state.writeClosed()
			state.closeUserCard(id)
			delete(state.outdatedCards, id)
			if state.freeSlots() >= len(metas) {
				break loop
			}
		}
	}

	if len(metas) < state.freeSlots() {
		outdated, errx := state.fetchOutdatedCards(state.freeSlots() - len(metas))
		if errx != nil {
			wsError(state.ws, "internal error fetching outdated cards", errx)
			return errx
		}

		for _, meta := range outdated {
			metas = append(metas, meta)
			state.outdatedCards[meta.CardID] = meta
		}
	}

	if err != nil {
		wsError(state.ws, "internal error fetching cards", err)
		return err
	}

	for _, meta := range metas {
		if err := state.writeUserCard(meta); err != nil {
			return err
		}

		state.busySlots++
		state.cards[meta.CardID] = true
		state.observer.Add(meta.CardID)
	}

	return nil
}

func (state *dashboardState) writeUserCard(meta *Meta) error {
	topics := JSON{}

	card, err := MustGetCard(meta.CardID)
	if err != nil {
		wsError(state.ws, "internal error getting card", err)
		return err
	}

	user, err := MustGetUser(card.UserID)
	if err != nil {
		wsError(state.ws, "internal error fetching user", err)
		return err
	}

	_topics, err := card.Topics()
	if err != nil {
		wsError(state.ws, "internal error fetching card topics", err)
		return err
	}

	for _, topic := range _topics {
		topics[strconv.Itoa(int(topic.ID))] = topic.ToJSON()
	}

	users := JSON{strconv.Itoa(int(user.ID)): user.ToCondensedJSON()}
	// if there is caller id, add that up
	if meta.CallerID != 0 {
		caller, err := MustGetUser(meta.CallerID)
		if err != nil {
			wsError(state.ws, "internal error fetching caller", err)
			return err
		}
		users[strconv.Itoa(int(caller.ID))] = caller.ToCondensedJSON()
	}

	// logrus.Debugf("d user%d cmd > show %d", u.ID, card.ID)
	if err := state.ws.WriteJSON(JSON{
		"method": "show",
		"card":   card.ToJSON(meta),
		"users":  users,
		"topics": topics,
	}); err != nil {
		logrus.Debugf("d user%d send user card failed %v", state.user.ID, err)
		return err
	}

	return nil
}

func (state *dashboardState) closeUserCard(cardID uint) {
	if state.cards[cardID] {
		// logrus.Debugf("d user%d cmd < close card%d", user.ID, uint(cid))

		// mark as not present on screen (closed by user)
		// and delete from closing cards list
		state.cards[cardID] = false
		state.observer.Remove(cardID)
		state.busySlots--
		delete(state.closing, cardID)
	} else {
		v, ok := state.cards[cardID]
		logrus.Debugf("d user%d cmd < close card%d (bad %v ok %v)", state.user.ID, cardID, v, ok)
	}
}

func (state *dashboardState) updateBuffer(meta *Meta) {
	if state.cards[meta.CardID] {
		state.buffer[meta.CardID] = *meta

		if meta.Live {
			delete(state.closing, meta.CardID)
		} else {
			state.closing[meta.CardID] = time.Now().Add(time.Duration(5+rand.Intn(20)) * time.Second)
		}
	} else {
		logrus.Debugf("d user%d got invalid notification for card %d", state.user.ID, meta.CardID)
	}
}

func (state *dashboardState) writeBuffer() error {
	for key, meta := range state.buffer {
		metaMsg := JSON{
			"method":    "update",
			"card_id":   meta.CardID,
			"caller_id": meta.CallerID,
			"views":     meta.Views,
			"rate":      meta.Rate,
			"live":      meta.Live,
		}

		if meta.CallerID != 0 {
			caller, err := MustGetUser(meta.CallerID)
			if err != nil {
				wsError(state.ws, "failed to load caller", err)
				return err
			}
			metaMsg["users"] = JSON{strconv.Itoa(int(caller.ID)): caller.ToCondensedJSON()}
		}

		// logrus.Debugf("d user%d cmd > update card%d", user.ID, meta.ID)
		if err := state.ws.WriteJSON(metaMsg); err != nil {
			logrus.Debugf("d user%d write failed (%s)", state.user.ID, err)
			return err
		}
		delete(state.buffer, key)
	}

	return nil
}

func (state *dashboardState) flush() error {
	if err := state.writeClosed(); err != nil {
		return err
	}

	if err := state.writeBuffer(); err != nil {
		return err
	}

	return nil
}

func (state *dashboardState) writeDebug() error {
	cardsLiveDebug := make([]uint, 0, len(state.cards))
	cardsDeadDebug := make([]uint, 0, len(state.cards))
	for id, val := range state.cards {
		if val {
			cardsLiveDebug = append(cardsLiveDebug, id)
		} else {
			cardsDeadDebug = append(cardsDeadDebug, id)
		}
	}
	bufferDebug := make(map[string]uint, len(state.buffer))
	for id, val := range state.buffer {
		bufferDebug[strconv.Itoa(int(id))] = val.CardID
	}
	closingDebug := make(map[string]string, len(state.closing))
	for id, val := range state.closing {
		closingDebug[strconv.Itoa(int(id))] = time.Since(val).String()
	}
	observerDebug := make([]uint, 0, len(state.observer.ids))
	for id := range state.observer.ids {
		observerDebug = append(observerDebug, id)
	}

	return state.ws.WriteJSON(JSON{
		"method": "debug",
		"slots": JSON{
			"user": state.slots,
			"free": state.freeSlots(),
			"busy": state.busySlots,
		},
		"cards": JSON{
			"live": cardsLiveDebug,
			"dead": cardsDeadDebug,
		},
		"buffer":   bufferDebug,
		"closing":  closingDebug,
		"observer": observerDebug,
	})
}

// TODO: make sure that we always fetch something
func (state *dashboardState) fetchOutdatedCards(amount int) ([]*Meta, error) {
	var metas []*Meta

	// state.ws.Logger().Debugf("fetchOutdatedCards: amount %v, len(cache) = %v, last fetch %s", amount, len(state.outdatedCardsCache), time.Since(state.outdatedCardsLastFetch))
	if len(state.outdatedCardsCache) < amount {
		if state.outdatedCardsLastFetch.Add(10 * time.Minute).After(time.Now()) {
			return metas, nil
		}

		topicIds, err := state.user.ActiveTopicIds()
		if err != nil {
			return nil, err
		}

		if len(topicIds) == 0 {
			topicIds, err = GetTopTopicIds(20)
			if err != nil {
				return nil, err
			}
		}

		excludedCardIds := []uint{}
		for id := range state.cards {
			excludedCardIds = append(excludedCardIds, id)
		}

		for id := range state.outdatedCardsCache {
			excludedCardIds = append(excludedCardIds, id)
		}

		// sqlx.In doesn't like empty arrays ;(
		if len(excludedCardIds) == 0 {
			excludedCardIds = append(excludedCardIds, 0)
		}

		query, args, err := sqlx.In("SELECT DISTINCT c.* FROM cards AS c LEFT JOIN cards_topics AS t ON t.card_id = c.id LEFT JOIN users AS u ON c.user_id = u.id LEFT JOIN scores AS s ON s.user_id = c.user_id WHERE s.callee_score >= 0 AND u.is_disabled = false AND c.is_live = false AND c.calls_count > 0 AND c.user_id != ? AND t.topic_id IN (?) AND c.id NOT IN (?) ORDER BY c.id DESC LIMIT 30", state.user.ID, topicIds, excludedCardIds)
		if err != nil {
			return nil, err
		}

		query = db.Rebind(query)

		var cards []Card

		err = db.Select(&cards, query, args...)
		if err != nil {
			return nil, err
		}

		if len(cards) == 0 {
			state.outdatedCardsLastFetch = time.Now()
		}

		for _, card := range cards {
			meta, err := card.ToMeta()
			if err != nil {
				return nil, err
			}

			// TODO: this must be optimized
			if card.CallsCount > 0 {
				calls, err := GetCallsByCardID(card.ID)
				if err != nil {
					return nil, err
				}
				if len(calls) > 0 {
					meta.CallerID = calls[0].CallerID
				}
			}

			state.outdatedCardsCache[meta.CardID] = meta
		}
	}

loop:
	for id, meta := range state.outdatedCardsCache {
		if len(metas) >= amount {
			break loop
		}

		metas = append(metas, meta)
		delete(state.outdatedCardsCache, id)
	}

	return metas, nil
}

func wsDashboard(ws *wsConn, user *User) {
	observer, err := dashboard.NewObserver()
	if err != nil {
		wsError(ws, "internal error", err)
		return
	}
	defer observer.Stop()

	state := &dashboardState{
		ws:                     ws,
		user:                   user,
		slots:                  DashboardSlots,
		cards:                  make(map[uint]bool, 0),
		buffer:                 make(map[uint]Meta, 0),
		closing:                make(map[uint]time.Time, 0),
		observer:               observer,
		outdatedCards:          make(map[uint]*Meta, 0),
		outdatedCardsCache:     make(map[uint]*Meta, 0),
		outdatedCardsLastFetch: time.Now().Add(-10 * time.Minute),
	}

	// recv client messages
	client := wsClient(ws, true)

	// recv ping timeouts
	timeout := wsPinger(ws)

	// update sender
	ticker := time.NewTicker(time.Second)
	defer ticker.Stop()

	if err := state.writeCards(); err != nil {
		return
	}

	for {
		// logrus.Debugf("d user%d cards{%v} deads{%v} cache{%v}", user.ID, cards, deads, cache)
		select {
		// client ping/pong timeout
		case <-timeout:
			ws.Logger().Debugf("d user%d close (timeout)", user.ID)
			return

			// client messages
			// supported dashboard commands are: debug, close
		case msg, ok := <-client:
			if !ok {
				ws.Logger().Debugf("d user%d close (client)", user.ID)
				return
			}

			switch msg.Method {
			case "debug":
				if err := state.writeDebug(); err != nil {
					ws.Logger().Debugf("d user%d cmd < debug failed %v", user.ID, err)
					return
				}

			case "close":
				if _, ok := msg.Raw["system_id"].(float64); ok {
					if err := state.closeSystemCard(); err != nil {
						ws.Logger().Debugf("d user%d close system failed %v", user.ID, err)
						return
					}
				} else if cid, ok := msg.Raw["card_id"].(float64); ok {
					state.closeUserCard(uint(cid))
				} else {
					wsError(ws, "invalid card id", fmt.Errorf("invalid card id: %#v", msg.Raw["card_id"]))
				}

				// send new cards
				if err := state.writeCards(); err != nil {
					return
				}

			default:
				// should not get here
				wsError(ws, fmt.Sprintf("unknown command `%s`", msg.Method), nil)
			}

		case meta, ok := <-state.observer.M:
			// meta updates
			if !ok {
				return
			}

			state.updateBuffer(&meta)

			// flush cached / updated data
		case <-ticker.C:
			if err := state.flush(); err != nil {
				return
			}

			if err := state.writeCards(); err != nil {
				return
			}
		}
	}
}
