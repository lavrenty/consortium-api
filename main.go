package main

import (
	"encoding/hex"
	"flag"
	"fmt"
	"log"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"

	"gopkg.in/redis.v3"

	"github.com/BurntSushi/toml"
	"github.com/Sirupsen/logrus"
	"github.com/stripe/stripe-go"

	"github.com/getsentry/raven-go"
	"github.com/gin-gonic/gin"

	_ "database/sql"

	_ "expvar"
	_ "net/http/pprof"

	"github.com/gin-gonic/contrib/expvar"
	"github.com/gin-gonic/contrib/secure"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

var config struct {
	Debug    bool
	Listen   string
	Origin   string
	Database string
	Secret   string
	Sentry   string
	Redis    struct {
		Addrs    []string
		Password string
		DB       int64 `toml:"db"`
		PoolSize int   `toml:"pool_size"`
	}
	Facebook struct {
		AppID            string `toml:"app_id"`
		Secret           string
		AccountKitSecret string `toml:"account_kit_secret"`
	}
	Opentok struct {
		APIKey string `toml:"api_key"`
		Secret string
	}
	NSQ struct {
		Namespace            string
		NsqdTCPAddress       string   `toml:"nsqd_tcp_address"`
		LookupdHTTPAddresses []string `toml:"lookupd_http_addresses"`
	}
	Google struct {
		APIKey string `toml:"api_key"`
	}
	Stripe struct {
		Key            string
		PublishableKey string `toml:"publishable_key"`
		WebhookKey     string `toml:"webhook_key"`
	}
	APNS struct {
		Topic    string
		ProdCert string `toml:"prod_cert"`
		ProdPass string `toml:"prod_pass"`
		DevCert  string `toml:"dev_cert"`
		DevPass  string `toml:"dev_pass"`
	}
	// Content Security Policy
	CSP string
	// HSTS
	STS int64
}
var jwtSecret []byte
var db *sqlx.DB
var sentry *raven.Client
var dashboard *Dashboard

// R is global redis client
var R *redis.Client
var locker *RedLock

var configPath string
var tokenVar bool
var radarVar int
var dashboardVar bool
var opentokVar bool
var userVar int
var callVar int
var dialVar int
var simulateVar int
var workerVar bool
var workerPushVar string
var rescoreVar bool
var schedulerVar bool
var pusherVar bool
var pusherPushVar string
var dashcloneVar bool
var migrateVar string

// JSON our
type JSON map[string]interface{}

func loadConfig(name string) {
	var err error

	if _, err = toml.DecodeFile(name, &config); err != nil {
		panic(err)
	}

	// config
	jwtSecret, err = hex.DecodeString(config.Secret)
	if err != nil {
		panic(err)
	}

	// db
	db, err = sqlx.Connect("postgres", config.Database)
	if err != nil {
		panic(fmt.Sprintf("postgres connect failed: %v", err))
	}
	if err = db.Ping(); err != nil {
		panic(fmt.Sprintf("postgres ping failed: %v", err))
	}
	db.SetMaxOpenConns(20)
	// db.SetMaxIdleConns(20)
	db.MapperFunc(dbUnderscore)

	// sentry
	sentry, err = raven.NewClient(config.Sentry, nil)
	if err != nil {
		panic(fmt.Sprintf("sentry new client failed: %v", err))
	}

	// redis
	R = redis.NewClient(&redis.Options{
		Addr:     config.Redis.Addrs[0],
		Password: config.Redis.Password,
		DB:       config.Redis.DB,
		PoolSize: config.Redis.PoolSize,
	})
	if err = R.Ping().Err(); err != nil {
		panic(fmt.Sprintf("redis ping failed: %v", err))
	}

	locker, err = NewRedLock(R, "lock:main")
	if err != nil {
		panic(err)
	}

	// stripe
	if len(config.Stripe.Key) < 2 || config.Stripe.Key[0:2] != "sk" {
		panic("stripe key is invalid (does not start with 'sk')")
	}
	if len(config.Stripe.PublishableKey) < 2 || config.Stripe.PublishableKey[0:2] != "pk" {
		panic("stripe publishable key is invalid (does not start with 'pk')")
	}

	stripe.Key = config.Stripe.Key
	if config.Debug {
		stripe.LogLevel = 2
	}

	// apns
	if _, err := os.Stat(config.APNS.ProdCert); os.IsNotExist(err) {
		panic("apns.prod_cert file is missing")
	}

	if _, err := os.Stat(config.APNS.DevCert); os.IsNotExist(err) {
		panic("apns.dev_cert file is missing")
	}

	if len(config.NSQ.Namespace) == 0 {
		panic("nsq.namespace is not set or empty")
	}

	// // google translation api
	// if _, err := languages.Detect("i hope you’re working, buddy"); err != nil {
	// 	panic(err)
	// }
}

func setupGin() {
	// gin
	ginMode := gin.ReleaseMode
	if config.Debug {
		ginMode = gin.DebugMode
	}
	gin.SetMode(ginMode)

	// routes
	r := gin.New()

	r.LoadHTMLGlob("templates/*")

	r.Use(RequestID())
	r.Use(Logger())
	r.Use(Recovery())
	r.Use(LogRecovery())

	r.Use(func(c *gin.Context) {
		if c.Request.Method == "GET" || c.Request.Method == "POST" || c.Request.Method == "DELETE" || c.Request.Method == "PUT" {
			c.Header("Pragma", "no-cache")
			c.Header("Expires", "Tue, 31 Mar 1981 05:00:00 GMT")
			c.Header("Cache-Control", "no-cache, no-store, must-revalidate, pre-check=0, post-check=0")
		}
	})

	r.Use(secure.Secure(secure.Options{
		STSSeconds:            config.STS,
		FrameDeny:             true,
		ContentTypeNosniff:    true,
		BrowserXssFilter:      true,
		ContentSecurityPolicy: config.CSP,
		IsDevelopment:         len(config.CSP) == 0 && config.STS == 0,
	}))

	r.Static("/assets", "./assets")

	// r.POST("/v1/auth/digits/connect", handleDigitsConnect)
	// r.POST("/v1/auth/account/connect", handleAccountKitConnect)
	r.POST("/v1/auth/facebook/connect", handleFacebookConnect)

	v1 := r.Group("/v1", requireToken())
	{
		// v1.POST("/auth/facebook/disconnect", handleFacebookDisconnect)

		v1.GET("/user/:user/profile", preloadUser(), handleUserProfile)
		v1.POST("/user/:user/endorse/:topic", preloadUser(), preloadTopic(), handleUserEndorse)
		v1.POST("/user/:user/language/:lang", preloadUser(), preloadLang(), handleUserLanguageUpdate)
		v1.DELETE("/user/:user/language/:lang", preloadUser(), preloadLang(), handleUserLanguageDestroy)
		v1.POST("/user/:user/languages", preloadUser(), handleUserLanguagesUpdate)
		v1.DELETE("/user/:user", preloadUser(), handleUserDelete)

		v1.POST("/notifications", handleNotificationsEnable)
		v1.DELETE("/notifications", handleNotificationsDisable)
		if config.Debug {
			r.POST("/debug/pusher/:user/:card", preloadUser(), preloadCard(), handleNotificationsPushDebug)
		}

		v1.GET("/topics/suggest", handleTopicsSuggest)
		v1.GET("/topics/recent", handleTopicsRecent)
		v1.GET("/topics/autocomplete", handleTopicsAutocomplete)
		v1.POST("/topics/subscribe", handleTopicsSubscribe)
		v1.DELETE("/topic/:topic", preloadTopic(), handleTopicUnsubscribe)

		v1.POST("/cards", handleCardsCreate)
		v1.GET("/card/:card", preloadCard(), handleCard)
		v1.POST("/card/:card/dial", preloadCard(), handleCardDial)
		v1.GET("/card/:card/radar", preloadCard(), handleCardRadarWebsocket) // wss, radar session
		v1.GET("/card/:card/report", preloadCard(), handleCardReport)
		v1.POST("/card/:card/report", preloadCard(), handleCardReportSubmit)
		v1.POST("/cards/rates", handleCardsRates)
		v1.GET("/cards/dashboard", handleCardsDashboardWebsocket) // wss, dashboard session

		v1.GET("/call/:call", preloadCall(), handleCallWebsocket) // wss, call session
		v1.POST("/call/:call/rate", preloadCall(), handleCallRate)
		v1.GET("/call/:call/report", preloadCall(), handleCallReport)
		v1.POST("/call/:call/report", preloadCall(), handleCallReportSubmit)

		v1.GET("/history", handleHistory)
		v1.GET("/history/:call", preloadCall(), handleHistoryItem)

		// Global billing customer iframe setup script
		v1.GET("/billing/setup", handleBillingSetup)

		// Customer billing (add / remove credit card)
		v1.POST("/billing/card", handleBillingCard)
		v1.DELETE("/billing/card", handleBillingCardDestroy)

		// Stripe-specific billing settings (payout only)
		v1.GET("/billing/stripe", handleStripeAccount)
		v1.POST("/billing/stripe", handleStripeAccountCreate)
		v1.PUT("/billing/stripe", handleStripeAccountUpdate)
		v1.DELETE("/billing/stripe", handleStripeAccountDelete)

		v1.POST("/billing/stripe/bank", handleStripeAccountBankCreate)
		v1.POST("/billing/stripe/card", handleStripeAccountCardCreate)

		// Issue payout
		v1.POST("/billing/transfer", handleBillingTransfer)
	}

	// Stripe webhook
	r.POST("/v1/stripe-webhook/account/:key", handleStripeAccountWebhook)
	r.POST("/v1/stripe-webhook/connect/:key", handleStripeConnectWebhook)

	// CSP reports
	r.POST("/csp-report", handleCSPReport)

	r.GET("/debug/vars", expvar.Handler())

	r.Run(config.Listen)
}

func init() {
	const (
		defaultConfig = "consortium.conf"
	)
	flag.IntVar(&userVar, "user", 0, "user to connect as")
	flag.StringVar(&configPath, "config", defaultConfig, "path to consortium.conf file")
	flag.BoolVar(&tokenVar, "token", false, "user to generate token for (-user required)")
	flag.IntVar(&radarVar, "radar", 0, "ws:// radar client for specified card id")
	flag.BoolVar(&dashboardVar, "dashboard", false, "ws:// dashboard client for specified user (-user required)")
	flag.BoolVar(&dashcloneVar, "dashclone", false, "start a dashboard clone process")
	flag.BoolVar(&opentokVar, "opentok", false, "generate opentok session")
	flag.IntVar(&callVar, "call", 0, "ws:// call connect with specified call id (-user required)")
	flag.IntVar(&dialVar, "dial", 0, "dial on specified card")
	flag.IntVar(&simulateVar, "simulate", 0, "simulate activity of specified amount of users, set $SIMULATOR_URL (e.g. https://api..., defaults to 'http://`consortium.conf:listen`')")
	flag.BoolVar(&workerVar, "worker", false, "start as worker instance")
	flag.BoolVar(&rescoreVar, "rescore", false, "rebuild score tables")
	flag.StringVar(&workerPushVar, "worker-push", "", "push specified task to worker ('list' to see all), e.g. -worker-push score_rebuild,1")
	flag.BoolVar(&schedulerVar, "scheduler", false, "start as scheduler instance. can be only one, otherwise expect bugs!")
	flag.BoolVar(&pusherVar, "pusher", false, "start pusher instance (currently APNS only)")
	flag.StringVar(&pusherPushVar, "pusher-push", "", "push specified task to pusher (`list` to see all), e.g. -pusher-push push_card_notification,3,card_id:4")
	flag.StringVar(&migrateVar, "migrate", "", "run db migrations (up/down)")
}

func main() {
	rand.Seed(time.Now().UnixNano())

	flag.Parse()

	loadConfig(configPath)

	if config.Debug {
		logrus.SetLevel(logrus.DebugLevel)
	}
	// logrus.SetFormatter(&logrus.JSONFormatter{})

	// generate token for specified user id
	if tokenVar {
		if userVar == 0 {
			logrus.Debugf("-user option required")
			return
		}

		user, err := GetUser(userVar)
		if err != nil {
			panic(err)
		}
		if user == nil {
			panic("user not found")
		}

		token, err := createToken(user, "console")
		if err != nil {
			panic(err)
		}
		fmt.Println(token)

		return
	}

	// generate opentok session
	if opentokVar {
		opentok, err := GetOpenTok()
		if err != nil {
			log.Panic(err)
		}
		logrus.Debugf("session: %s", opentok.SessionID)
		logrus.Debugf("token 1: %s", opentok.Token(1, 6*time.Hour))
		logrus.Debugf("token 2: %s", opentok.Token(2, 6*time.Hour))
		return
	}

	// client connect to radar (ws)
	if radarVar > 0 {
		clientRadar(radarVar)
		return
	}

	// client connect to dashboard (ws)
	if dashboardVar {
		if userVar == 0 {
			logrus.Debugf("-user option required")
			return
		}

		clientDashboard(userVar)
		return
	}

	if rescoreVar {
		rebuildAllScores()
		return
	}

	if callVar > 0 {
		if userVar == 0 {
			logrus.Debugf("-user option required")
			return
		}
		clientCall(callVar, userVar)
		return
	}

	if dialVar > 0 {
		if userVar == 0 {
			logrus.Debugf("-user option required")
			return
		}
		clientDial(dialVar, userVar)
		return
	}

	if simulateVar > 0 {
		clientSimulate(simulateVar)
		return
	}

	workerSetup()
	if workerVar {
		worker.Work()
		return
	}

	pusherSetup()
	if pusherVar {
		pusher.Work()
		return
	}

	if len(pusherPushVar) > 0 {
		if pusherPushVar == "list" {
			for k := range pusher.w.handlers {
				logrus.Debugf("%s,%%d", k)
			}
			return
		}

		vars := strings.Split(pusherPushVar, ",")
		name := vars[0]
		id, _ := strconv.Atoi(vars[1])
		args := JSON{}
		for _, k := range vars[2:] {
			kv := strings.SplitN(k, ":", 2)
			if len(kv) != 2 {
				args[kv[0]] = false
			} else {
				num, err := strconv.Atoi(kv[1])
				if err == nil {
					args[kv[0]] = num
				} else {
					args[kv[0]] = kv[1]
				}
			}
		}

		logrus.Debugf("calling %v(%d, %#v)", name, id, args)
		if err := pusher.w.PerformAsync(name, uint(id), args); err != nil {
			log.Fatal(err)
		}
		return
	}

	if len(workerPushVar) > 0 {
		if workerPushVar == "list" {
			for k := range worker.handlers {
				logrus.Debugf("%s,%%d", k)
			}
			return
		}
		args := strings.SplitN(workerPushVar, ",", 2)
		if len(args) != 2 {
			log.Fatalln("invalid args")
		}
		name := args[0]
		id, _ := strconv.Atoi(args[1])

		worker.PerformAsync(name, uint(id), nil)
		return
	}

	if schedulerVar {
		Scheduler()
		return
	}

	if dashcloneVar {
		_, err := NewDashboard(R)
		if err != nil {
			log.Fatal(err)
		}

		time.Sleep(time.Hour * 24 * 30)
		return
	}

	if len(migrateVar) > 0 {
		Migrate(migrateVar)
		return
	}

	var err error
	dashboard, err = NewDashboard(R)
	if err != nil {
		log.Fatal(err)
	}

	setupGin()
}
