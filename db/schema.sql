-- postgresql 9.4
-- schema used in tests

create type gender as enum ('male', 'female');

-- users
create table users (
  id serial unique primary key,

  name varchar(192),
  first_name varchar(64),
  middle_name varchar(64),
  last_name varchar(64),

  birthday date,
  gender gender,
  locale varchar(16) not null default 'en_US',

  languages varchar(64) not null default 'en',

  balance integer not null default 0,
  balance_currency varchar(3) not null default 'usd',

  pending_balance integer not null default 0,
  pending_balance_currency varchar(3) not null default 'usd',

  has_unpaid_invoice integer,

  picture varchar(256),

  email varchar(256) unique,

  questions_count integer not null default 0,
  answers_count integer not null default 0,

  stripe_customer varchar(64) unique,
  stripe_card_brand varchar(32),
  stripe_card_last_four varchar(4),

  fb_id varchar(64) unique not null,
  fb_access_token varchar(256),
  fb_permissions varchar(256) not null default '',

  fb_card_seen_at timestamp with time zone,
  fb_card_count integer not null default 0,

  account_kit_id varchar(64) unique,
  account_kit_number varchar(64),
  account_kit_access_token varchar(256),

  is_disabled boolean not null default false,
  disabled_reason varchar(256),
  disabled_by integer,

  notifications boolean not null default false,
  next_notification_at timestamp with time zone not null default current_timestamp,
  notifications_frequency varchar(32) not null default 'daily',

  notifications_card_count integer not null default 0,
  notifications_card_seen_at timestamp with time zone,

  last_seen_at timestamp with time zone not null default current_timestamp,

  updated_at timestamp with time zone not null default current_timestamp,
  created_at timestamp with time zone not null default current_timestamp
);
create index on users ( fb_id );
create index on users ( account_kit_id );
alter table users add constraint users_disabled_by_fkey foreign key (disabled_by) references users (id);


-- friends
create table friends (
  id serial unique primary key,
  user_id integer not null references users,
  friend_id integer not null references users,
  -- source is something like 'fb', 'tw', 'ln' (for linkedin) and so forth
  source varchar(2) not null,

  updated_at timestamp with time zone not null default current_timestamp,
  created_at timestamp with time zone not null default current_timestamp
);
create index on friends ( user_id );
create index on friends ( friend_id );
create unique index on friends ( user_id, friend_id, source );

-- scores
create table scores (
  id serial unique primary key,
  user_id integer not null references users,

  fb_verified boolean,
  fb_is_verified boolean,
  fb_friends_count integer,

  caller_score integer not null default 0,
  callee_score integer not null default 0,

  updated_at timestamp with time zone not null default current_timestamp,
  created_at timestamp with time zone not null default current_timestamp
);
create unique index on scores ( user_id );

-- topics
create table topics (
  id serial unique primary key,
  name varchar(256) unique,

  subscribers integer not null default 0,

  ref_id integer references topics,
  parent_id integer references topics,

  is_public boolean not null default 'f',
  is_reviewed boolean not null default 'f',

  created_at timestamp with time zone not null default current_timestamp
);
create index on topics ( name );
create index on topics ( subscribers );
create index on topics ( is_public );
create index on topics ( is_reviewed );
create index on topics ( parent_id );
create index on topics ( ref_id );

create table users_topics (
  user_id integer not null references users,
  topic_id integer not null references topics,

  is_deleted boolean not null default 'f',
  is_suggested boolean not null default 'f',

  calls_count integer not null default 0,
  rating integer not null default 0,
  score integer not null default 0,

  card_seen_at timestamp with time zone,

  created_at timestamp with time zone not null default current_timestamp
);
create index on users_topics ( user_id );
create index on users_topics ( topic_id );
create unique index on users_topics ( user_id, topic_id );
create index on users_topics ( user_id, score );
create index on users_topics ( topic_id, score );
create index on users_topics ( user_id, card_seen_at );
create index on users_topics ( user_id, is_suggested );

-- endorsmenets
create table endorsements (
  user_id integer references users,
  topic_id integer references topics,
  endorser_id integer references users,

  is_endorsed boolean,
  card_seen_at timestamp with time zone,

  priority integer not null default 0,

  updated_at timestamp with time zone not null default current_timestamp,
  created_at timestamp with time zone not null default current_timestamp
);
create index on endorsements ( user_id );
create index on endorsements ( topic_id );
create index on endorsements ( endorser_id );
create unique index on endorsements ( user_id, topic_id, endorser_id );


-- cards
create table cards (
  id serial unique primary key,
  user_id integer not null references users,
  text varchar(256) not null,
  rate integer not null,
  language varchar(2) not null,

  is_live boolean not null default false,
  calls_count integer not null default 0,
  views_count integer not null default 0,

  updated_at timestamp with time zone not null default current_timestamp,
  created_at timestamp with time zone not null default current_timestamp
);
create index on cards ( user_id );
create index on cards ( is_live );

create table cards_topics (
  card_id integer not null references cards,
  topic_id integer not null references topics
);
create unique index on cards_topics ( card_id, topic_id );


-- calls
create table calls (
  id serial unique primary key,

  card_id integer not null references cards,

  caller_id integer not null references users,
  callee_id integer not null references users,

  duration integer not null default 0,

  rate integer not null,

  caller_conn_quality integer not null default 0,
  callee_conn_quality integer not null default 0,

  caller_rating integer not null default 0,
  callee_rating integer not null default 0,

  session_id varchar(256) not null,

  is_hangup boolean not null default false,

  caller_cost integer not null default 0,
  callee_cost integer not null default 0,
  application_fee integer not null default 0,

  started_at timestamp with time zone,
  ended_at timestamp with time zone,

  updated_at timestamp with time zone not null default current_timestamp,
  created_at timestamp with time zone not null default current_timestamp
);
create index on calls ( card_id );
create index on calls ( caller_id );
create index on calls ( callee_id );
create index on calls ( is_hangup );

create table invoices (
  id serial unique primary key,
  user_id integer not null references users,
  call_id integer not null references calls,

  -- Text description that will be displayed in the invoice when emailed / shown to user
  description varchar(256) not null,

  -- Extra information about an invoice for the customer’s credit card statement
  statement_descriptor varchar(256) not null,

  -- Final amount due at this time for this invoice. If the invoice’s total is smaller than the minimum charge amount, for example, or if there is account credit that can be applied to the invoice, the amount_due may be 0. If there is a positive starting_balance for the invoice (the customer owes money), the amount_due will also take that into account. The charge that gets generated for the invoice will be for the amount specified in amount_due
  amount_due integer not null,
  currency varchar(3) not null,
  fee integer not null,
  -- Number of payment attempts made for this invoice, from the perspective of the payment retry schedule. Any payment attempt counts as the first attempt, and subsequently only automatic retries increment the attempt count. In other words, manual payment attempts after the first attempt do not affect the retry schedule.
  attempt_count integer not null default 0,

  -- Whether or not the invoice is still trying to collect payment. An invoice is closed if it’s either paid or it has been marked closed. A closed invoice will no longer attempt to collect payment
  closed boolean not null default false,

  -- The time at which payment will next be attempted
  next_payment_attempt timestamp with time zone,

  paid boolean not null default false,

  -- Starting customer balance before attempting to pay invoice. If the invoice has not been attempted yet, this will be the current customer balance.
  -- starting_balance integer not null default 0,
  -- Ending customer balance after attempting to pay invoice. If the invoice has not been attempted yet, this will be null
  -- ending_balance integer,

  card_last_four varchar(4) not null default '',
  card_brand varchar(32) not null default '',

  updated_at timestamp with time zone not null default current_timestamp,
  created_at timestamp with time zone not null default current_timestamp
);
create unique index on invoices ( call_id );
create index on invoices ( user_id );

alter table users add constraint users_has_unpaid_invoice_fkey foreign key (has_unpaid_invoice) references invoices (id);


-- this is stripe managed account
create table stripe_accounts (
  id serial unique primary key,
  user_id integer not null references users,

  -- stripe account ID to be used with calls on behalf of this account
  stripe_account_id varchar(64) not null,
  -- two letter country code
  country varchar(2) not null,

  -- default and list of all currencies supported by this account
  default_currency varchar(3) not null,

  -- keys
  secret varchar(64) not null,
  publish varchar(64) not null,

  -- verification
  transfers_enabled boolean not null default false,
  details_submitted boolean not null default false,
  disabled_reason varchar(64),
  due_by timestamp with time zone,

  updated_at timestamp with time zone not null default current_timestamp,
  created_at timestamp with time zone not null default current_timestamp
);
create unique index on stripe_accounts ( user_id );


-- call - earnings for your call
-- refund - refund in case user is fined
-- adjustment - internal funds transfer to top up user account in case there is not enough real amount in users balance
create table transactions (
  id serial unique primary key,

  -- Owner of this transaction and money
  user_id integer not null references users,

  -- Signed integer with amount attributed to the balance. Will be negative on trasnfers.
  amount integer not null,
  -- Net is actual amount that influences balance value. In case of `transfer_failure` this will be 0 while `amount` will show intended value for payout that system have failed to transfer
  net integer not null,
  currency varchar(3) not null,

  -- Type of the transaction, one of: call, refund, adjustment, transfer, transfer_cancel, transfer_refund, or transfer_failure
  type varchar(64) not null,

  -- Text description
  description varchar(64) not null,

  -- type is `call`, `refund`
  call_id integer references calls,
  invoice_id integer references invoices,

  updated_at timestamp with time zone not null default current_timestamp,
  created_at timestamp with time zone not null default current_timestamp
);
create index on transactions ( user_id );
create index on transactions ( call_id );


-- devices for notifications
create table devices (
  id serial unique primary key,
  user_id integer not null references users,

  token varchar(256) not null,
  is_enabled boolean not null default false,
  is_sandbox boolean not null default false,

  last_notified_at timestamp with time zone,

  created_at timestamp with time zone not null default current_timestamp
);
create index on devices ( user_id, token );

-- reports
create table reports (
  id serial unique primary key,

  user_id integer not null references users,
  call_id integer references calls,
  card_id integer references cards,

  obj varchar(32) not null,

  report_type varchar(32) not null,
  report_text text,

  report_ip inet not null,

  is_resolved boolean not null default false,

  updated_at timestamp with time zone not null default current_timestamp,
  created_at timestamp with time zone not null default current_timestamp
);

create index on reports ( user_id );
create index on reports ( call_id );
create index on reports ( card_id );
