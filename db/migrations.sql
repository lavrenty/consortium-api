-- migrations

-- 20dec2015
alter table users add last_seen_at timestamp not null default current_timestamp;
create index on users ( fb_id );
create index on users ( digits_number );

-- 21dec2015
alter table users add name varchar(192);
alter table users add middle_name varchar(64);

create table friends (
  id serial unique primary key,
  user_id integer not null,
  friend_id integer not null,
  source varchar(2) not null,

  updated_at timestamp not null default current_timestamp,
  created_at timestamp not null default current_timestamp
);
create unique index on friends ( user_id, friend_id, source );

-- 22dec2015
create table scores (
  user_id integer not null,

  fb_verified boolean,
  fb_is_verified boolean,
  fb_friends_count integer,

  updated_at timestamp not null default current_timestamp,
  created_at timestamp not null default current_timestamp
);
create unique index on scores ( user_id );

-- 24dec2015
alter table scores add caller_count integer not null default 0;
alter table scores add caller_bad_count integer not null default 0;
alter table scores add caller_duration integer not null default 0;
alter table scores add caller_balance numeric(10, 2) not null default 0.0;
alter table scores add caller_positive_endorsements integer not null default 0;
alter table scores add caller_negative_endorsements integer not null default 0;

alter table scores add callee_count integer not null default 0;
alter table scores add callee_bad_count integer not null default 0;
alter table scores add callee_duration integer not null default 0;
alter table scores add callee_balance numeric(10, 2) not null default 0.0;

-- 25dec2015
alter table cards rename price to rate;
alter table calls rename price to rate;
alter table users add fb_permissions varchar(256) not null default '';

-- 29dec2015
alter table users add fb_card_seen_at timestamp;
alter table users add fb_card_count integer not null default 0;

alter table endorsements alter endorsed drop not null;
alter table endorsements rename endorsed to is_endorsed;
alter table endorsements add card_seen_at timestamp;
alter table endorsements add priority integer not null default 0;

-- 30dec2015
update users set fb_permissions = 'user_likes,user_friends,email,user_birthday';

-- 12jan2016
alter table cards alter rate type integer;
alter table calls alter rate type integer;
alter table calls alter cost drop default;
alter table calls alter cost type integer;
alter table calls alter cost set default 0;

alter table scores alter caller_balance drop default;
alter table scores alter caller_balance type integer;
alter table scores alter caller_balance set default 0;

alter table scores alter callee_balance drop default;
alter table scores alter callee_balance type integer;
alter table scores alter callee_balance set default 0;

-- 14jan2016
alter table users add stripe_customer varchar(64);
alter table users add stripe_card_brand varchar(32);
alter table users add stripe_card_last_four varchar(4);

-- 21jan2016
alter table scores add constraint scores_user_id_fkey foreign key (user_id) references users (id);
alter table friends add constraint friends_user_id_fkey foreign key (user_id) references users (id);
alter table friends add constraint friends_friend_id_fkey foreign key (friend_id) references users (id);

-- 1feb2016
alter table users add balance integer not null default 0;

-- 3feb2016
create table invoices (
  id serial unique primary key,
  user_id integer not null references users,
  call_id integer not null references calls,

  -- Text description that will be displayed in the invoice when emailed / shown to user
  description varchar(256) not null,

  -- Extra information about an invoice for the customer’s credit card statement
  statement_descriptor varchar(256) not null,

  -- Final amount due at this time for this invoice. If the invoice’s total is smaller than the minimum charge amount, for example, or if there is account credit that can be applied to the invoice, the amount_due may be 0. If there is a positive starting_balance for the invoice (the customer owes money), the amount_due will also take that into account. The charge that gets generated for the invoice will be for the amount specified in amount_due
  amount_due integer not null,
  currency varchar(3) not null,
  -- Number of payment attempts made for this invoice, from the perspective of the payment retry schedule. Any payment attempt counts as the first attempt, and subsequently only automatic retries increment the attempt count. In other words, manual payment attempts after the first attempt do not affect the retry schedule.
  attempt_count integer not null default 0,

  -- Whether or not the invoice is still trying to collect payment. An invoice is closed if it’s either paid or it has been marked closed. A closed invoice will no longer attempt to collect payment
  closed boolean not null default false,

  -- The time at which payment will next be attempted
  next_payment_attempt timestamp,

  paid boolean not null default false,

  -- Starting customer balance before attempting to pay invoice. If the invoice has not been attempted yet, this will be the current customer balance.
  -- starting_balance integer not null default 0,
  -- Ending customer balance after attempting to pay invoice. If the invoice has not been attempted yet, this will be null
  -- ending_balance integer,

  updated_at timestamp not null default current_timestamp,
  created_at timestamp not null default current_timestamp
);
create unique index on invoices ( call_id );
create index on invoices ( user_id );


-- this is stripe managed account
create table stripe_accounts (
  id serial unique primary key,
  user_id integer not null references users,

  -- stripe account ID to be used with calls on behalf of this account
  stripe_account_id varchar(64) not null,
  -- two letter country code
  country varchar(2) not null,

  -- default and list of all currencies supported by this account
  default_currency varchar(3) not null,
  currencies_supported varchar(64) not null,

  -- keys
  secret varchar(64) not null,
  publish varchar(64) not null,

  -- verification
  disabled_reason varchar(64),
  due_by timestamp,
  fields_needed varchar(256),

  updated_at timestamp not null default current_timestamp,
  created_at timestamp not null default current_timestamp
);
create unique index on stripe_accounts ( user_id );


-- call - earnings for your call
-- refund - refund in case user is fined
-- adjustment - internal funds transfer to top up user account in case there is not enough real amount in users balance
create table transactions (
  id serial unique primary key,

  -- Owner of this transaction and money
  user_id integer not null references users,

  -- Signed integer with amount attributed to the balance. Will be negative on trasnfers.
  amount integer not null,
  -- Net is actual amount that influences balance value. In case of `transfer_failure` this will be 0 while `amount` will show intended value for payout that system have failed to transfer
  net integer not null,
  currency varchar(3) not null,

  -- Type of the transaction, one of: call, refund, adjustment, transfer, transfer_cancel, transfer_refund, or transfer_failure
  type varchar(64) not null,

  -- Text description
  description varchar(64) not null,

  -- type is `call`, `refund`
  call_id integer references calls,
  invoice_id integer references invoices,

  updated_at timestamp not null default current_timestamp,
  created_at timestamp not null default current_timestamp
);
create index on transactions ( user_id );
create index on transactions ( call_id );

alter table calls drop column cost;

-- 8feb2016
alter table users add has_unpaid_invoice boolean not null default false;
alter table invoices add fee integer not null;

-- 11feb2016
alter table calls add is_hangup boolean not null default false;
update calls set is_hangup = true;
create index on calls ( is_hangup );

alter table calls add caller_rating integer not null default 0;
alter table calls add callee_rating integer not null default 0;

update calls set caller_conn_quality = 0 where caller_conn_quality is null;
update calls set callee_conn_quality = 0 where callee_conn_quality is null;
alter table calls alter caller_conn_quality set not null;
alter table calls alter caller_conn_quality set default 0;
alter table calls alter callee_conn_quality set not null;
alter table calls alter callee_conn_quality set default 0;

update calls set caller_rating = trunc(random() * 5 + 1), callee_rating = trunc(random() * 5 + 1), caller_conn_quality = trunc(random() * 5 + 1), callee_conn_quality = trunc(random() * 5 + 1) where is_hangup = true;

-- 11feb2016
drop table scores;
create table scores (
  id serial unique primary key,
  user_id integer not null references users,

  fb_verified boolean,
  fb_is_verified boolean,
  fb_friends_count integer,

  caller_score integer not null default 0,
  callee_score integer not null default 0,

  updated_at timestamp not null default current_timestamp,
  created_at timestamp not null default current_timestamp
);
create unique index on scores ( user_id );

-- 12feb2016
alter table users_topics add calls_count integer not null default 0;
update users_topics set calls_count = (select count(*) from calls left join cards on calls.card_id = cards.id left join cards_topics on cards.id = cards_topics.card_id where caller_id = users_topics.user_id and cards_topics.topic_id = users_topics.topic_id and calls.duration > 60);

alter table users_topics drop column rating;
alter table users_topics add score integer not null default 0;
alter table users_topics add rating integer not null default 0;

alter table users add questions_count integer not null default 0;
update users set questions_count = (select count(*) from calls where callee_id = users.id AND duration > 60);
alter table users add answers_count integer not null default 0;
update users set answers_count = (select count(*) from calls where caller_id = users.id AND duration > 60);

-- 15feb2016
alter table users_topics rename is_hidden to is_suggested;
alter table users_topics add card_seen_at timestamp;
create index on users_topics ( user_id, score );
create index on users_topics ( topic_id, score );
create index on users_topics ( user_id, card_seen_at );
create index on users_topics ( user_id, is_suggested );

-- 16feb2016
alter table invoices add card_last_four varchar(4) not null default '';
alter table invoices add card_brand varchar(32) not null default '';

alter table calls add caller_cost integer not null default 0;
alter table calls add callee_cost integer not null default 0;
alter table calls add application_fee integer not null default 0;

update calls set callee_cost = 100 + ((duration/60) * rate) where rate > 0 and duration > 60;
update calls set caller_cost = ((duration/60) * rate) * 0.9  where rate > 0 and duration > 60;
update calls set application_fee = callee_cost - caller_cost;

update calls set ended_at = created_at where ended_at is null;
update calls set started_at = ended_at - interval '1 seconds' * duration where started_at is null and ended_at is not null;

alter table users drop column has_unpaid_invoice;
alter table users add has_unpaid_invoice integer references invoices;

-- 17feb2016
alter table users add is_disabled bool not null default false;
alter table users add disabled_reason varchar(256);
alter table users add disabled_by integer;
alter table users add constraint users_disabled_by_fkey foreign key (disabled_by) references users (id);

-- 22feb2016
create table devices (
  id serial unique primary key,
  user_id integer not null references users,

  token varchar(256) not null,
  is_enabled boolean not null default false,
  is_sandbox boolean not null default false,

  created_at timestamp not null default current_timestamp
);
create index on devices ( user_id, token );

alter table users add notifications boolean not null default false;
alter table users add notifications_card_seen_at timestamp;
alter table users add notifications_card_count integer not null default 0;

-- 5mar2006
alter table cards add is_live boolean not null default false;
alter table cards add calls_count integer not null default 0;
alter table cards add views_count integer not null default 0;

create index on cards ( is_live );

alter table devices alter last_notified_at set data type timestamp with time zone;
alter table devices alter created_at set data type timestamp with time zone;

alter table users alter updated_at set data type timestamp with time zone;
alter table users alter created_at set data type timestamp with time zone;
alter table users alter last_seen_at set data type timestamp with time zone;
alter table users alter fb_card_seen_at set data type timestamp with time zone;
alter table users alter notifications_card_seen_at set data type timestamp with time zone;

alter table friends alter updated_at set data type timestamp with time zone;
alter table friends alter created_at set data type timestamp with time zone;

alter table scores alter updated_at set data type timestamp with time zone;
alter table scores alter created_at set data type timestamp with time zone;

alter table topics alter created_at set data type timestamp with time zone;

alter table users_topics alter created_at set data type timestamp with time zone;
alter table users_topics alter card_seen_at set data type timestamp with time zone;

alter table endorsements alter updated_at set data type timestamp with time zone;
alter table endorsements alter created_at set data type timestamp with time zone;
alter table endorsements alter card_seen_at set data type timestamp with time zone;

alter table cards add updated_at timestamp with time zone not null default current_timestamp;
alter table cards alter created_at set data type timestamp with time zone;
update cards set updated_at = created_at;

alter table calls alter started_at set data type timestamp with time zone;
alter table calls alter ended_at set data type timestamp with time zone;
alter table calls alter updated_at set data type timestamp with time zone;
alter table calls alter created_at set data type timestamp with time zone;

alter table invoices alter next_payment_attempt set data type timestamp with time zone;
alter table invoices alter updated_at set data type timestamp with time zone;
alter table invoices alter created_at set data type timestamp with time zone;

alter table stripe_accounts alter due_by set data type timestamp with time zone;
alter table stripe_accounts alter updated_at set data type timestamp with time zone;
alter table stripe_accounts alter created_at set data type timestamp with time zone;

alter table transactions alter updated_at set data type timestamp with time zone;
alter table transactions alter created_at set data type timestamp with time zone;

alter table users drop column digits_id;
alter table users drop column digits_number;
alter table users drop column digits_access_token;

alter table users add account_kit_id varchar(64);
alter table users add account_kit_number varchar(64);
alter table users add account_kit_access_token varchar(256);
create unique index on users ( account_kit_id );

-- 29 jun 2016
delete from cards_topics where card_id in (select cards.id from cards left join users on cards.user_id = users.id where users.fb_id is null);
delete from calls where card_id in (select cards.id from cards left join users on cards.user_id = users.id where users.fb_id is null);
delete from calls where caller_id in (select id from users where fb_id is null);
delete from cards where user_id in (select id from users where fb_id is null);
delete from scores where user_id in (select id from users where fb_id is null);
delete from devices where user_id in (select id from users where fb_id is null);
delete from users_topics where user_id in (select id from users where fb_id is null);
delete from endorsements where user_id in (select id from users where fb_id is null);
delete from endorsements where endorser_id in (select id from users where fb_id is null);
delete from users where fb_id is null;
alter table users alter fb_id set not null;
