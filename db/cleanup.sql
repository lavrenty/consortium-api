-- Executed before running tests to clean up database state
-- after this `schema.sql` is loaded into db
drop table if exists endorsements, users_topics, topics, users, cards, cards_topics, calls, scores, friends, invoices, transactions, stripe_accounts, devices, reports;
drop type if exists gender;
