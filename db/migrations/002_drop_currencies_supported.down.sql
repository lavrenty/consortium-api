-- stripe dropped support for this field

alter table stripe_accounts drop column transfers_enabled;
alter table stripe_accounts drop column details_submitted;
