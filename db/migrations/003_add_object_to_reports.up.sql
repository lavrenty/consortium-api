alter table reports alter call_id drop not null;
alter table reports add column card_id integer references cards;

alter table reports add obj varchar(32) not null default 'call';
alter table reports alter obj drop default;

create index on reports ( call_id );
create index on reports ( card_id );
