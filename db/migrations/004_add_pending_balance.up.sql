
alter table users add balance_currency varchar(3) not null default 'usd';
alter table users add pending_balance integer not null default 0;
alter table users add pending_balance_currency varchar(3) not null default 'usd';
