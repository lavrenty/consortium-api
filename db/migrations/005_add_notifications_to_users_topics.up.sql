
alter table users add column next_notification_at timestamp with time zone not null default current_timestamp;
alter table users add column notifications_frequency varchar(32) not null default 'daily';
