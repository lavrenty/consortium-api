-- psql

create table reports (
  id serial unique primary key,

  user_id integer not null references users,
  call_id integer not null references calls,

  report_type varchar(32) not null,
  report_text text,

  report_ip inet not null,

  is_resolved boolean not null default false,

  updated_at timestamp with time zone not null default current_timestamp,
  created_at timestamp with time zone not null default current_timestamp
);

create index on reports ( user_id );
