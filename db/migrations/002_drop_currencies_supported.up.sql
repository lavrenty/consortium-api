-- stripe dropped support for this field

alter table stripe_accounts drop column currencies_supported;
alter table stripe_accounts drop column fields_needed;

alter table stripe_accounts add column transfers_enabled boolean not null default false;
alter table stripe_accounts add column details_submitted boolean not null default false;
