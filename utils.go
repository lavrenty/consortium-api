package main

import (
	"errors"
	"fmt"
	"net/http"
	"runtime/debug"
	"sync/atomic"

	"github.com/getsentry/raven-go"
	"github.com/gin-gonic/gin"
)

func atomicLoadBool(addr *int32) bool {
	return atomic.LoadInt32(addr) != 0
}

func atomicStoreBool(addr *int32, value bool) {
	val := int32(0)

	if value {
		val = 1
	}

	atomic.StoreInt32(addr, val)
}

// LogRecovery wraps() panic and logs to sentry
func LogRecovery() gin.HandlerFunc {
	return func(c *gin.Context) {
		defer func() {
			flags := map[string]string{
				"url":    c.Request.RequestURI,
				"method": c.Request.Method,
				"ip":     c.ClientIP(),
			}
			if rval := recover(); rval != nil {
				debug.PrintStack()
				rvalStr := fmt.Sprint(rval)
				packet := raven.NewPacket(rvalStr, raven.NewException(errors.New(rvalStr), raven.NewStacktrace(2, 3, nil)))
				sentry.Capture(packet, flags)
				c.Writer.WriteHeader(http.StatusInternalServerError)
			}
			// for _, item := range c.Errors {
			// 	packet := raven.NewPacket(item.Error(), &raven.Message{item.Error(), []interface{}{item.Meta}})
			// 	sentry.Capture(packet, flags)
			// }
		}()
		c.Next()
	}
}
