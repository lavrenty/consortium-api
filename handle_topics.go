package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// GET /topics/autocomplete?q=...
// Suggest and return up to 50 topics ordered by popularity
func handleTopicsAutocomplete(c *gin.Context) {
	// user := c.MustGet("current").(*User)

	query := c.Query("q")
	if len(query) < 1 {
		apiSuccess(c)
		return
	}

	topics, err := TopicsAutocomplete(query, 50)
	if err != nil {
		apiErrorInternal(c, err)
		return
	}

	// unwrap array of topics into json
	result := []JSON{}
	for _, topic := range topics {
		result = append(result, topic.ToJSON())
	}

	c.JSON(http.StatusOK, gin.H{"success": true, "topics": result})
}

// GET /topics/suggest
// returns personalized list of suggestde topics
func handleTopicsSuggest(c *gin.Context) {
	user := c.MustGet("current").(*User)

	topics, err := user.SuggestedTopics(25)
	if err != nil {
		apiErrorInternal(c, err)
		return
	}

	// unwrap array of topics into json
	result := []JSON{}
	for _, topic := range topics {
		result = append(result, topic.ToStatsJSON())
	}

	c.JSON(http.StatusOK, gin.H{"success": true, "topics": result})
}

// GET /topics/recent - returns recent topics used by user + some popular suggestions
func handleTopicsRecent(c *gin.Context) {
	user := c.MustGet("current").(*User)

	recent, err := user.TopicsRecent(10)
	if err != nil {
		apiErrorInternal(c, err)
		return
	}

	limit := 10 - len(recent)
	popular, err := GetTopTopics(limit + 10)
	if err != nil {
		apiErrorInternal(c, err)
		return
	}

	result := map[string][]JSON{}
	for _, topic := range recent {
		result["recent"] = append(result["recent"], topic.ToJSON())
	}

	for _, topic := range popular {
		result["popular"] = append(result["popular"], topic.ToJSON())
	}

	c.JSON(http.StatusOK, gin.H{"success": true, "topics": result})
}

// POST /topics/subscribe
// Subscribe user to certain topic
func handleTopicsSubscribe(c *gin.Context) {
	user := c.MustGet("current").(*User)

	var topicMsg struct {
		Name    string `json:"name" binding:"required"`
		Suggest bool   `json:"suggest"`
	}

	if err := c.BindJSON(&topicMsg); err != nil {
		apiError(c, 400, "request error", err)
		return
	}

	topic, err := FindOrCreateTopicByName(topicMsg.Name)
	if err != nil {
		apiErrorInternal(c, err)
		return
	}

	if err := user.Subscribe(topic, topicMsg.Suggest); err != nil {
		apiErrorInternal(c, err)
		return
	}

	c.JSON(http.StatusOK, gin.H{"success": true, "topic": topic.ToJSON()})
}

// DELETE /topic/:topic
// Unsubscribe user from certain topic
func handleTopicUnsubscribe(c *gin.Context) {
	user := c.MustGet("current").(*User)
	topic := c.MustGet("topic").(*Topic)

	if err := user.Unsubscribe(topic); err != nil {
		apiErrorInternal(c, err)
		return
	}

	c.JSON(http.StatusOK, gin.H{"success": true})
}
