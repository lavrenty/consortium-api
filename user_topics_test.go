package main

import (
	"math/rand"
	"testing"
)

func TestTopicSubscribe(t *testing.T) {
	user, err := GetUser(1)
	if err != nil || user == nil {
		t.Fatal(err, user)
	}

	topics, err := user.Topics()
	if err != nil {
		t.Fatal(err)
	}

	initial := len(topics)

	names := []string{"Investing", "Movies", "Mathematics"}
	for _, name := range names {
		topic, errx := FindOrCreateTopicByName(name)
		if errx != nil {
			t.Fatal(err)
		}

		sb := topic.Subscribers
		if err = user.Subscribe(topic, false); err != nil {
			t.Fatal(err)
		}
		if topic.Subscribers-1 != sb {
			t.Fatal(sb, topic.Subscribers, topic)
		}

		// double subscribe
		if err = user.Subscribe(topic, false); err != nil {
			t.Fatal(err)
		}
		if topic.Subscribers-1 != sb {
			t.Fatal(sb, topic.Subscribers, topic)
		}
	}

	// reload
	user, err = GetUser(1)
	if err != nil || user == nil {
		t.Fatal(err, user)
	}

	topics, err = user.Topics()
	if err != nil || len(topics)-initial != len(names) {
		t.Fatal(err, topics, names)
	}

	// unsubscribe unexistent
	topic, err := FindOrCreateTopicByName("Quora")
	if err != nil {
		t.Fatal(err)
	}

	sb := topic.Subscribers
	if err = user.Unsubscribe(topic); err != nil {
		t.Fatal(err)
	}
	if sb != topic.Subscribers {
		t.Fatal(sb, topic.Subscribers)
	}

	// unsubscribe own
	topics, err = user.Topics()
	if err != nil || len(topics)-initial != len(names) {
		t.Fatal(err, topics, names)
	}

	for _, topic := range topics {
		// check counters
		sb := topic.Subscribers
		if err = user.Unsubscribe(&topic); err != nil {
			t.Fatal(err)
		}
		if sb-1 != topic.Subscribers {
			t.Fatal(sb, topic.Subscribers)
		}

		// check db is in sync
		other, errx := GetTopic(topic.ID)
		if errx != nil {
			t.Fatal(err)
		}
		if other.Subscribers != topic.Subscribers {
			t.Fatal(other.Subscribers, topic.Subscribers)
		}
	}

	// must be empty
	topics, err = user.Topics()
	if err != nil || len(topics) != 0 {
		t.Fatal(err, topics)
	}
}

func TestGetSuggestedTopic(t *testing.T) {
	user := testCreateUser(t)

	names := []string{"Investing", "Movies", "Mathematics", "Other", "Test topic"}
	for _, name := range names {
		topic, err := FindOrCreateTopicByName(name)
		if err != nil {
			t.Fatal(err)
		}

		if err := user.Subscribe(topic, true); err != nil {
			t.Fatal(err)
		}
		score := TopicSuggestableScore + rand.Intn(100)
		if _, err := db.Exec("UPDATE users_topics SET score = $1 WHERE user_id = $2 AND topic_id = $3", score, user.ID, topic.ID); err != nil {
			t.Fatal(err)
		}
	}

	topic, err := user.GetSuggestedTopic()
	if err != nil {
		t.Fatal(err)
	}

	if err := user.UpdateSuggestedTopicCardAsSeen(topic); err != nil {
		t.Fatal(err)
	}
}
